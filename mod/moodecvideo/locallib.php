<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private moodecvideo module utility functions
 *
 * @package mod_moodecvideo
 * @copyright  2009 Petr Skoda (http://skodak.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/filelib.php");
require_once("$CFG->libdir/resourcelib.php");
require_once("$CFG->dirroot/mod/moodecvideo/lib.php");


/**
 * File browsing support class
 */
class moodecvideo_content_file_info extends file_info_stored {
    public function get_parent() {
        if ($this->lf->get_filepath() === '/' and $this->lf->get_filename() === '.') {
            return $this->browser->get_file_info($this->context);
        }
        return parent::get_parent();
    }
    public function get_visible_name() {
        if ($this->lf->get_filepath() === '/' and $this->lf->get_filename() === '.') {
            return $this->topvisiblename;
        }
        return parent::get_visible_name();
    }
}

function moodecvideo_get_editor_options($context) {
    global $CFG;
    return array('subdirs'=>1, 'maxbytes'=>$CFG->maxbytes, 'maxfiles'=>-1, 'changeformat'=>1, 'context'=>$context, 'noclean'=>1, 'trusttext'=>0);
}

function clean_iframe($url) {

    $url = strip_tags($url,"<iframe>");  // interpret and keep only iframe tags

    $url = strstr($url, '<iframe'); // Delete everything before iframe
    $length = strpos($url, "iframe>")+7 ;
    $url = substr($url, 0, $length); // Delete everything after iframe
    if($url == "0"){ // Check if no balise iframe
        $url = "" ;
    }
    return $url ;

}

function get_iframe_id($url){
    // Delete iframe tags
    $start = strpos($url, 'src="')+5;
    $length = strpos($url, "iframe>");
    $iframe = substr($url, $start, $length);

    // Keep only URL
    $endurl = strpos($iframe, '"')+1;
    $iframe = substr($iframe, 0, $endurl);

    // Keep video ID and parameters
    $startid = strpos($iframe, 'embed/')+6;
    $lengthid = strpos($iframe, '"');
    $iframewithparams = substr($iframe, $startid, $lengthid);

    // Keep video ID only
    if(strpos($iframewithparams, '?')){
        $endid = strpos($iframewithparams, '?');
        $videoid = substr($iframewithparams, 0, $endid);
    }else {
        $endid = strlen($iframewithparams);
        $videoid = substr($iframewithparams, 0, $endid);
        $videoid = str_replace('"', '', $iframewithparams);
    }


    return $videoid ;
}

function get_iframe_params($url){
    // Delete iframe tags
    $start = strpos($url, 'src="')+5;
    $length = strpos($url, "iframe>");
    $iframe = substr($url, $start, $length);

    // Keep only URL
    $endurl = strpos($iframe, '"')+1;
    $iframe = substr($iframe, 0, $endurl);

    // Keep video ID and parameters
    $startid = strpos($iframe, 'embed/')+6;
    $lengthid = strpos($iframe, '"');
    $iframewithparams = substr($iframe, $startid, $lengthid);

    //Keep video params

    if(strpos($iframewithparams, '?')) {
        $startparams = strpos($iframewithparams, '?') + 1;
        $lengthparams = strpos($iframewithparams, '"');
        $params = substr($iframewithparams, $startparams, $lengthparams);
        $params = str_replace('"', '', $params);

        //  var_dump($params);
        $params = explode("&", $params);
        $param = array();
        foreach ($params as $key => $value) {
            $paramExplode = explode("=", $value);
            $key = $paramExplode[0];
            $value = $paramExplode[1];
            $param[$key] = $value;
        }


        $parameter = 'playerVars: { ';
        foreach ($param as $key => $value) {
            $parameter = $parameter . '\'' . $key . '\': \'' . $value . '\',';
        }
        $parameter = $parameter . '\'frameborder\': 0,\'autoplay\': 0, \'disablekb\': 1},';
    }
    else {
        $parameter = 'playerVars: { \'autoplay\': 0, \'disablekb\': 1 },';

    }

    return $parameter ;
}