<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'moodecvideo', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   mod_moodecvideo
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['configdisplayoptions'] = 'Veuillez sélectionner toutes les options à mettre à disposition. les réglages existants ne seront pas modifiés. Il est possible de sélectionner plusieurs champs à la fois.';
$string['content'] = 'Vidéo Iframe';
$string['content_help'] = 'Coller ici l\'iframe que vous aurez récupéré sur une site de gestion vidéo comme Youtube';
$string['contentheader'] = 'Vidéo';
$string['createmoodecvideo'] = 'Créer une nouvelle ressource vidéo';
$string['displayoptions'] = 'Type d\'affichage disponible';
$string['displayselect'] = 'Afficher';
$string['displayselectexplain'] = 'Sélectionner le type d\'affichage';
$string['legacyfiles'] = 'Migrer les anciens fichiers de cours';
$string['legacyfilesactive'] = 'Actif';
$string['legacyfilesdone'] = 'Terminé';
$string['modulename'] = 'Moodec Video';
$string['modulename_help'] = 'Cette ressource vidéo permet aux professeurs d\'intégrer des "iframe vidéo" sur une nouvelle page. Ces vidéos peuvent venir de diffuseur en ligne comme Youtube ou Vimeo';
$string['modulenameplural'] = 'moodecvideos';
$string['optionsheader'] = 'Display options';
$string['moodecvideo-mod-moodecvideo-x'] = 'Une ressource vidéo';
$string['moodecvideo:addinstance'] = 'Ajouter une nouvelle ressource vidéo';
$string['moodecvideo:view'] = 'Voir le contenu vidéo';
$string['pluginadministration'] = 'Administration Vidéo';
$string['pluginname'] = 'moodecvideo';
$string['printheading'] = 'Afficher le nom de la vidéo';
$string['printheadingexplain'] = 'Afficher le nom de la vidéo au-dessus de son contenu ?';
$string['printintro'] = 'Afficher la description de la vidéo';
$string['printintroexplain'] = 'Afficher la description de la vidéo au-dessus de son contenu ?';
$string['endpoint'] = 'Endpoint';
$string['username'] = 'Nom d\'utilisateur';
$string['password'] = 'Mot de passe';
