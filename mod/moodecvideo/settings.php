<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * moodecvideo module admin settings and defaults
 *
 * @package mod_moodecvideo
 * @copyright  2009 Petr Skoda (http://skodak.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    // Endpoint.
    $settings->add(new admin_setting_configtext('moodecvideo/endpoint',
        get_string('endpoint', 'moodecvideo'), '',
        'http://your.domain.com/endpoint/location/', PARAM_URL));
    // Username.
    $settings->add(new admin_setting_configtext('moodecvideo/username',
        get_string('username', 'moodecvideo'), '', 'username', PARAM_TEXT));
    // Key or password.
    $settings->add(new admin_setting_configtext('moodecvideo/password',
        get_string('password', 'moodecvideo'), '', 'password', PARAM_TEXT));
}

