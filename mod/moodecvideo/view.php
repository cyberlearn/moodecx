<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * moodecvideo module version information
 *
 * @package mod_moodecvideo
 * @copyright  2009 Petr Skoda (http://skodak.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/moodecvideo/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

$id      = optional_param('id', 0, PARAM_INT); // Course Module ID
$p       = optional_param('p', 0, PARAM_INT);  // moodecvideo instance ID
$inpopup = optional_param('inpopup', 0, PARAM_BOOL);

if ($p) {
    if (!$moodecvideo = $DB->get_record('moodecvideo', array('id'=>$p))) {
        print_error('invalidaccessparameter');
    }
    $cm = get_coursemodule_from_instance('moodecvideo', $moodecvideo->id, $moodecvideo->course, false, MUST_EXIST);

} else {
    if (!$cm = get_coursemodule_from_id('moodecvideo', $id)) {
        print_error('invalidcoursemodule');
    }
    $moodecvideo = $DB->get_record('moodecvideo', array('id'=>$cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);
$courseid = $course->id ;
require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/moodecvideo:view', $context);

// Trigger module viewed event.
$event = \mod_moodecvideo\event\course_module_viewed::create(array(
   'objectid' => $moodecvideo->id,
   'context' => $context
));
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('moodecvideo', $moodecvideo);
$event->trigger();

// Update 'viewed' state if required by completion system
require_once($CFG->libdir . '/completionlib.php');
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

$PAGE->set_url('/mod/moodecvideo/view.php', array('id' => $cm->id));

$PAGE->set_title($course->shortname.': '.$moodecvideo->name);
$PAGE->set_heading($course->fullname);
$PAGE->set_activity_record($moodecvideo);

/*$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/moodecvideo/src/xapiwrapper.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/moodecvideo/src/videoprofile.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/moodecvideo/src/xapi-youtube-statements.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/moodecvideo/src/initplayer.js'),true);*/

echo $OUTPUT->header();

echo HTML_WRITER::start_tag('div', array('class'=>'videotitle'));

echo HTML_WRITER::start_tag('h1');
    echo HTML_WRITER::start_tag('img', array('src'=>'./pix/icon.gif'));
    echo HTML_WRITER::end_tag('img');
    echo ' '.$moodecvideo->name ;
echo HTML_WRITER::end_tag('h1');
echo HTML_WRITER::end_tag('div');

//old youtube video iframe
//echo HTML_WRITER::start_tag('div', array('class'=>'iframevideomoodec'));
//    echo $moodecvideo->content ;
//echo HTML_WRITER::end_tag('div');

//Beginning xapi youtube---------------------------------------
//These parameters should be reloaded from database

$endpoint = get_config('moodecvideo', 'endpoint');
$userendpoint = get_config('moodecvideo', 'username');
$passendpoint = get_config('moodecvideo', 'password');
//Parameters necessary to the statements
$dirsrc = $CFG->wwwroot.'/mod/moodecvideo/src';
$username = $USER->username;
$userid = $USER->id;
$moduleurl = $CFG->wwwroot.'/mod/moodecvideo/view.php?id='.$cm->id;
$modulename = $moodecvideo->name;
$baseurl = $CFG->wwwroot;

if(strpos($moodecvideo->content,"youtube")) {
    $videoid = get_iframe_id($moodecvideo->content);
    $param = get_iframe_params($moodecvideo->content);

//The div bloc for video player.
    echo HTML_WRITER::start_tag('div', array('id' => 'player','class'=>'iframevideomoodec'));
    echo HTML_WRITER::end_tag('div');

    echo '<script type="text/javascript" src="' . $dirsrc . '/xapiwrapper.min.js"></script>';
    echo '<script type="text/javascript" src="' . $dirsrc . '/videoprofile.js"></script>';
    echo '<script type="text/javascript" src="' . $dirsrc . '/xapi-youtube-statements.js"></script>';
    echo '<script type="text/javascript" src="' . $dirsrc . '/initplayer.js"></script>';

    echo '<script>';
//Call the init() function in "initplayer.js" to initialize the player
    echo 'init("' . $username . '","' . $userid . '","' . $moduleurl . '","' . $modulename . '","' . $baseurl . '","' . $endpoint . '","' . $userendpoint . '","' . $passendpoint . '","' . $courseid . '","' . $moodecvideo->id . '");';
//Add video player to the bloc. We need $videoid here.
    echo 'var player;';
    echo 'function onYouTubeIframeAPIReady() {';
    echo 'player = new YT.Player(\'player\', {';
    echo 'videoId: "' . $videoid . '",';
    //echo 'playerVars: { \'autoplay\': 0, \'disablekb\': 1 },';
    echo $param ;
    echo 'events: {';
    echo '\'onReady\': ADL.XAPIYoutubeStatements.onPlayerReady,';
    echo '\'onStateChange\': ADL.XAPIYoutubeStatements.onStateChange';
    echo '}';
    echo '});';
    echo '}';
    echo '</script>';
//End xapi youtube---------------------------------------

}else {
    echo HTML_WRITER::start_tag('div', array('id'=>'player','class'=>'iframevideomoodec'));
    echo $moodecvideo->content;
    echo HTML_WRITER::end_tag('div');
}

if ($moodecvideo->intro) {
    echo HTML_WRITER::start_tag('div', array('class'=>'videodescription'));
        echo $moodecvideo->intro ;
    echo HTML_WRITER::end_tag('div');
}

echo $OUTPUT->footer();
