/**
 * Created by leyun.xia on 07.03.2017.
 */
function initYT() {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

function init(username, userid, moduleurl, modulename, baseurl, endpoint, userendpoint, passendpoint, courseid, videoid) {
    var myXAPI = {};

    ADL.launch(function (err, launchdata, xAPIWrapper) {
        if (!err) {
            ADL.XAPIWrapper = xAPIWrapper;
            myXAPI.actor = launchdata.actor;
            if (launchdata.customData.content) {

                myXAPI.context = {
                    contextActivities:
                    {
                        grouping:
                        [{id: launchdata.customData.content}]
                    },
                    extensions :
                        {
                            'http://lrs.learninglocker.net/define/extensions/moodle_logstore_standard_log' : {
                                courseid : courseid,
                                objectid : videoid,
                                userid : userid
                            }
                        }
                };
            } else {
                myXAPI.context = {
                    contextActivities: {
                        grouping:
                            [{id: "http://adlnet.gov/event/xapiworkshop/launch/no-customData"}]
                    },
                    extensions : {
                    'http://lrs.learninglocker.net/define/extensions/moodle_logstore_standard_log' :
                        {
                                courseid : courseid,
                                objectid : videoid,
                                userid : userid
                        }
                }};
            }
        } else {
            ADL.XAPIWrapper.changeConfig({
                "endpoint":endpoint,
                "user":userendpoint,
                "password":passendpoint
            });
            myXAPI.actor = {
                name: username,
                account: {homePage:baseurl, name: userid}
            };

            myXAPI.context = {
                contextActivities: {
                    grouping:
                        [{id: "http://adlnet.gov/event/xapiworkshop/launch/no-customData"}]
                },
                extensions : {
                    'http://lrs.learninglocker.net/define/extensions/moodle_logstore_standard_log' :
                        {
                            courseid : courseid,
                            objectid : videoid,
                            userid : userid
                        }
                }};
        }
        myXAPI.object = {id: moduleurl,
            definition:
            {
                type: "http://lrs.learninglocker.net/define/type/moodle/moodecvideo",
                name: {"en": modulename}
            }};

        ADL.XAPIYoutubeStatements.changeConfig(myXAPI);

    }, true);

    initYT()
}
