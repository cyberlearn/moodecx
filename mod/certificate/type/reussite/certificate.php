<?php

// This file is part of the certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A4_non_embedded certificate type
 *
 * @package    mod_certificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$pdf = new PDF($certificate->orientation, 'mm', 'A4', true, 'UTF-8', false);

$pdf->SetTitle($certificate->name);
$pdf->SetProtection(array('modify'));
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

// Define variables
// Landscape
if ($certificate->orientation == 'L') {
    $x = 10;
    $y = 50;
    $sealx = 218;
    $sealy = 170;
    $sigx = 47;
    $sigy = 155;
    $custx = 47;
    $custy = 155;
    $wmarkx = 40;
    $wmarky = 31;
    $wmarkw = 212;
    $wmarkh = 148;
    $brdrx = 0;
    $brdry = 0;
    $brdrw = 297;
    $brdrh = 210;
    $codey = 175;
} else { // Portrait
    $x = 10;
    $y = 40;
    $sealx = 150;
    $sealy = 220;
    $sigx = 30;
    $sigy = 230;
    $custx = 30;
    $custy = 230;
    $wmarkx = 26;
    $wmarky = 58;
    $wmarkw = 158;
    $wmarkh = 170;
    $brdrx = 0;
    $brdry = 0;
    $brdrw = 210;
    $brdrh = 297;
    $codey = 250;
}

// Add Moodec Logo
$pdf->Image("$CFG->dirroot/mod/certificate/pix/logo.png", 110, 40, 80, 12);
// Add images and lines
certificate_print_image($pdf, $certificate, CERT_IMAGE_BORDER, $brdrx, $brdry, $brdrw, $brdrh);
certificate_draw_frame($pdf, $certificate);
// Set alpha to semi-transparency
$pdf->SetAlpha(0.2);
certificate_print_image($pdf, $certificate, CERT_IMAGE_WATERMARK, $wmarkx, $wmarky, $wmarkw, $wmarkh);
$pdf->SetAlpha(1);
certificate_print_image($pdf, $certificate, CERT_IMAGE_SEAL, $sealx, $sealy, 45, 10);
certificate_print_image($pdf, $certificate, CERT_IMAGE_SIGNATURE, 210, $sigy, 30, 17);

// Add text
$pdf->SetTextColor(0, 124, 183);
certificate_print_text($pdf, $x, $y+10, 'C', 'Helvetica', '', 40, get_string('pdfTitle', 'certificate'));
certificate_print_text($pdf, $x, $y+30, 'C', 'Helvetica', '', 14, get_string('pdfSubTitle', 'certificate'));
$pdf->SetTextColor(0, 124, 183);
certificate_print_text($pdf, $x, $y + 40, 'C', 'Helvetica', '', 12, get_string('certify', 'certificate'));
certificate_print_text($pdf, $x, $y + 48, 'C', 'Helvetica', '', 20, fullname($USER));
certificate_print_text($pdf, $x, $y + 60, 'C', 'Helvetica', '', 12, get_string('statement', 'certificate'));
certificate_print_text($pdf, $x, $y + 67, 'C', 'Helvetica', '', 20, format_string($course->fullname));
certificate_print_text($pdf, $x, $y + 85, 'C', 'Helvetica', '', 12, certificate_get_date($certificate, $certrecord, $course));
//certificate_print_text($pdf, $x, $y + 102, 'C', 'Helvetica', '', 10, certificate_get_grade($certificate, $course));
//certificate_print_text($pdf, $x, $y + 112, 'C', 'Helvetica', '', 10, certificate_get_outcome($certificate, $course));
//if ($certificate->printhours) {
//    certificate_print_text($pdf, $x, $y + 122, 'C', 'Helvetica', '', 10, get_string('credithours', 'certificate') . ': ' . $certificate->printhours);
//}

certificate_print_text($pdf, $x, $codey, 'C', 'Helvetica', '', 12, certificate_get_code($certificate, $certrecord));

$i = 0;
if ($certificate->printteacher) {
    $context = context_module::instance($cm->id);
    if ($teachers = get_users_by_capability($context, 'mod/certificate:printteacher', '', $sort = 'u.lastname ASC', '', '', '', '', false)) {
        foreach ($teachers as $teacher) {
            certificate_print_text($pdf, 160, 145, 'C', 'Helvetica', '', 12, fullname($teacher));
            break ;
        }
    }
}

certificate_print_text($pdf, 160, 145, 'C', 'Helvetica', '', 12, format_string($certificate->teacher));

certificate_print_text($pdf, 28, $codey, 'L', 'Helvetica', '', 12,"www.hes-so.ch");
certificate_print_text($pdf, $custx, $custy, 'L', null, null, null, $certificate->customtext);
