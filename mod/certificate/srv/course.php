<?php


/**
 * Verify a certificate code
 * @param $code
 * @return mixed
 */
 
 include_once('../../../config.php');
 
 
function verifyCertificate($code){
    global $DB, $CFG, $PAGE;

	include_once('../lib.php');
	include_once('../locallib.php');

	$context = '' ;
    $PAGE->set_context($context);
		
    $sql = "SELECT ci.timecreated AS citimecreated,
     ci.code, ci.certificateid, ci.userid, c.*, u.firstname, u.lastname
     FROM {certificate_issues} ci
                           INNER JOIN {user} u
                           ON u.id = ci.userid
                           INNER JOIN {certificate} c
                           ON c.id = ci.certificateid
                           WHERE ci.code = ?";
    $certificates = $DB->get_records_sql($sql, array($code));
	//return json_encode($certificates);
    // Print Section.
    foreach ($certificates as $certdata) {
        $course = $DB->get_record('course', array('id' => $certdata->course));
        if ($course) {
           $course->fullname ;
        }

        // Modify printdate so that date is always printed.
        $certdata->printdate = 1;
        $certrecord = new stdClass();
        $certrecord->timecreated = $certdata->citimecreated;
        $certrecord->code = $certdata->code;
        $certrecord->userid = $certdata->userid;
        $certrecord->user = $certdata->firstname . " " . $certdata->lastname;

        $course = $DB->get_record('course', array('id' => $certdata->course));
        $date = certificate_get_date($certdata, $certrecord, $course, $certdata->userid);

        if ($course) {
           $certrecord->course = $course->fullname;
        }

        if ($date) {
            $certrecord->date = $date;
        }

        if ($course && $certdata->printgrade > 0) {
            $certrecord->grade =  certificate_get_grade($certdata, $course, $certdata->userid);
        }
		
        return json_encode($certrecord);
    }
}

?>