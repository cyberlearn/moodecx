<?php
/**
 * Created by PhpStorm.
 * User: christop.hadorn
 * Date: 06.10.2015
 * Time: 14:16
 */

require_once('course.php');


global $DB, $CFG, $PAGE;
	

$action = htmlspecialchars((!empty($_REQUEST['action']) ? $_REQUEST['action'] : ''));
$codeCertificate = htmlspecialchars((!empty($_REQUEST['codecertificate']) ? $_REQUEST['codecertificate'] : ''));


switch($action) {
	case 'verifyCertificate':	
		echo verifyCertificate($codeCertificate);
		break;
}
	
