<?php
require_once("../../config.php");
require_once("$CFG->dirroot/mod/certificate/locallib.php");
require_once("$CFG->dirroot/mod/certificate/deprecatedlib.php");
require_once("$CFG->libdir/pdflib.php");

require_login();
if (isguestuser()) {
    die();
}

$context = context_user::instance($USER->id);

$title = get_string("mycertificates", "certificate");
$PAGE->set_url('/mod/certificate/list_certificates.php');
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading($title);
//$PAGE->requires->css(new moodle_url("/mod/certificate/style.css"));
echo $OUTPUT->header();

$list_certificate = get_user_certificates();
$countCertificate = count($list_certificate);

	echo '<div id="list_certificate">';
	echo '<h1 id="titleListCertificate">'.$title.'</h1>';
	echo '<div id="list">';
	
	if($countCertificate == 0) {
		echo '<div id="nocertificate">';
		echo get_string('nocertificate','certificate');
		echo '</div>';
	}
	
    for($i = 0 ; $i < $countCertificate ; $i++){
		$idCertificate = $list_certificate[$i]->id;
        $picture = $list_certificate[$i]->picture;
        $fullname = $list_certificate[$i]->fullname;
        $timecreated = $list_certificate[$i]->timecreated;
		$cmid = $list_certificate[$i]->cmid;
		$courseid = $list_certificate[$i]->courseid;
		
		$linkname = get_string('download', 'moodle');
		$link = new moodle_url('/mod/certificate/view.php?id='.$cmid.'&action=get');
		$button = new single_button($link, $linkname);
		
    echo '<div class="objCertificate">';
		echo '<h3><a href="'. new moodle_url("/course/view.php?id=$courseid").'">'.$fullname.'</a></h3>';
		echo '<span>'.get_string("myrecievedate", "certificate") . date("d/m/Y",$timecreated).'</span>';
		echo $OUTPUT->render($button);
		echo '</div>';
    }
	echo '</div>';
    echo '</div>';
echo $OUTPUT->footer();
echo '<script>';
include_once 'certificates.js';
echo '</script>';



