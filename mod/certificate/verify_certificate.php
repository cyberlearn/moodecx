<?php
require_once("../../config.php");
require_once("$CFG->dirroot/mod/certificate/locallib.php");
require_once("$CFG->dirroot/mod/certificate/deprecatedlib.php");
require_once("$CFG->libdir/pdflib.php");

$title = get_string("verifycertificate", "certificate");
$PAGE->set_url('/mod/certificate/verify_certificate.php');


$PAGE->set_context(context_system::instance());
echo $OUTPUT->header();


echo "    <div id=\"frmCertificate\">\n"; 
echo "		<h1 id=\"titleCertificate\">" . get_string('verifycertificate', 'certificate'). "</h1>\n"; 
echo '<div id="info"></div>'; 
echo '<div id="infoCertificate">'
				.'<div class="formGroup">'
						.'<label>'.get_string('codeverify', 'certificate').'</label><span id="info_verify_certificate"></span>'
				.'</div>'
				.'<div class="formGroup">'
						.'<label>'.get_string('toverify', 'certificate').'</label><span id="info_user_certificate"></span>'
				.'</div>'
				.'<div class="formGroup">'
						.'<label >'.get_string('courseverify', 'certificate').'</label><span id="info_course_certificate"></span>'
				.'</div>'
				.'<div class="formGroup">'
						.'<label>'. get_string('myrecievedate', 'certificate').'</label><span id="info_date_certificate"></span>'
				.'</div>'
				.'<div class="formGroup">'
						.'<label >'. get_string('gradeverify', 'certificate').'</label><span id="info_grade_certificate"></span>'
				.'</div>'
		.'</div>';

		echo '<div id="frm">'
					.'<div class="formGroup">' 
						.'<label>'. get_string('entercode', 'certificate') . '</label>'
						.'<input type="text" name="certnumber" id="certnumber" size="10" value="" class="formInput">'
					.'</div>'
				.'</div>'; 
		echo '<button name="submit" class="btnAction" onClick="verifyCertificate();">'.   get_string('validate', 'certificate').'</button>'; 

	echo '</div>'; 
echo '	<input type="hidden" id="validCertificate" value="'. get_string("validcertificate", "certificate").'">'; 
echo '  <input type="hidden" id="errorCertificate" value="'. get_string("error", "certificate").'"/>'; 
echo '	<input type="hidden" id="mandatoryfield" value="'.get_string("mandatoryfield", "certificate").'">';
echo '	<input type="hidden" id="rootMoodec" value="' . new moodle_url('/') . '"/>'; 


echo $OUTPUT->footer();
echo '<script>';
include_once 'certificates.js';
echo '</script>';

?>