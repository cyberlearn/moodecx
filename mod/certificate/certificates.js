$(document).ready(function() {
    $('#table_certificate tr').click(function() { 
        var href = $(this).find("a").prop("href");
        if(href) {
            window.location = href;
        }
    });
		
		$( "#certnumber" ).focus(function() {
			$( "#frm" ).css('opacity', 1);
		});
});

/**
 * Verification certificate Page
 */
function verifyCertificate(){
    var codeCertificate = $("#certnumber").val();
		$("#info").show();
			console.log(codeCertificate);
    if($.trim(codeCertificate)){
        var data = {
            codecertificate: codeCertificate,
            action: 'verifyCertificate'
        }
		console.log(data);
        $.ajax({
            url: $("#rootMoodec").val() + "mod/certificate/srv/ajax.php",
            type: "POST",
            data: data,
            datatype: "json",
            success: function (data) {
				console.log(data);
				
                $("#certnumber").css('background-color','');
                $("#certnumber").css('border','');
								
                if($.trim(data).length > 0){
					
                    var response = jQuery.parseJSON(data);
                    $("#infoCertificate").css("display","table");
										$( "#frm" ).css('opacity', 0.7);
                    $("#info").html("<div class='success'>"+ $("#validCertificate").val() + "</div>");
                    $("#info_verify_certificate").text(response.code);
                    $("#info_user_certificate").text(response.user);
                    $("#info_course_certificate").text(response.course);
                    $("#info_date_certificate").text(response.date);
                    if($.trim(response.grade).length > 0) {
                        $("#info_grade_certificate").text(response.grade);
                        $("#info_grade_certificate").parent().show();
                    } else {
                        $("#info_grade_certificate").parent().hide();
                    }
                } else {
                    $("#info").html("<div class='error'>"+ $("#errorCertificate").val() + "</div>");
                    $("#infoCertificate").css("display","none");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    } else {
        $("#info").html("<div class='error'>"+ $("#mandatoryfield").val() + "</div>");
        $("#infoCertificate").css("display","none");

        var colorTmp = 'rgba(0, 124, 183, 0.1)';
        $("#certnumber").css('background-color',colorTmp);
        $("#certnumber").css('border','1px solid #007cb7');
    }
}