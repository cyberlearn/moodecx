<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Prints a particular instance of moodecdescription
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_moodecdescription
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Replace moodecdescription with the name of your module and remove this line.

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

require_once($CFG->dirroot.'/mod/moodecdescription/lib.php');
require_once($CFG->dirroot.'/lib/weblib.php');

error_log("view.php", 0);

clearstatcache();

global $PAGE ;

$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... moodecdescription instance ID - it should be named as the first character of the module.

if ($id) {
    $cm         = get_coursemodule_from_id('moodecdescription', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moodecdescription  = $DB->get_record('moodecdescription', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $moodecdescription  = $DB->get_record('moodecdescription', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $moodecdescription->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('moodecdescription', $moodecdescription->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);
$context = context_module::instance($cm->id);



$event = \mod_moodecdescription\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
// In the next line you can use $PAGE->activityrecord if you have set it, or skip this line if you don't have a record.
//$event->add_record_snapshot($PAGE->cm->modname, $activityrecord);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/moodecdescription/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($moodecdescription->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

/*
 * Other things you may want to set - remove if not needed.
 * $PAGE->set_cacheable(false);
 * $PAGE->set_focuscontrol('some-html-id');
 * $PAGE->add_body_class('moodecdescription-'.$somevar);
 */

// Output starts here.
echo $OUTPUT->header();

//Conditions to show the intro can change to look for own settings or whatever.
/*if ($moodecdescription->intro) {
    echo $OUTPUT->box(format_module_intro('moodecdescription', $moodecdescription, $cm->id), 'generalbox mod_introbox', 'moodecdescriptionintro');
}*/







$itemid = file_get_submitted_draft_itemid('prerequisite');
$image = file_rewrite_pluginfile_urls($moodecdescription->prerequisite, 'pluginfile.php', $context->id, 'mod_moodecdescription', 'prerequisite', 0);


$fs = get_file_storage();
$coursecontext = context_course::instance($course->id);
$urlpartners = [];
if ($files = $fs->get_area_files($context->id, 'mod_moodecdescription', 'partnersattachment', 1, 'sortorder', false)) {
    foreach ($files as $file) {
        //echo $file->get_contextid() . "--";
        $fileurl = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), 1, $file->get_filepath(), $file->get_filename());
        array_push($urlpartners, $fileurl);
    }
}


if ($files = $fs->get_area_files($coursecontext->id, 'course', 'overviewfiles', '0', 'sortorder', false)) {
    foreach ($files as $file) {
        $fileurl = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), '', $file->get_filepath(), $file->get_filename());
    }
    $url = $fileurl;
}
else{
    $url = './pix/default_course_image.png';
}


$videoEmbe = '';
if($moodecdescription->video) {
    $videoEmbe = $moodecdescription->video;
}

$partners = '';
if(!empty($urlpartners)){
    foreach($urlpartners as $urlpartner){
		$partners .= HTML_WRITER::start_tag('img', array('src' => $urlpartner, 'class' => 'md_logopartner'));
    }
}

$hasInfoPlus = false;
$prerequisite = '';
$syllabus = '';
$reading = '';
$faq = '';
$startdate = '';
$endcoursedate = '';
$courseformat = '';
$price = '';
$courseimage = '';
$teachers = [];


$courseimage = HTML_WRITER::start_tag('img', array('src' => $url, 'class' => ''));


if(!empty($moodecdescription->prerequisite)) {
	$prerequisite = file_rewrite_pluginfile_urls($moodecdescription->prerequisite, 'pluginfile.php', $context->id, 'mod_moodecdescription', 'prerequisite', 0);
	$hasInfoPlus = true;
}   
if(!empty($moodecdescription->syllabus)) {
    $syllabus = file_rewrite_pluginfile_urls($moodecdescription->syllabus, 'pluginfile.php', $context->id, 'mod_moodecdescription', 'syllabus', 0);
    $hasInfoPlus = true;
}
if(!empty($moodecdescription->reading)) {
	$reading = file_rewrite_pluginfile_urls($moodecdescription->reading, 'pluginfile.php', $context->id, 'mod_moodecdescription', 'reading', 0);
	$hasInfoPlus = true;
}   
if(!empty($moodecdescription->faq)) {
    $faq = file_rewrite_pluginfile_urls($moodecdescription->faq, 'pluginfile.php', $context->id, 'mod_moodecdescription', 'faq', 0);
    $hasInfoPlus = true;
}

if($moodecdescription->courseformat != ""){
	$courseformat = get_string($moodecdescription->courseformat, "moodecdescription");
}

if(!is_null($moodecdescription->price) && $moodecdescription->price > 0) {
	$price = $moodecdescription->price . '.00 ' . $moodecdescription->currency;;
}


$teacherslist = get_teachers($course->id);

foreach($teacherslist as $teacherinfo){
	$userteacher = $DB->get_record("user", array('id'=>$teacherinfo->id));
	$teacher['picture'] = $OUTPUT->user_picture($userteacher, array('size'=>1));
	$teacher['name'] = $teacherinfo->firstname . ' ' . $teacherinfo->lastname ;
	//$teacher['email'] = $teacherinfo->email;
	$teachers[] = $teacher;
}

$mdContent = [
	'fullname'			=> $course->fullname,
	'courseimage'		=> $courseimage,
	'video'				=> $moodecdescription->video,
	'summary'			=> $course->summary,
	'partners'			=> $partners,
	'infocourse'		=> $hasInfoPlus,
	'prerequisite'		=> $prerequisite,
	'syllabus'			=> $syllabus,
	'reading'			=> $reading,
	'faq'				=> $faq,
	'institution'		=> $moodecdescription->institution,
	'effort'			=> $moodecdescription->effort .' '. get_string("weektime", "moodecdescription"),
	'startdate'			=> $startdate,
	'endcoursedate'		=> $endcoursedate,
	'duration'			=> $moodecdescription->duration .' '.get_string("week", "moodecdescription"),
	'courseformat'		=> $courseformat,
	'price'				=> $price,
	'teachers'			=> $teachers
	
];

echo $OUTPUT->render_from_template('mod_moodecdescription/mdContent', $mdContent);
echo $OUTPUT->footer();
