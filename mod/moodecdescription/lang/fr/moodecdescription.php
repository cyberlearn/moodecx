<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * French strings for moodecdescription
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_moodecdescription
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Moodec Description';
$string['name'] = 'Nom de la description';
$string['coursename'] = "Nom du cours";
$string['courseshortname'] = "Diminutif du cours";
$string['moodecdescriptionname'] = 'Institution';
$string['attachment'] = 'Image du cours';

$string['attachmentshelp'] = 'Image du cours';
$string['attachmentshelp_help'] = 'Seuls les types d\'image .jpg ou .png est accepté.';

$string['course_summary'] = 'Résumé du cours';
$string['urlredirection'] = 'URL de redirection';
$string['price'] = 'Coût';
$string['currency'] = 'Devise';
$string['courseformat'] = "Format";
$string['online'] = "Cours en ligne";
$string['inclass'] = "Cours présentiel";
$string['hybrid'] = "Cours hybride";
$string['courseformatdesc_help'] = "<b>Cours en ligne</b> : Le cours est suivi uniquement en ligne.<br><b>Cours présentiel</b> : Le cours est suivi en classe.<br><b>Cours hybride</b> : Le cours est suivi en ligne et en classe.";
$string['free'] = "Gratuit";
$string['efforthours'] = 'Effort par semaine (heures)';
$string['courseduration'] = "Durée du cours";
$string['moodecdescription:addinstance'] = "Ajouter une instance de Moodec description";
$string['moodecdescription:submit'] = "Envoyer (valider) une Moodec description";
$string['moodecdescription:view'] = "Voir une la description du cours";

$string['header_informations'] = 'Avancé';
$string['moreinfo'] = 'Plus d\'informations';
$string['syllabuseditor'] = 'Programme';
$string['syllabuseditor_help'] = 'Aide programme';
$string['prerequisiteeditor'] = 'Prérequis';
$string['prerequisiteeditor_help'] = 'Aide Prérequis';
$string['faqeditor'] = 'FAQ';
$string['faqeditor_help'] = 'Aide FAQ';
$string['readingeditor'] = 'Lectures';
$string['readingeditor_help'] = 'Aide lectures';
$string['partners'] = 'Partenaires';
$string['partnersidentifier'] = 'Logos des partenaires';
$string['partnersidentifier_help'] = 'Drag & dropper les logos des partenaires. In order to display theme in a correct order, please name your files : 1.png, 2.png, 3.gif and so on.';



$string['modulenameplural'] = 'Moodec Descriptions';
$string['modulename_help'] = 'Utilisation du plugin moodecdescription pour... | Le plugin moodecdescription permet de...';
$string['moodecdescriptionfieldset'] = 'Ensemble des champs personnalisés';

$string['moodecdescriptionname_help'] = 'Ceci est le contenu de la bulle d`aide associée au champ Nom de moodecdescription. Notez que la syntaxe est prise en charge';
$string['moodecdescription'] = 'Moodec Description';
$string['pluginadministration'] = 'Administration moodecdescription';
$string['pluginname'] = 'Moodec Description';
$string['weektime'] = 'h/sem';
$string['week'] = 'semaines';


$string['errorinstance'] = 'Il existe déjà une instance de l`activité dans ce cours, vous allez être redirigé';
$string['erroraccess'] = 'Seuls les administrateurs peuvent modifier cette activité';

$string['institutionheader'] = 'Institution';
$string['effortheader'] = 'Effort';
$string['durationheader'] = 'Durée';
$string['coursesummaryheader'] = 'Résumé du cours';
$string['teacherheader'] = 'Professeurs';
$string['video'] = 'Iframe vidéo';
$string['coursetags'] = 'Tags';
$string['coursetags_help'] = 'Tags séparés par des virgules';
$string['endcoursedate'] = 'Date de fin du cours';
$string['reset'] = 'Remise à zéro de la description du cours';
$string['teacher'] = 'Professeur';