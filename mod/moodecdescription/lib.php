<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Library of interface functions and constants for module moodecdescription
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 * All the moodecdescription specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_moodecdescription
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/mod/moodecdescription/mod_form.php');
require_once($CFG->dirroot.'/tag/lib.php');


/*
 * Example constant:
 * define('moodecdescription_ULTIMATE_ANSWER', 42);
 */

/**
 * Moodle core API
 */
function moodecdescription_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_ARCHETYPE:           return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_GROUPS:                  return false;
        case FEATURE_GROUPINGS:               return false;
        case FEATURE_MOD_INTRO:               return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return false;
        case FEATURE_GRADE_HAS_GRADE:         return false;
        case FEATURE_GRADE_OUTCOMES:          return false;
        case FEATURE_BACKUP_MOODLE2:          return true;
        case FEATURE_SHOW_DESCRIPTION:        return true;

        default: return null;
    }
}


/**
 * Saves a new instance of the moodecdescription into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $moodecdescription An object from the form in mod_form.php
 * @param mod_moodecdescription_mod_form $mform
 * @return int The id of the newly inserted moodecdescription record
 */
function moodecdescription_add_instance($data, $mform = null) {
    global $DB, $CFG, $COURSE;

    //

    $cmid = $data->coursemodule;

    if ($mform) {
        // save for database
        $data->prerequisiteformat = intval($data->prerequisiteeditor['format']);
        $data->prerequisite       = $data->prerequisiteeditor['text'];
        $data->syllabusformat = intval($data->syllabuseditor['format']);
        $data->syllabus       = $data->syllabuseditor['text'];
        $data->readingformat = intval($data->readingeditor['format']);
        $data->reading       = $data->readingeditor['text'];
        $data->faqformat = intval($data->faqeditor['format']);
        // $data->tags = $data->coursetags;
        $data->faq       = $data->faqeditor['text'];
        $data->course_summaryformat = intval($data->course_summaryeditor['format']);
        $data->course_summary       = $data->course_summaryeditor['text'];
        $data->video = iframe_cleanvideo($data->video);
        $data->intro = $data->course_summaryeditor['text']; ;
        $data->introformat=intval($data->course_summaryeditor['format']);
        $extension = $mform->get_new_filename('picture');
    }


    if(!isset($data->endcourse))
        $data->endcoursedate = $COURSE->enddate;
    else
        $data->endcoursedate = $data->endcourse;


    $data->picture = './pix/default_course_image.png' ;

    if(empty($data->price)){
        $data->price = NULL;
        $data->currency = NULL;
    }

    $data->timecreated = time();
    $data->timemodified = time();
    $data->id = $DB->insert_record('moodecdescription', $data);

    // Update Course name && end date

    $query = "UPDATE {course} SET fullname=?, shortname=?, enddate=? WHERE id =?" ;
    $param = array($data->coursename, $data->courseshortname, $data->endcoursedate, $data->course);

    $DB->execute($query, $param);


    // You may have to add extra stuff in here.

    // we need to use context now, so we need to make sure all needed info is already in db
    $DB->set_field('course_modules', 'instance', $data->id, array('id'=>$cmid));
    $context = context_module::instance($cmid);
    $coursecontext = context_course::instance($data->course);


    // Enregistrement dans BD de l'image ajoutée au FileManager OU suppression de celle-ci.
    if(isset($data->partnersattachments))
        file_save_draft_area_files($data->partnersattachments, $context->id, 'mod_moodecdescription', 'partnersattachment', 1, moodecdescription_get_image_options($context, 10));

    if(isset($data->attachments))
        file_save_draft_area_files($data->attachments, $coursecontext->id, 'course', 'overviewfiles', 0, moodecdescription_get_image_options($context, 1));


    if ($mform and !empty($data->course_summaryeditor['itemid'])) {

        $draftitemid = $data->course_summaryeditor['format'];
        $data->course_summary = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'course_summary', 0, moodecdescription_get_editor_options($context), $data->course_summary);
        $query = "UPDATE {course} SET summary=? WHERE id =?" ;
        $param = array($data->course_summary,$data->course);
        $DB->execute($query, $param);
    }

    if ($mform and !empty($data->prerequisiteeditor['itemid'])) {
        $draftitemid = $data->prerequisiteeditor['itemid'];

        $data->prerequisite = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'prerequisite', 0, moodecdescription_get_editor_options($context), $data->prerequisite);
        $DB->update_record('moodecdescription', $data);
    }

    if ($mform and !empty($data->syllabuseditor['itemid'])) {
        $draftitemid = $data->syllabuseditor['itemid'];
        $data->syllabus = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'syllabus', 0, moodecdescription_get_editor_options($context), $data->syllabus);
        $DB->update_record('moodecdescription', $data);
    }

    if ($mform and !empty($data->readingeditor['itemid'])) {
        $draftitemid = $data->readingeditor['itemid'];
        $data->reading = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'reading', 0, moodecdescription_get_editor_options($context), $data->reading);
        $DB->update_record('moodecdescription', $data);
    }

    if ($mform and !empty($data->faqeditor['itemid'])) {
        $draftitemid = $data->faqeditor['itemid'];
        $data->faq = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'faq', 0, moodecdescription_get_editor_options($context), $data->faq);
        $DB->update_record('moodecdescription', $data);
    }

    // Refaire la même chose pour les 3 autres editor

    return $data->id ;
}

function deletefilemoodecdescription($id){
    global $DB, $CFG;

    $param = array($id);
    $query = "SELECT picture FROM {moodecdescription} WHERE id= ? ";
    $filename = $DB->get_fieldset_sql($query, $param);

    $uploaddir = "/mod/moodecdescription/pix/thumbnail/";
    $destination = $filename[0];

    if($filename[0]){
        if(strcmp($filename[0],'./pix/default_course_image.png')==0){
            // Egal default image
        }else {
            if(file_exists($destination)) {
                unlink($destination);
            }
        }

    }

    $table = "files";
    $conditions = array("filename"=>$filename[0]);

    $DB->delete_records($table, $conditions) ;


}

/**
 * Updates an instance of the moodecdescription in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $moodecdescription An object from the form in mod_form.php
 * @param mod_moodecdescription_mod_form $mform
 * @return boolean Success/Fail
 */
function moodecdescription_update_instance($data, $mform = null) {
    global $DB, $CFG;

    $cmid  = $data->coursemodule;
    $data->timemodified = time();
    $data->id = $data->instance;
    //$data->tags = $data->coursetags;
    $data->prerequisiteformat = intval($data->prerequisiteeditor['format']);
    $data->prerequisite       = $data->prerequisiteeditor['text'];
    $data->syllabusformat = intval($data->syllabuseditor['format']);
    $data->syllabus       = $data->syllabuseditor['text'];
    $data->endcoursedate = $data->endcourse;
    $data->readingformat = intval($data->readingeditor['format']);
    $data->reading       = $data->readingeditor['text'];
    $data->faqformat = intval($data->faqeditor['format']);
    $data->faq       = $data->faqeditor['text'];
    $data->course_summaryformat = intval($data->course_summaryeditor['format']);
    $data->course_summary       = $data->course_summaryeditor['text'];
    $data->video = iframe_cleanvideo($data->video);

    $data->intro = $data->course_summaryeditor['text']; ;
    $data->introformat=intval($data->course_summaryeditor['format']); ;

    deletefilemoodecdescription($data->instance);

    $query = "UPDATE {course} SET fullname=?, shortname=?, enddate=? WHERE id =?" ;
    $param = array($data->coursename, $data->courseshortname, $data->endcoursedate, $data->course);

    $DB->execute($query, $param);




    if(empty($data->price)){
        $data->price = NULL;
        $data->currency = NULL;
    }

    $DB->update_record('moodecdescription', $data);
    $context = context_module::instance($cmid);
    $coursecontext = context_course::instance($data->course);


    // Enregistrement dans BD de l'image ajoutée au FileManager OU suppression de celle-ci.
    file_save_draft_area_files($data->partnersattachments, $context->id, 'mod_moodecdescription', 'partnersattachment', 1, moodecdescription_get_image_options($context, 10));
    file_save_draft_area_files($data->attachments, $coursecontext->id, 'course', 'overviewfiles', 0, moodecdescription_get_image_options($context, 1));


    if ($data->course_summaryeditor['itemid']) {
        $draftitemid = $data->course_summaryeditor['format'];
        $data->course_summary = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'course_summary', 0, moodecdescription_get_editor_options($context), $data->course_summary);
        $query = "UPDATE {course} SET summary=? WHERE id =?" ;
        $param = array($data->course_summary,$data->course);
        $DB->execute($query, $param);
    }

    if ($data->prerequisiteeditor['itemid']) {
        $draftitemid = $data->prerequisiteeditor['itemid'];
        $data->prerequisite = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'prerequisite', 0, moodecdescription_get_editor_options($context), $data->prerequisite);
        $DB->update_record('moodecdescription', $data);
    }

    if ($data->syllabuseditor['itemid']) {
        $draftitemid = $data->syllabuseditor['itemid'];
        $data->syllabus = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'syllabus', 0, moodecdescription_get_editor_options($context), $data->syllabus);
        $DB->update_record('moodecdescription', $data);
    }

    if ($data->readingeditor['itemid']) {
        $draftitemid = $data->readingeditor['itemid'];
        $data->reading = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'reading', 0, moodecdescription_get_editor_options($context), $data->reading);
        $DB->update_record('moodecdescription', $data);
    }

    if ($data->faqeditor['itemid']) {
        $draftitemid = $data->faqeditor['itemid'];
        $data->faq = file_save_draft_area_files($draftitemid, $context->id, 'mod_moodecdescription', 'faq', 0, moodecdescription_get_editor_options($context), $data->faq);
        $DB->update_record('moodecdescription', $data);
    }


    // You may have to add extra stuff in here.

    return true;

}

function iframe_cleanvideo($url) {

    $url = strip_tags($url,"<iframe>");  // interpret and keep only iframe tags

    $url = strstr($url, '<iframe'); // Delete everything before iframe
    $length = strpos($url, "iframe>")+7 ;
    $url = substr($url, 0, $length); // Delete everything after iframe
    if($url == "0"){ // Check if no balise iframe
        $url = "" ;
    }
    return $url ;

}

/**
 * Removes an instance of the moodecdescription from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function moodecdescription_delete_instance($id) {
    global $DB;

    if (! $moodecdescription = $DB->get_record('moodecdescription', array('id' => $id))) {
        return false;
    }


    deletefilemoodecdescription($id);

    // Delete any dependent records here.

    $DB->delete_records('moodecdescription', array('id' => $moodecdescription->id));

    return true;
}

/**
 * Returns a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @return stdClass|null
 */
function moodecdescription_user_outline($course, $user, $mod, $moodecdescription) {

    $return = new stdClass();
    $return->time = 0;
    $return->info = '';
    return $return;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * @param stdClass $course the current course record
 * @param stdClass $user the record of the user we are generating report for
 * @param cm_info $mod course module info
 * @param stdClass $moodecdescription the module instance record
 * @return void, is supposed to echp directly
 */
function moodecdescription_user_complete($course, $user, $mod, $moodecdescription) {
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in moodecdescription activities and print it out.
 * Return true if there was output, or false is there was none.
 *
 * @return boolean
 */
function moodecdescription_print_recent_activity($course, $viewfullnames, $timestart) {
    return false; // True if anything was printed, otherwise false.
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link moodecdescription_print_recent_mod_activity()}.
 *
 * @param array $activities sequentially indexed array of objects with the 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $course the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 * @return void adds items into $activities and increases $index
 */
function moodecdescription_get_recent_mod_activity(&$activities, &$index, $timestart, $course, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@see moodecdescription_get_recent_mod_activity()}
 *
 * @return void
 */
function moodecdescription_print_recent_mod_activity($activity, $course, $detail, $modnames, $viewfullnames) {
}

/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * @return boolean
 * @todo Finish documenting this function
 **/
function moodecdescription_cron () {
    return true;
}

/**
 * Returns all other caps used in the module
 *
 * @example return array('moodle/site:accessallgroups');
 * @return array
 */
function moodecdescription_get_extra_capabilities() {
    return array();
}

/**
 * Gradebook API                                                              //
 */

/**
 * Is a given scale used by the instance of moodecdescription?
 *
 * This function returns if a scale is being used by one moodecdescription
 * if it has support for grading and scales. Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $moodecdescriptionid ID of an instance of this module
 * @return bool true if the scale is used by the given moodecdescription instance
 */
function moodecdescription_scale_used($moodecdescriptionid, $scaleid) {
    global $DB;

    return false ;
}

/**
 * Checks if scale is being used by any instance of moodecdescription.
 *
 * This is used to find out if scale used anywhere.
 *
 * @param $scaleid int
 * @return boolean true if the scale is used by any moodecdescription instance
 */
function moodecdescription_scale_used_anywhere($scaleid) {

    return false ;
}

/**
 * Creates or updates grade item for the give moodecdescription instance
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass $moodecdescription instance object with extra cmidnumber and modname property
 * @param mixed optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return void
 */
function moodecdescription_grade_item_update(stdClass $moodecdescription, $grades=null) {

    return false ;
}

/**
 * Update moodecdescription grades in the gradebook
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass $moodecdescription instance object with extra cmidnumber and modname property
 * @param int $userid update grade of specific user only, 0 means all participants
 * @return void
 */
function moodecdescription_update_grades(stdClass $moodecdescription, $userid = 0) {

    return false ;
}

/**
 * File API                                                                   //
 */

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function moodecdescription_get_file_areas($course, $cm, $context) {
    return array();
}

/**
 * File browsing support for moodecdescription file areas
 *
 * @package mod_nmoodecdescription
 * @category files
 *
 * @param file_browser $browser
 * @param array $areas
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param int $itemid
 * @param string $filepath
 * @param string $filename
 * @return file_info instance or null if not found
 */
function moodecdescription_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename) {
    return null;
}

/**
 * Serves the files from the moodecdescription file areas
 *
 * @package mod_moodecdescription
 * @category files
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the moodecdescription's context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 */
function moodecdescription_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options=array()) {
    global $DB, $CFG;


    error_log("Je passe dans la méthode", 0);

// Check the contextlevel is as expected - if your plugin is a block, this becomes CONTEXT_BLOCK, etc.
    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }

    error_log("Je passe dans le contexte", 0);
    // Make sure the filearea is one of those used by the plugin.
    if ($filearea !== 'partnersattachment' && $filearea !== 'attachment' && $filearea !== 'prerequisite' && $filearea !== 'syllabus' && $filearea !== 'reading' && $filearea !== 'faq' && $filearea !='thumbnail' ) {
        return false;
    }


    // Make sure the user is logged in and has access to the module (plugins that are not course modules should leave out the 'cm' part).
    //require_login($course, true, $cm);

    // Check the relevant capabilities - these may vary depending on the filearea being accessed.
    if (!has_capability('mod/moodecdescription:view', $context)) {
        return false;
    }
    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }


    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'mod_moodecdescription', $filearea, $itemid, $filepath, $filename);



    if (!$file) {
        return false; // The file does not exist.
    }

    // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering.
    // From Moodle 2.3, use send_stored_file instead.
    send_stored_file($file, 86400, 0, $forcedownload, $options);
}


/**
 * Navigation API                                                             //
 */

/**
 * Extends the global navigation tree by adding moodecdescription nodes if there is a relevant content
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $navref An object representing the navigation tree node of the moodecdescription module instance
 * @param stdClass $course
 * @param stdClass $module
 * @param cm_info $cm
 */
function moodecdescription_extend_navigation(navigation_node $navref, stdclass $course, stdclass $module, cm_info $cm) {
}

/**
 * Extends the settings navigation with the moodecdescription settings
 *
 * This function is called when the context for the page is a moodecdescription module. This is not called by AJAX
 * so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav {@link settings_navigation}
 * @param navigation_node $moodecdescription {@link navigation_node}
 */
function moodecdescription_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $moodecdescriptionnode=null) {
}

function get_teachers($course) {

    global $DB ;

    $query = "SELECT u.id, u.username, u.firstname, u.lastname
                FROM {user} AS u, {role_assignments} AS ra, {course} AS c, {context} AS ct
                WHERE (ra.roleid = 3 OR ra.roleid = 4) 
                AND ra.userid = u.id
                AND c.id = ?
                AND ct.id = ra.contextid
                AND c.id = ct.instanceid";

    $param = array($course);

    $teachers = $DB->get_records_sql($query, $param);


    return $teachers ;


}

function moodecdescription_reset_userdata($data) {
    global $DB, $COURSE ;

    if (!empty($data->reset_description)) {
        $description = $DB->get_record("moodecdescription", array("course"=>$data->courseid));
        $description->picture = "./pix/default_course_image.png" ;
        $description->effort = 0;
        $description->prerequisite = "";
        $description->syllabus = "";
        $description->reading = "";
        $description->faq = "";
        //$description->tags = "";
        $description->duration = 0;
        $description->video = "";
        $DB->update_record("moodecdescription", $description);
        $componentstr = get_string('modulename', 'moodecdescription');
        $status[] = array('component'=>$componentstr, 'item'=>get_string('reset', 'moodecdescription'), 'error'=>false);
    }
    return $status;
}

function moodecdescription_reset_course_form_defaults($course) {
    return array('reset_description'=>0);
}

function moodecdescription_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'moodecdescriptionheader', get_string('modulename', 'moodecdescription'));
    $mform->addElement('checkbox', 'reset_description', get_string('reset','moodecdescription'));

}