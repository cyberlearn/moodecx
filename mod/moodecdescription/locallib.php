<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Internal library of functions for module moodecdescription
 *
 * All the moodecdescription specific functions, needed to implement the module
 * logic, should go here. Never include this file from your lib.php!
 *
 * @package    mod_moodecdescription
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
//error_log("locallib.php", 0);
/*
 * Does something really useful with the passed things
 *
 * @param array $things
 * @return object
 *function moodecdescription_do_something_useful(array $things) {
 *    return new stdClass();
 *}
 */

function moodecdescription_get_editor_options($context) {
    global $CFG;
    return array('subdirs'=>1,'enable_filemanagement' => false, 'maxbytes'=>$CFG->maxbytes, 'maxfiles'=>-1, 'changeformat'=>1, 'context'=>$context, 'noclean'=>1, 'trusttext'=>0);
}

function moodecdescription_get_image_options($context, $maxFiles) {
    global $CFG;
    return array('subdirs' => 0, 'maxbytes' => '0', 'maxfiles' => $maxFiles, 'context' => $context, 'accepted_types' => array('.jpg', '.png', '.gif'));
}


function moodecdescription_check_instance() {
    global $DB, $COURSE, $USER ;

    $query = "SELECT id from {modules} WHERE name = ?";
    $moduleid = $DB->get_fieldset_sql($query,$param=array('moodecdescription'));
    $moduleid = $moduleid[0];

    $query = "SELECT id from {course_modules} WHERE course = ? and module=?";

    $param = array($COURSE->id, $moduleid);
    $editid = $DB->get_fieldset_sql($query, $param);
    if(!empty($editid)) {
        $editid = $editid[0];
    }
    return $editid ;
}

function moodecdescription_get_course_tag($context) {
    global $DB ;

    $query = "SELECT t.name, ti.tagid, ti.itemid, ti.contextid from {tag_instance} as ti, {tag} as t 
              WHERE t.id = ti.tagid  AND ti.itemid = ? AND ti.contextid = ? ";
    $tags = $DB->get_fieldset_sql($query,$param=array($context->instanceid, $context->id));

    return $tags ;

}
