
<?php
/**
 * User: Martin Tazlari  @Cyberlearn
 * Date: 01.12.2015
 * Time: 11:35
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once('../../../config.php');
require_once('../lib.php');

/// get url variables
$id = optional_param('id', 0, PARAM_INT);


$PAGE->set_pagelayout('admin');



$records = display_group_ip_location($id);

echo json_encode($records);