<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package    mod
 * @subpackage moodecgrpmanagement
 * @copyright  2013 Université de Lausanne
 * @author     Nicolas Dunand <Nicolas.Dunand@unil.ch>
 * @modifed by Martin Tazlari
 * @copyright 2016 Cyberlearn
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once("../../config.php");
require_once("lib.php");

$id         = required_param('id', PARAM_INT);   //moduleid
$format     = optional_param('format', moodecgrpmanagement_PUBLISH_NAMES, PARAM_INT);
$download   = optional_param('download', '', PARAM_ALPHA);
$action     = optional_param('action', '', PARAM_ALPHA);
$userids = optional_param_array('userid', array(), PARAM_INT); //get array of responses to delete.

$url = new moodle_url('/mod/moodecgrpmanagement/report.php', array('id'=>$id));
if ($format !== moodecgrpmanagement_PUBLISH_NAMES) {
    $url->param('format', $format);
}
if ($download !== '') {
    $url->param('download', $download);
}
if ($action !== '') {
    $url->param('action', $action);
}
$PAGE->set_url($url);

if (! $cm = get_coursemodule_from_id('moodecgrpmanagement', $id)) {
    print_error("invalidcoursemodule");
}

if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
    print_error("coursemisconf");
}

require_login($course->id, false, $cm);

$context = context_module::instance($cm->id);

require_capability('mod/moodecgrpmanagement:readresponses', $context);

if (!$moodecgrpmanagement = moodecgrpmanagement_get_moodecgrpmanagement($cm->instance)) {
    print_error('invalidcoursemodule');
}

$strmoodecgrpmanagement = get_string("modulename", "moodecgrpmanagement");
$strmoodecgrpmanagements = get_string("modulenameplural", "moodecgrpmanagement");
$strresponses = get_string("responses", "moodecgrpmanagement");

$eventparams = array(
    'context' => $context,
    'objectid' => $moodecgrpmanagement->id
);
$event = \mod_moodecgrpmanagement\event\report_viewed::create($eventparams);
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('moodecgrpmanagement', $moodecgrpmanagement);
$event->trigger();

if (data_submitted() && $action == 'delete' && has_capability('mod/moodecgrpmanagement:deleteresponses',$context) && confirm_sesskey()) {
    moodecgrpmanagement_delete_responses($userids, $moodecgrpmanagement, $cm, $course); //delete responses.
    redirect("report.php?id=$cm->id");
}

if (!$download) {
    $PAGE->navbar->add($strresponses);
    $PAGE->set_title(format_string($moodecgrpmanagement->name).": $strresponses");
    $PAGE->set_heading($course->fullname);
    echo $OUTPUT->header();
    /// Check to see if groups are being used in this moodecgrpmanagement
    $groupmode = groups_get_activity_groupmode($cm);
    if ($groupmode) {
        groups_get_activity_group($cm, true);
        groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/moodecgrpmanagement/report.php?id='.$id);
    }
} else {
    $groupmode = groups_get_activity_groupmode($cm);
    $groups = moodecgrpmanagement_get_groups($moodecgrpmanagement);
    $groups_ids = array();
    foreach($groups as $group) {
        $groups_ids[] = $group->id;
    }
}
$users = moodecgrpmanagement_get_response_data($moodecgrpmanagement, $cm, $groupmode);

if ($download == "ods" && has_capability('mod/moodecgrpmanagement:downloadresponses', $context)) {
    require_once("$CFG->libdir/odslib.class.php");

/// Calculate file name
    $filename = clean_filename("$course->shortname ".strip_tags(format_string($moodecgrpmanagement->name,true))).'.ods';
/// Creating a workbook
    $workbook = new MoodleODSWorkbook("-");
/// Send HTTP headers
    $workbook->send($filename);
/// Creating the first worksheet
    $myxls = $workbook->add_worksheet($strresponses);

/// Print names of all the fields
    $myxls->write_string(0,0,get_string("lastname"));
    $myxls->write_string(0,1,get_string("firstname"));
    $myxls->write_string(0,2,get_string("idnumber"));
    $myxls->write_string(0,3,get_string("email"));
    $myxls->write_string(0,4,get_string("group"));
    $myxls->write_string(0,5,get_string("choice","moodecgrpmanagement"));

/// generate the data for the body of the spreadsheet
    $i=0;
    $row=1;
    if ($users) {
        $displayed = array();
        foreach ($users as $option => $userid) {
            foreach($userid as $user) {
                if (in_array($user->id, $displayed)) {
                    continue;
                }
                $displayed[] = $user->id;
                $myxls->write_string($row,0,$user->lastname);
                $myxls->write_string($row,1,$user->firstname);
                $studentid=(!empty($user->idnumber) ? $user->idnumber : " ");
                $myxls->write_string($row,2,$studentid);
                $myxls->write_string($row,3,$user->email);
                $ug2 = array();
                if ($usergrps = groups_get_all_groups($course->id, $user->id)) {
                    foreach ($groups_ids as $gid) {
                        if (array_key_exists($gid, $usergrps)) {
                            $ug2[] = $usergrps[$gid]->name;
                        }
                    }
                }
                $myxls->write_string($row, 4, implode(', ', $ug2));
                $row++;
                $pos=5;
            }
        }
    }
    /// Close the workbook
    $workbook->close();

    exit;
}

//print spreadsheet if one is asked for:
if ($download == "xls" && has_capability('mod/moodecgrpmanagement:downloadresponses', $context)) {
    require_once("$CFG->libdir/excellib.class.php");

/// Calculate file name
    $filename = clean_filename("$course->shortname ".strip_tags(format_string($moodecgrpmanagement->name,true))).'.xls';
/// Creating a workbook
    $workbook = new MoodleExcelWorkbook("-");
/// Send HTTP headers
    $workbook->send($filename);
/// Creating the first worksheet
    // assigning by reference gives this: Strict standards: Only variables should be assigned by reference in /data_1/www/html/moodle/moodle/mod/moodecgrpmanagement/report.php on line 157
    // removed the ampersand.
    $myxls = $workbook->add_worksheet($strresponses);
/// Print names of all the fields
    $myxls->write_string(0,0,get_string("lastname"));
    $myxls->write_string(0,1,get_string("firstname"));
    $myxls->write_string(0,2,get_string("idnumber"));
    $myxls->write_string(0,3,get_string("email"));
    $myxls->write_string(0,4,get_string("group"));
    $myxls->write_string(0,5,get_string("choice","moodecgrpmanagement"));


/// generate the data for the body of the spreadsheet
    $i=0;
    $row=1;
    if ($users) {
        $displayed = array();
        foreach ($users as $option => $userid) {
            foreach($userid as $user) {
                if (in_array($user->id, $displayed)) {
                    continue;
                }
                $displayed[] = $user->id;
                $myxls->write_string($row,0,$user->lastname);
                $myxls->write_string($row,1,$user->firstname);
                $studentid=(!empty($user->idnumber) ? $user->idnumber : " ");
                $myxls->write_string($row,2,$studentid);
                $myxls->write_string($row,3,$user->email);
                $ug2 = array();
                if ($usergrps = groups_get_all_groups($course->id, $user->id)) {
                    foreach ($groups_ids as $gid) {
                        if (array_key_exists($gid, $usergrps)) {
                            $ug2[] = $usergrps[$gid]->name;
                        }
                    }
                }
                $myxls->write_string($row, 4, implode(', ', $ug2));
                $row++;
            }
        }
        $pos=5;
    }
    /// Close the workbook
    $workbook->close();
    exit;
}

// print text file
if ($download == "txt" && has_capability('mod/moodecgrpmanagement:downloadresponses', $context)) {
    $filename = clean_filename("$course->shortname ".strip_tags(format_string($moodecgrpmanagement->name,true))).'.txt';

    header("Content-Type: application/download\n");
    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Expires: 0");
    header("Cache-Control: must-revalidate,post-check=0,pre-check=0");
    header("Pragma: public");

    /// Print names of all the fields

    echo get_string("firstname")."\t".get_string("lastname") . "\t". get_string("idnumber") . "\t";
    echo get_string("group"). "\t";
    echo get_string("choice","moodecgrpmanagement"). "\n";

    /// generate the data for the body of the spreadsheet
    $i=0;
    if ($users) {
        $displayed = array();
        foreach ($users as $option => $userid) {
            foreach($userid as $user) {
                if (in_array($user->id, $displayed)) {
                    continue;
                }
                $displayed[] = $user->id;
                echo $user->lastname;
                echo "\t".$user->firstname;
                $studentid = " ";
                if (!empty($user->idnumber)) {
                    $studentid = $user->idnumber;
                }
                echo "\t". $studentid."\t";
                $ug2 = array();
                if ($usergrps = groups_get_all_groups($course->id, $user->id)) {
                    foreach ($groups_ids as $gid) {
                        if (array_key_exists($gid, $usergrps)) {
                            $ug2[] = $usergrps[$gid]->name;
                        }
                    }
                }
                echo implode(', ', $ug2) . "\t";
                echo "\n";
            }
        }
    }
    exit;
}
// Show those who haven't answered the question.
if (!empty($moodecgrpmanagement->showunanswered)) {
    $moodecgrpmanagement->option[0] = get_string('notanswered', 'moodecgrpmanagement');
    $moodecgrpmanagement->maxanswers[0] = 0;
}

$results = prepare_moodecgrpmanagement_show_results($moodecgrpmanagement, $course, $cm, $users);
$renderer = $PAGE->get_renderer('mod_moodecgrpmanagement');
echo $renderer->display_result($results, has_capability('mod/moodecgrpmanagement:readresponses', $context));

//now give links for downloading spreadsheets.
if (!empty($users) && has_capability('mod/moodecgrpmanagement:downloadresponses',$context)) {

    // Create table to store buttons
    $tablebutton = new html_table();
    $tablebutton->attributes['class'] = 'downloadreport';
    $btndownloadods = $OUTPUT->single_button(new moodle_url("report.php", array('id'=>$cm->id, 'download'=>'ods')), get_string("downloadods"));
    $btndownloadxls = $OUTPUT->single_button(new moodle_url("report.php", array('id'=>$cm->id, 'download'=>'xls')), get_string("downloadexcel"));
    $btndownloadtxt = $OUTPUT->single_button(new moodle_url("report.php", array('id'=>$cm->id, 'download'=>'txt')), get_string("downloadtext"));
    $tablebutton->data[] = array($btndownloadods, $btndownloadxls, $btndownloadtxt);


		echo html_writer::tag('div', $btndownloadods.$btndownloadxls. $btndownloadtxt, array('class' => 'buttons'));
}

echo $OUTPUT->footer();

