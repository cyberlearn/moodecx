General information
====================

This activity is heavily based on the " Group choice" activity module, and behaves roughly like it. Making a choice enrols you in a group, changing your choice unenrols you from the precedent group and enrols you in the new one, and so on.

This module allows students to enrol themselves in a group within a course. The teacher can choose from which groups the students can chose, and the maximum nummber of students allowed in each group.

The students can view the members of each group before making a choice, and (if the teacher allows it) change their selected group until the deadline.
They can alswo create their own public or private group protected with a key. Other student can contact the group creator for asking the registration Key.

Each group creator can add informations about his group a description, a video or an image. A map with the group's members location will also be display in a group Detail page.

Installation
=============

1. unzip, and copy into Moodle's /mod folder
2. visit administration page to install module
3. use in any course as wished

Features
==========

1. create groups within your course
2. create a Group Management activity and select groups which users can chose from
3. student can also create group
4. create a group description page with informations about the groups members


