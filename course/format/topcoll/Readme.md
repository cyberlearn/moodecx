﻿﻿Important !
============
Une fois le changement de version effectué, il est nécessaire d'ajouter ce code à la fin du fichier ./badges/renderer.php :


/**
 * Collection of all badges for view.php page
 */
class badge_collection implements renderable {

    /** @var string how are the data sorted */
    public $sort = 'name';

    /** @var string how are the data sorted */
    public $dir = 'ASC';

    /** @var int page number to display */
    public $page = 0;

    /** @var int number of badges to display per page */
    public $perpage = BADGE_PERPAGE;

    /** @var int the total number of badges to display */
    public $totalcount = null;

    /** @var array list of badges */
    public $badges = array();

    /**
     * Initializes the list of badges to display
     *
     * @param array $badges Badges to render
     */
    public function __construct($badges) {
        $this->badges = $badges;
    }
}

/**
 * Collection of badges used at the index.php page
 */
class badge_management extends badge_collection implements renderable {
}

/**
 * Collection of user badges used at the mybadges.php page
 */
class badge_user_collection extends badge_collection implements renderable {
    /** @var array backpack settings */
    public $backpack = null;

    /** @var string search */
    public $search = '';

    /**
     * Initializes user badge collection.
     *
     * @param array $badges Badges to render
     * @param int $userid Badges owner
     */
    public function __construct($badges, $userid) {
        global $CFG;
        parent::__construct($badges);

        if (!empty($CFG->badges_allowexternalbackpack)) {
            $this->backpack = get_backpack_settings($userid, true);
        }
    }
}
