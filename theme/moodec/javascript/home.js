/**
 * Created by christop.hadorn on 06.10.2015.
 */

$(function () {
	
    // Get course list most popular
    getCourseListTop(8);
    getFutureCourseListTop(8);

    var idCourse = gup('courseid', location.href);
	var idUser = $("#idUser").val();

	if(idCourse){
		getCourse(idCourse,idUser);
	}

	$('.closePopUp').on('click', function () {
		window.location.hash="";
	})
});
