<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace theme_moodec\output;

use coding_exception;
use html_writer;
use tabobject;
use tabtree;
use custom_menu_item;
use custom_menu;
use block_contents;
use navigation_node;
use action_link;
use stdClass;
use moodle_url;
use preferences_groups;
use action_menu;
use help_icon;
use single_button;
use single_select;
use paging_bar;
use url_select;
use context_course;
use pix_icon;
use theme_config;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot ."/course/renderer.php");
require_once($CFG->libdir.'/coursecatlib.php');

/**
 * Renderers to align Moodle's HTML with that expected by Bootstrap
 *
 * @package    theme_moodec
 * @copyright  2012 Bas Brands, www.basbrands.nl
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


class core_renderer extends \theme_boost\output\core_renderer {

    public function course_title()
	{
		global $PAGE;
		$html = html_writer::start_div('region-main-header');
        $html .= $this->context_header();
        $html .= html_writer::end_div();
		return $html;
	}
	
	/**
     * Wrapper for header elements.
     *
     * @return string HTML to display the main header.
     */
    public function full_header() {

        global $PAGE, $COURSE;
        
        $html = html_writer::start_tag('header', array('id' => 'page-header', 'class' => 'row'));
        $html .= html_writer::start_div('col-xs-12 p-a-1');
        $html .= html_writer::start_div('card');
        $html .= html_writer::start_div('card-block');
		

        $pageheadingbutton = $this->page_heading_button();
        if (empty($PAGE->layout_options['nonavbar'])) {
            $html .= html_writer::start_div('clearfix w-100 pull-xs-left', array('id' => 'page-navbar'));
            $html .= html_writer::tag('div', $this->navbar(), array('class' => 'breadcrumb-nav'));

            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button pull-xs-right');
            $html .= html_writer::end_div();
        } else if ($pageheadingbutton) {
            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button nonavbar pull-xs-right');
        }
       $html .= html_writer::tag('div', $this->course_header(), array('id' => 'course-header'));
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_tag('header');
        return $html;
    }
	
	
	
	public function frontpage_header() {
		global $PAGE, $COURSE;
		if(isloggedin() && is_siteadmin()) 
		{
			$html = html_writer::start_tag('header', array('id' => 'page-header', 'class' => 'row'));
			$html .= html_writer::start_div('col-xs-12 p-a-1');
			$html .= html_writer::start_div('card');
			$html .= html_writer::start_div('card-block');
			
			if (isset($COURSE->id) && $COURSE->id == 1) {
				$html .= html_writer::div($this->context_header_settings_menu(), 'pull-xs-right context-header-settings-menu');
			}
			$pageheadingbutton = $this->page_heading_button();
			$html .= html_writer::div($pageheadingbutton, 'breadcrumb-button nonavbar pull-xs-right');
			
			$html .= html_writer::end_div();
			$html .= html_writer::end_div();
			$html .= html_writer::end_div();
			$html .= html_writer::end_tag('header');
			return $html;
		}
	}
	
	
	public function dashboard_header() {

        global $PAGE, $COURSE;
        
        $html = html_writer::start_tag('header', array('id' => 'page-header', 'class' => 'row mymoodle'));
        $html .= html_writer::start_div('col-xs-12 p-a-1');
        $html .= html_writer::start_div('card');
        $html .= html_writer::start_div('card-block');
		
		$pageheadingbutton = $this->page_heading_button();
		
        $html .= html_writer::start_div('clearfix w-100 pull-xs-left', array('id' => 'page-navbar'));
        $html .= html_writer::tag('div', get_string('mymoodle', 'admin'), array('class' => 'titleDashboard'));
        
		$nbrBtn = substr_count($pageheadingbutton, 'singlebutton');
		if($nbrBtn == 1)
		{
			$pageheadingbutton = str_replace("singlebutton", "singlebutton editBtn", $pageheadingbutton);
		}
		else
		{
			$pageheadingbutton = preg_replace('/singlebutton/', 'singlebutton resetBtn', $pageheadingbutton, 1);
			if( ( $pos = strrpos( $pageheadingbutton , 'singlebutton' ) ) !== false ) {
				$search_length  = strlen( 'singlebutton' );
				$pageheadingbutton  = substr_replace( $pageheadingbutton , 'singlebutton cancelBtn' , $pos , $search_length );
			}
		}
		
		$html .= html_writer::div($pageheadingbutton, 'breadcrumb-button btn-mode-edition pull-xs-right');
        $html .= html_writer::end_div();
        
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_tag('header');

		return $html;
    }
	
	

    public function image_url($imagename, $component = 'moodle') {
        // Strip -24, -64, -256  etc from the end of filetype icons so we
        // only need to provide one SVG, see MDL-47082.
        $imagename = \preg_replace('/-\d\d\d?$/', '', $imagename);
        return $this->page->theme->image_url($imagename, $component);
    }

    public function header_navbar() {
        global $PAGE;


		$html = '';
        $pageheadingbutton = $this->page_heading_button();
		if (empty($PAGE->layout_options['nonavbar'])) {
            $html .= html_writer::start_div('clearfix w-100 pull-xs-left', array('id' => 'page-navbar'));
            $html .= html_writer::tag('div', $this->navbar(), array('class' => 'breadcrumb-nav'));
            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button pull-xs-right');
            $html .= html_writer::end_div();
        } else if ($pageheadingbutton) {
            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button nonavbar pull-xs-right');
        }
		
        $html .= html_writer::tag('div', $this->course_header(), array('id' => 'course-header'));
        
        return $html;
    }
	
	/*
     * This renders the navbar.
     * Uses bootstrap compatible html.
     */
    public function navbar() {
        return $this->render_from_template('core/navbar', $this->page->navbar);
    }

	/*
     * 
     * add edit on 
     */
    public function edit_button(moodle_url $url) {
        global $SITE, $PAGE, $USER, $CFG, $COURSE;
        $url->param('sesskey', sesskey());
        if ($this->page->user_is_editing()) {
            $url->param('edit', 'off');
            $btn = 'btn-edit-off';
            $title = get_string('turneditingoff' , 'moodle');
            $icon = 'fa-edit-off';
        } else {
            $url->param('edit', 'on');
            $btn = 'btn-edit-on';
            $title = get_string('turneditingon' , 'moodle');
            $icon = 'fa-edit-on';
        }
        return html_writer::tag('a', html_writer::start_tag('i', array('class' => $icon . ' fa fa-fw')) .
            html_writer::end_tag('i') . $title, array('href' => $url, 'class' => 'btn  ' . $btn, 'title' => $title));
        return $output;
    }
	
	public function costom_loginBox() {
		$loginpage = $this->is_login_page();
		$loginurl = get_login_url();
		
		$returnstr = "";
		$usermenulist = "";
		$usertxtlogin = '<span id="usertext">';
		$userpiclogin = '';
		$usertxtlogintmp = '';
		
		if (during_initial_install()) {return $returnstr;}
		
		
		if ($loginpage) {
			
		}
		$usertxtlogintmp = get_string('loggedinnot');
		if (isguestuser()) 
		{
			$usertxtlogintmp = get_string('loggedinasguest');
		}
		$userpiclogin = html_writer::tag('img', '',
			array(
				'src'=>$this->image_url('i/user'),
				'class'=>'userpicture',
				'alt'=>get_string('login'),
				'title'=>$usertxtlogintmp
			)
		);
		$returnstr = '<ul id="loginbox" class="nav"><li class="dropdown usermenuonlogged"><a href="#" class="dropdown-toggle" data-toggle="dropdown" title="usermenuonlogged">';
		$returnstr .= $usertxtlogin.$usertxtlogintmp.'</span>';
		$returnstr .= '<i class="fas fa-user-circle" aria-hidden="true"></i></a>';
		
		if (strpos($_SERVER['REQUEST_URI'], "login") == false)
		{
			$returnstr .= '<ul class="dropdown-menu" id="loginpaneldropdown">';

			$returnstr .= '<li class="loginpanel">
							<form action="'.new moodle_url('/login/index.php').'" method="post" id="login">';
							
			if($this->has_identity_providers())
			{
				$returnstr .= '<div class="potentialidplist" class="m-t-1">';
				foreach ($this->get_identity_providers() as $provider) {
					$returnstr .= '<div class="potentialidp">';
					$returnstr .= '<a href="'.$provider['url'].'" ';
					$returnstr .= 'title="'.$provider['name'].'" class="btn btn-secondary btn-block '.$provider['name'].'">';
					if(isset($provider['iconurl']))
					{
						$returnstr .= '<img src="'.$provider['iconurl'].'" alt="" width="24" height="24"/>';
					}
					
					$returnstr .= $provider['name'];
					$returnstr .= '</a>';
					$returnstr .= '</div>';
				}


				$returnstr .= '</div>';
				$returnstr .= '<div class="loginPanelSep sep"><div class="septxt">'.get_string('loginSep','theme_moodec').'</div></div>';				
			}				
			$returnstr .= '<div class="loginform">
							<div class="input-group input-group-username">
								<span class="input-group-addon">
									<i class="fa fa-user-circle"></i>
								</span>
								<input type="text" placeholder="'.get_string('username').'" name="username" id="username" class="form-control" size="15" value="">
							</div>
							<div class="input-group input-group-password">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" placeholder="'.get_string('password').'" name="password" id="password" class="form-control" size="15" value="">
							</div>
							<button type="submit" id="loginsubmitbtn" class="btn btn-primary btn-block" form="login" value="'.get_string('login').'">'.get_string('login').'</button>
							</div>
							
							<div class="clearer"><!-- --></div>';
			$returnstr .= '</form></li>';

			$returnstr .= '<li class="signup">
								<div class="forgetpass">
									<a href="'.new moodle_url('/login/forgot_password.php').'" class="linkLostPass">'.get_string('loginPanelForgotten', 'theme_moodec').'</a>
									<a href="'.new moodle_url('/login/signup.php').'" class="linkInscription">'.get_string('startsignup').'</a>
								</div>
							</li>';
			$returnstr .= '</li></ul>';
		}
		$returnstr .= '</li></ul>';
		return $returnstr;
	}
	
	/**
     * Verify whether the site has identity providers
     *
     * @return mixed
     */
    public function has_identity_providers() {
        global $CFG;

        $authsequence = get_enabled_auth_plugins(true);

        require_once($CFG->libdir . '/authlib.php');

        $identityproviders = \auth_plugin_base::get_identity_providers($authsequence);

        return !empty($identityproviders);
    }
	
	/**
     * Return the site identity providers
     *
     * @return mixed
     */
    public function get_identity_providers() {
        global $CFG;

        $authsequence = get_enabled_auth_plugins(true);

        require_once($CFG->libdir . '/authlib.php');

        $identityproviders = \auth_plugin_base::get_identity_providers($authsequence);

        return $identityproviders;
    }
	
	
	
	
	
	
	
	
	
	
	
    /*
     * This renders the bootstrap top menu.
     *
     * This renderer is needed to enable the Bootstrap style navigation.
     */
    protected function render_custom_menu(custom_menu $menu) {
        global $CFG;
        $context = $this->page->context;



        if (isloggedin() && !isguestuser()) {

			
            $branchlabel = get_string('mymoocs', 'theme_moodec');
			$branchtitle = $branchlabel;
            $branchurl   = new moodle_url('/my/index.php');
            $branchsort  = 10000;
 
            $branch = $menu->add($branchlabel, $branchurl, $branchtitle, $branchsort);
            
            if ($courses = enrol_get_my_courses(NULL, 'fullname ASC')) {
                foreach ($courses as $course) {
                    if ($course->visible){
                        $branch->add(format_string($course->fullname), new moodle_url('/course/view.php?id='.$course->id), format_string($course->shortname));
                    }
                }
            } else {
                $noenrolments = get_string('noenrolments', 'theme_moodec');
                $branch->add('<em>'.$noenrolments.'</em>', new moodle_url('/'), $noenrolments);
            }

        }

        if (!$menu->has_children()) {
            return '';
        }
		
		$content = '';
        foreach ($menu->get_children() as $item) {
            $context = $item->export_for_template($this);
            $content .= $this->render_from_template('core/custom_menumycourse_item', $context);
        }

        return $content;
    }
	public function custom_menu_admin()
	{
		global $CFG;
		$html  = '';
		if (is_siteadmin()) {
			$branchtitle = get_string('administration', 'theme_moodec');
			$branchlabel = '<span class="txt">'.$branchtitle.'</span>';
			$branchurl = new moodle_url('/admin/search.php');
			
		
			$html = html_writer::start_tag('li', array('class' => 'nav-item admin'));
			$html .= html_writer::tag('a',  $branchlabel, array('href' => $branchurl, 'class' => 'nav-item nav-link', 'title' => $branchtitle));
			$html .= html_writer::end_tag('li');
			

                        
		}
		return $html;
	}

	
	
	public function custom_menu_header( $idSelected=0) {
		//echo "<script>console.log( 'idSelected = " . $idSelected . "' );</script>";
		
		if(!isset($idSelected))
			$idSelected = 0;
		
		$menuContent = '';
		$cMenuHeader = '<div id="banner-menu"><ul id="banner-menu-ul">';


		$menuUrl = new moodle_url('/?redirect=0');
		$menuLabel = get_string('home', 'theme_moodec');
		$paremLi = array('id'=>'menu1', 'class'=>'menu_selected');
		if($idSelected == 1)
		{
			$paremLi = array('id'=>'menu1', 'class'=>'menu_selected');
			$menuContent = html_writer::tag('span', '', array('id'=>'menuicon_home', 'class'=>'menuicon', 'title'=>$menuLabel));
			$menuContent .= html_writer::tag('span',$menuLabel);
			$menuContent = '<div id="banner-menu-item">'.$menuContent.'</div>';
		}
		else
		{
			$paremLi = array('id'=>'menu1');
			$menuContent = html_writer::link($menuUrl, '<span id="menuicon_home" class="menuicon" title="'.$menuLabel.'"></span><span>'.$menuLabel.'</span>', array('id' => 'banner-menu-item'));
		}
		$cMenuHeader .= html_writer::tag('li', $menuContent, $paremLi);
		$paremLi = array();

		
		$menuUrl = new moodle_url('/?redirect=0&categoryid=1');
		$menuLabel = get_string('catalog', 'theme_moodec');
		if($idSelected == 2)
		{
			$paremLi = array('id'=>'menu2', 'class'=>'menu_selected');
			$menuContent = html_writer::tag('span', '', array('id'=>'menuicon_catalog', 'class'=>'menuicon', 'title'=>$menuLabel));
			$menuContent .= html_writer::tag('span',$menuLabel);
			$menuContent = '<div id="banner-menu-item">'.$menuContent.'</div>';
		}
		else
		{
			$paremLi = array('id'=>'menu2');
			$menuContent = html_writer::link($menuUrl, '<span id="menuicon_catalog" class="menuicon" title="'.$menuLabel.'"></span><span>'.$menuLabel.'</span>', array('id' => 'banner-menu-item'));
		}
		$cMenuHeader .= html_writer::tag('li', $menuContent, $paremLi);
		$paremLi = array();

		
		$menuUrl = new moodle_url('/?redirect=0&contact=1');
		$menuLabel = get_string('contact', 'theme_moodec');
		if($idSelected == 3)
		{
			$paremLi = array('id'=>'menu3', 'class'=>'menu_selected');
			$menuContent = html_writer::tag('span', '', array('id'=>'menuicon_contact', 'class'=>'menuicon', 'title'=>$menuLabel));
			$menuContent .= html_writer::tag('span',$menuLabel);
			$menuContent = '<div id="banner-menu-item">'.$menuContent.'</div>';
		}
		else
		{
			$paremLi = array('id'=>'menu3');
			$menuContent = html_writer::link($menuUrl, '<span id="menuicon_contact" class="menuicon" title="'.$menuLabel.'"></span><span>'.$menuLabel.'</span>', array('id' => 'banner-menu-item'));
		}
		$cMenuHeader .= html_writer::tag('li', $menuContent, $paremLi);
		$paremLi = array();

		return $cMenuHeader.'</ul></div>';


	}
	
        
	public function custom_menu_language()
	{
		global $CFG;
		$langmenu = new custom_menu();
		
		$langs = get_string_manager()->get_list_of_translations();
        $haslangmenu = $this->lang_menu() != '';

        if (!$haslangmenu) {
            return '';
        }
		else{
            $strlang = get_string('language');
            $currentlang = current_language();
            if (isset($langs[$currentlang])) {
                $currentlang = $langs[$currentlang];
            } else {
                $currentlang = $strlang;
            }
            
            $start  = strpos($currentlang,"(" );
            $end = strpos($currentlang,")");
            $langcode = substr($currentlang, $start+1, $end-$start-1);

            
            $this->language = $langmenu->add($langcode, new moodle_url('#'), $strlang, 10000);
            foreach ($langs as $langtype => $langname) {
                $this->language->add($langname, new moodle_url($this->page->url, array('lang' => $langtype)), $langname);
            }
		}
		$content = '';
        foreach ($langmenu->get_children() as $item) {
            $context = $item->export_for_template($this);
            $content .= $this->render_from_template('theme_moodec/custom_langmenu_item', $context);
        }

        return $content;
	}
	
	
    protected function render_thiscourse_menu(custom_menu $menu) {
        global $CFG;

        $content = '';
        foreach ($menu->get_children() as $item) {
            $context = $item->export_for_template($this);
            $content .= $this->render_from_template('theme_moodec/activitygroups', $context);
        }

        return $content;
    }

    public function thiscourse_menu() {
        global $PAGE, $COURSE, $OUTPUT, $CFG;
        $menu = new custom_menu();
        $context = $this->page->context;
            if (isset($COURSE->id) && $COURSE->id > 1) {
                $branchtitle = get_string('thiscourse', 'theme_moodec');
                $branchlabel = $branchtitle;
                $branchurl = new moodle_url('#');
                $branch = $menu->add($branchlabel, $branchurl, $branchtitle, 10002);

                $data = theme_moodec_get_course_activities();

                foreach ($data as $modname => $modfullname) {
                    if ($modname === 'resources') {
                        
                        $branch->add($modfullname, new moodle_url('/course/resources.php', array('id' => $PAGE->course->id)));
                    } else {

                        $branch->add($modfullname, new moodle_url('/mod/'.$modname.'/index.php',
                                array('id' => $PAGE->course->id)));
                    }
                }
                
        }

        return $this->render_thiscourse_menu($menu);
    }

    

   

   


    public function teacherdash() {
        global $PAGE, $COURSE, $CFG, $DB, $OUTPUT;

        require_once($CFG->dirroot.'/completion/classes/progress.php');
        $togglebutton = '';
        $hasteacherdash = '';
        if (isloggedin() && ISSET($COURSE->id) && $COURSE->id > 1) {
            $course = $this->page->course;
            $context = context_course::instance($course->id);
            $hasteacherdash = has_capability('moodle/course:viewhiddenactivities', $context);

            if (has_capability('moodle/course:viewhiddenactivities', $context)) {
                $togglebutton = get_string('coursemanagementbutton', 'theme_moodec');
            } 
        }
        $course = $this->page->course;
        $context = context_course::instance($course->id);
		$tmpEditCog = $this->context_header_settings_menu().'';
		$haseditcog = true;
		if(strlen($tmpEditCog)<=0)
		{
			$haseditcog = false;
		}
        
        $editcog = html_writer::div($this->context_header_settings_menu(), 'pull-xs-right context-header-settings-menu');
        $thiscourse = $this->thiscourse_menu();
        $showincourseonly = isset($COURSE->id) && $COURSE->id > 1 && isloggedin() && !isguestuser(); 
        $globalhaseasyenrollment = enrol_get_plugin('easy');
        $coursehaseasyenrollment = '';
        if($globalhaseasyenrollment) {
            $coursehaseasyenrollment = $DB->record_exists('enrol', array('courseid' => $COURSE->id, 'enrol' => 'easy'));
            $easyenrollinstance = $DB->get_record('enrol', array('courseid' => $COURSE->id, 'enrol' => 'easy'));
        }
        
        //link catagories
        $haspermission = has_capability('enrol/category:config', $context) && isset($COURSE->id) && $COURSE->id > 1;
        $userlinks = get_string('userlinks', 'theme_moodec');
        $userlinksdesc = get_string('userlinks_desc', 'theme_moodec');
        $qbank = get_string('qbank', 'theme_moodec');
        $qbankdesc = get_string('qbank_desc', 'theme_moodec');
        $badges = get_string('badges', 'theme_moodec');
        $badgesdesc = get_string('badges_desc', 'theme_moodec');
        $coursemanage = get_string('coursemanage', 'theme_moodec');
        $coursemanagedesc = get_string('coursemanage_desc', 'theme_moodec');

        //user links
        if($coursehaseasyenrollment && isset($COURSE->id) && $COURSE->id > 1){
            $easycodetitle = get_string('header_coursecodes', 'enrol_easy');
            $easycodelink = new moodle_url('/enrol/editinstance.php', array('courseid' => $PAGE->course->id, 'id' => $easyenrollinstance->id, 'type' =>'easy'));
        }
        $gradestitle = get_string('gradesoverview', 'gradereport_overview');
        $gradeslink = new moodle_url('/grade/report/grader/index.php', array('id' => $PAGE->course->id));
        $enroltitle = get_string('enrolledusers', 'enrol');
        $enrollink = new moodle_url('/user/index.php', array('id' => $PAGE->course->id));
        $grouptitle = get_string('groups', 'group');
        $grouplink = new moodle_url('/group/index.php', array('id' => $PAGE->course->id));
        $enrolmethodtitle = get_string('enrolmentinstances', 'enrol');
        $enrolmethodlink = new moodle_url('/enrol/instances.php', array('id' => $PAGE->course->id));
        
        //user reports
        $logstitle = get_string('logs', 'moodle');
        $logslink = new moodle_url('/report/log/index.php', array('id' => $PAGE->course->id));
        $livelogstitle = get_string('loglive:view', 'report_loglive');
        $livelogslink = new moodle_url('/report/loglive/index.php', array('id' => $PAGE->course->id));
        $participationtitle = get_string('participation:view', 'report_participation');
        $participationlink = new moodle_url('/report/participation/index.php', array('id' => $PAGE->course->id));
        //$activitytitle = get_string('outline:view', 'report_outline');
        //$activitylink = new moodle_url('/report/outline/index.php', array('id' => $PAGE->course->id));

        //questionbank
        $qbanktitle = get_string('questionbank', 'question');
        $qbanklink = new moodle_url('/question/edit.php', array('courseid' => $PAGE->course->id));
        $qcattitle = get_string('questioncategory', 'question');
        $qcatlink = new moodle_url('/question/category.php', array('courseid' => $PAGE->course->id));
        $qimporttitle = get_string('import', 'question');
        $qimportlink = new moodle_url('/question/import.php', array('courseid' => $PAGE->course->id));
        $qexporttitle = get_string('export', 'question');
        $qexportlink = new moodle_url('/question/export.php', array('courseid' => $PAGE->course->id));
        
        //manage course
        $courseadmintitle = get_string('courseadministration', 'moodle');
        $courseadminlink = new moodle_url('/course/admin.php', array('courseid' => $PAGE->course->id));
        $coursecompletiontitle = get_string('coursecompletion', 'moodle');
        $coursecompletionlink = new moodle_url('/course/completion.php', array('id' => $PAGE->course->id));
        $courseresettitle = get_string('reset', 'moodle');
        $courseresetlink = new moodle_url('/course/reset.php', array('id' => $PAGE->course->id));
        $coursebackuptitle = get_string('backup', 'moodle');
        $coursebackuplink = new moodle_url('/backup/backup.php', array('id' => $PAGE->course->id));
        //$courserestoretitle = get_string('restore', 'moodle');
        $courserestorelink = new moodle_url('/backup/restorefile.php', array('contextid' => $PAGE->context->id));
        $courseimporttitle = get_string('import', 'moodle');
        $courseimportlink = new moodle_url('/backup/import.php', array('id' => $PAGE->course->id));
        $courseedittitle = get_string('editcoursesettings', 'moodle');
        $courseeditlink = new moodle_url('/course/edit.php', array('id' => $PAGE->course->id));
        
        //badges
        $badgemanagetitle = get_string('managebadges', 'badges');
        $badgemanagelink = new moodle_url('/badges/index.php?type=2', array('id' => $PAGE->course->id));
        $badgeaddtitle = get_string('newbadge', 'badges');
        $badgeaddlink = new moodle_url('/badges/newbadge.php?type=2', array('id' => $PAGE->course->id));
        
        //misc
        $recyclebintitle = get_string('pluginname', 'tool_recyclebin');
        $recyclebinlink = new moodle_url('/admin/tool/recyclebin/index.php', array('contextid' => $PAGE->context->id));
        //$filtertitle = get_string('filtersettings', 'filters');
        //$filterlink = new moodle_url('/filter/manage.php', array('contextid' => $PAGE->context->id));
        //send to template
		$hasquestionpermission = has_capability('moodle/question:add', $context);
		$hasbadgepermission = has_capability('moodle/badges:awardbadge', $context);
		$hascoursepermission = has_capability('moodle/backup:backupcourse', $context);
		$hasuserpermission = has_capability('moodle/course:viewhiddenactivities', $context);
		$hasgradebookshow = $PAGE->course->showgrades == 1;
        $hascompletionshow = $PAGE->course->enablecompletion == 1;
		
		$btnMoretitle = get_string('morenavigationlinks');
		$btnMorelink = new moodle_url('/course/admin.php', array('courseid' => $PAGE->course->id));
		
        $dashlinks = [
        'showincourseonly' =>$showincourseonly,
        'haspermission' => $haspermission,
        'thiscourse' => $thiscourse,
        'haseditcog' => $haseditcog,
        'editcog' => $editcog,
        'togglebutton' => $togglebutton,
        'userlinkstitle' => $userlinks,
        'userlinksdesc' => $userlinksdesc,
        'qbanktitle' => $qbank,
        'qbankdesc' => $qbankdesc,
        'badgestitle' => $badges,
        'badgesdesc' => $badgesdesc,
        'coursemanagetitle' => $coursemanage,
        'coursemanagedesc' => $coursemanagedesc,

        'hasteacherdash' => $hasteacherdash,
		'teacherdash' =>array('hasquestionpermission' => $hasquestionpermission, 'hasbadgepermission' => $hasbadgepermission, 'hascoursepermission' => $hascoursepermission, 'hasuserpermission' => $hasuserpermission),

        'studentcourseadminlink' => $courseadminlink,

        'dashlinks' => array(

                array('hasuserlinks' => $enroltitle, 'title' => $enroltitle, 'url' => $enrollink),
                array('hasuserlinks' => $grouptitle, 'title' => $grouptitle, 'url' => $grouplink),
                array('hasuserlinks' => $enrolmethodtitle, 'title' => $enrolmethodtitle, 'url' => $enrolmethodlink),
                array('hasuserlinks' => $logstitle, 'title' => $logstitle, 'url' => $logslink),
                array('hasuserlinks' => $livelogstitle, 'title' => $livelogstitle, 'url' => $livelogslink),
                array('hasuserlinks' => $participationtitle, 'title' => $participationtitle, 'url' => $participationlink),
               // array('hasuserlinks' => $activitytitle, 'title' => $activitytitle, 'url' => $activitylink),
                array('hasqbanklinks' => $qbanktitle, 'title' => $qbanktitle, 'url' => $qbanklink),
                array('hasqbanklinks' => $qcattitle, 'title' => $qcattitle, 'url' => $qcatlink),
                array('hasqbanklinks' => $qimporttitle, 'title' => $qimporttitle, 'url' => $qimportlink),
                array('hasqbanklinks' => $qexporttitle, 'title' => $qexporttitle, 'url' => $qexportlink),
                array('hascoursemanagelinks' => $courseedittitle, 'title' => $courseedittitle, 'url' => $courseeditlink),
                array('hascoursemanagelinks' => $coursecompletiontitle, 'title' => $coursecompletiontitle, 'url' => $coursecompletionlink),
                array('hascoursemanagelinks' => $courseadmintitle, 'title' => $courseadmintitle, 'url' => $courseadminlink),
                array('hascoursemanagelinks' => $courseresettitle, 'title' => $courseresettitle, 'url' => $courseresetlink),
                array('hascoursemanagelinks' => $coursebackuptitle, 'title' => $coursebackuptitle, 'url' => $coursebackuplink),
                //array('hascoursemanagelinks' => $courserestoretitle, 'title' => $courserestoretitle, 'url' => $courserestorelink),
                array('hascoursemanagelinks' => $courseimporttitle, 'title' => $courseimporttitle, 'url' => $courseimportlink),
                array('hascoursemanagelinks' => $recyclebintitle, 'title' => $recyclebintitle, 'url' => $recyclebinlink),
                //array('hascoursemanagelinks' => $filtertitle, 'title' => $filtertitle, 'url' => $filterlink),
                array('hasbadgelinks' => $badgemanagetitle, 'title' => $badgemanagetitle, 'url' => $badgemanagelink),
                array('hasbadgelinks' => $badgeaddtitle, 'title' => $badgeaddtitle, 'url' => $badgeaddlink),
				array('hascoursemanagemorelinks' => $btnMoretitle, 'title' => $btnMoretitle, 'url' => $btnMorelink),
            ),
        ];

        //attach easy enrollment links if active
        if ($globalhaseasyenrollment && $coursehaseasyenrollment) {
            $dashlinks['dashlinks'][] = array('haseasyenrollment' => $coursehaseasyenrollment, 'title' => $easycodetitle, 'url' => $easycodelink);

        } 
            return $this->render_from_template('theme_moodec/teacherdash', $dashlinks );
        
    }
	
	
	/**
     * Renders the login form.
     *
     * @param \core_auth\output\login $form The renderable.
     * @return string
     */
    public function render_login(\core_auth\output\login $form) {
        global $SITE;

        $context = $form->export_for_template($this);

        // Override because rendering is not supported in template yet.
        $context->cookieshelpiconformatted = $this->help_icon('cookiesenabled');
        $context->errorformatted = $this->error_text($context->error);

       // $context->logourl = $this->get_logo();
        $context->sitename = format_string($SITE->fullname, true, array('context' => \context_course::instance(SITEID)));

        return $this->render_from_template('core/login', $context);
    }
	

	/**
     * Whether we should display the main theme logo in the navbar.
     *
     * @return bool
     */
    public function should_display_theme_logo() {
        $logo = $this->get_theme_logo_url();

        return !empty($logo);
    }

    /**
     * Get the main logo URL.
     *
     * @return string
     */
    public function get_theme_logo_url() {
        $theme = theme_config::load('moove');

        return $theme->setting_file_url('logo', 'logo');
    }


}