<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A two column layout for the boost theme.
 *
 * @package   theme_moodec
 * @copyright 2017 Cyberlearn
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);
require_once($CFG->libdir . '/behat/lib.php');

$PAGE->requires->jquery();

$PAGE->requires->js('/theme/moodec/javascript/date.js');
$PAGE->requires->js('/theme/moodec/javascript/function_moodec.js');


//$PAGE->requires->css('/theme/moodec/scss/jquery-ui.min.css');

if (isloggedin()) {
    $navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
} else {
    $navdraweropen = false;
}
$extraclasses = [];

$adminview=false;
if(is_siteadmin())
{
	$adminview=true;
	$extraclasses = [' adminview'];
}
else
{
	$extraclasses = [' notadminview'];
}

if ($navdraweropen) {
    $extraclasses[] .= 'drawer-open-left';
}

$frontpage = false;
$partenaires = '';
$captcha = '';
$idSelected = 1;

$fileInclude = 'theme_moodec/frontpage';
if (isset($_GET['categoryid'])) 
{
	$fileInclude = 'theme_moodec/frontpage_search';
	$PAGE->requires->js('/theme/moodec/javascript/woco.accordion.js');
	$PAGE->requires->js('/theme/moodec/javascript/search.js');
	$idSelected = 2;
} 
else if (isset($_GET['contact']))
{
	$fileInclude = 'theme_moodec/frontpage_contact';
	$PAGE->requires->js('/theme/moodec/javascript/contact.js');
	$idSelected = 3;
}  
else
{
	$idSelected = 1;
	$fileInclude = 'theme_moodec/frontpage';
	$PAGE->requires->js('/theme/moodec/javascript/home.js');
	
	$extraclasses[] .= 'site_frontpage';
	$frontpage = true;
	
	$urlPartenaires = ['http://www.hes-so.ch/', 'http://www.hes-so.ch/fr/recherche-hes-so-31.html', 'https://cyberlearn.hes-so.ch/', 'http://www.hesge.ch/heds/', 'http://www.hevs.ch/fr/'];
	
	$imgPartenaires = [$OUTPUT->image_url('P_logoHES', 'theme_moodec'),$OUTPUT->image_url('P_logo_isnet', 'theme_moodec'),$OUTPUT->image_url('P_logoCL', 'theme_moodec'),$OUTPUT->image_url('P_heds', 'theme_moodec'),$OUTPUT->image_url('P_hevs', 'theme_moodec')];
	
	$imgsizePartenaires = [150,151,150,150,150];
	
	$partenaires = '';
	
	for($i = 0; $i < count($imgPartenaires); $i++) {
		$partenaires .= '<a href="'. $urlPartenaires[$i] .'" target="_blank"><img src="'. $imgPartenaires[$i] .'" width="'. $imgsizePartenaires[$i] .'"></a>';
	}

}

/*
else if (isset($_GET['certificate']))
{
	$fileInclude = 'home/certificate.php';
	$idSelected = 4;
}
*/


$bodyattributes = $OUTPUT->body_attributes($extraclasses);
$blockshtml = $OUTPUT->blocks('side-pre');
$contentblockshtml = $OUTPUT->blocks('content');
$hasblocks = strpos($blockshtml, 'data-block=') !== false;
$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();

$menuheader = $OUTPUT->custom_menu_header($idSelected);

$token = md5(rand(1000,9999));
$_SESSION['token'] = $token;


$keyword = (!empty($_GET['keyword']) ? $_GET['keyword'] : '');
$templatecontext = [
    'adminview' => $adminview,
	'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
	'showbacktotop' => true,
    'sidepreblocks' => $blockshtml,
	'contentblocks' => $contentblockshtml,
    'hasblocks' => true,
    'bodyattributes' => $bodyattributes,
    'navdraweropen' => $navdraweropen,
    'regionmainsettingsmenu' => $regionmainsettingsmenu,
    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
    'logo' => $OUTPUT->image_url('logoMooc', 'theme_moodec'),
    'logoMoocHes' => $OUTPUT->image_url('logoMoocHes', 'theme_moodec'),
    'hes-so' => $OUTPUT->image_url('logoHES', 'theme_moodec'),
    'hesso_S' => $OUTPUT->image_url('logoHESS', 'theme_moodec'),
    'coursesearchaction' => $CFG->wwwroot . '/course/search.php',
	'isloggedin' => isloggedin(),
	'isguest' => isguestuser(),
	'keyword' => $keyword,
	'partenaires' => $partenaires,
	'frontpage' => $frontpage,
	'menuheader' => $menuheader,
	'idUser' => $USER->id,
	'sessiontoken' => $token,
	'iconMail' => $OUTPUT->image_url('icon_mail', 'theme_moodec'),
];






$templatecontext['flatnavigation'] = $PAGE->flatnav;

echo $OUTPUT->render_from_template($fileInclude, $templatecontext);

