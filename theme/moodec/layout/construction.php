<?php

defined('MOODLE_INTERNAL') || die();

/**
 * A login page layout for the boost theme.
 *
 * @package   theme_boost
 * @copyright 2016 Damyon Wiese
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$bodyattributes = $OUTPUT->body_attributes();

$templatecontext = [
	'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'bodyattributes' => $bodyattributes,
    'logo' => $OUTPUT->image_url('logoHeaderWhite', 'theme_moodec'),
    'logoMoocHes' => $OUTPUT->image_url('logoMoocHes', 'theme_moodec'),
    'hes-so' => $OUTPUT->image_url('hes-so', 'theme_moodec'),
    'coursesearchaction' => $CFG->wwwroot . '/course/search.php'
];

$PAGE->requires->jquery();
//$PAGE->requires->js('/theme/moodec/javascript/jquery.fancybox.min.js');



echo $OUTPUT->render_from_template('theme_moodec/construction', $templatecontext);

