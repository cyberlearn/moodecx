<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file.
 *
 * @package   theme_moodec
 * @copyright 2017 Cyberlearn HES-SO
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// This line protects the file from being accessed by a URL directly.
defined('MOODLE_INTERNAL') || die();


$string['pluginname'] = 'MOOC HES-SO 3.5';
$string['choosereadme'] = 'Le theme moodec est un thème hérité de Boost pour Cyberlearn HES-SO.';
$string['region-side-pre'] = 'Droite';
$string['region-content'] = 'Centre';

$string['assistance'] = 'Assistance';
$string['links'] = 'Liens';
$string['joinin'] = 'Rejoignez-nous!';
$string['blog'] = 'Blog';
$string['team'] = 'Équipe';
$string['help'] = 'Aide';
$string['home'] = 'Accueil';
$string['catalog'] = 'Catalogue';
$string['contact'] = 'Contact';
$string['searchcourse'] = 'Recherche de cours';
$string['administration'] = 'Administration';
$string['loginaai'] = 'Login AAI';
$string['logoutaai'] = 'Logout AAI';
$string['login'] = 'Vous avez déjà un compte ?';
$string['myhome'] = 'Mon tableau de bord';
$string['loggedinas'] = ' connecté en tant que ';
$string['loggedinfrom'] = 'Connectés à partir de ';
$string['loginSep'] = 'ou';
$string['loginPanelForgotten'] = 'Mot de passe oublié';



//teacher and student dashboard slider
$string['thiscourse'] = 'Ce Cours';
$string['userlinks'] = 'Liens utilisateur';
$string['userlinks_desc'] = 'Administrez vos étudiants';
$string['qbank'] = 'Banque de questions';
$string['qbank_desc'] = 'Créez et organisez vos questions de quiz';
$string['badges'] = 'Badges';
$string['badges_desc'] = 'Récompensez vos étudiants';
$string['coursemanage'] = 'Paramètres de cours';
$string['coursemanage_desc'] = 'Administrez l\'entier de votre cours';
$string['coursemanagementbutton'] = 'Administration du cours';
$string['studentdashbutton'] = 'Ce cours';
$string['courseinfo'] = 'Description du cours';
$string['coursestaff'] = 'Professeur-e-s du cours';
$string['activitylinkstitle'] = 'Activités';
$string['activitylinkstitle_desc'] = 'Voir toutes les activités du cours';
$string['myprogresstext'] = 'Ma progression';
$string['mygradestext'] = 'Mes notes';


//Edit Button Text
$string['editon'] = 'Activer le mode édition';
$string['editoff'] = 'Désactiver le mode édition';


$string['mymoocs'] = 'Mes moocs';
$string['mycourses'] = 'Mes cours';
$string['myunits'] = 'Mes unités';
$string['mymodules'] = 'Mes modules';
$string['myclasses'] = 'Mes classes';
$string['noenrolments'] = 'Vous n\'êtes actuellement inscrit-e à aucun cours';
$string['siteadminquicklink'] = 'Administration du site';

$string['backtotop'] = 'Haut de page';

$string['guestUser'] = 'Anonyme';


/**
 * HOME PAGE
 */
$string['subscribehome'] = 'Inscrivez-vous';
$string['nowhome'] = 'dès maintenant';
$string['bannersoustitre'] = 'La plateforme Mooc de la ';
$string['topcourse'] = 'Cours disponibles';
$string['topfuturecourse'] = "Cours à venir";
$string['partenaires'] = 'Partenaires';
$string['enrolme'] = 'M\'inscrire';
$string['enrolnotyet'] = 'Disponible à partir du ';
$string['searchadvanced'] = 'Recherche avancée';
$string['keywords'] = 'Mots-clés';
$string['category'] = 'Catégories';
$string['institution'] = 'Institutions';
$string['lang'] = 'Langues';
$string['moreinfo'] = 'Informations complémentaires';
$string['prerequisite'] = 'Pré-requis';
$string['syllabus'] = 'Syllabus';
$string['reading'] = 'Lecture';
$string['faq'] = 'FAQ';
$string['school'] = 'Institution';
$string['startdate'] = 'Début';
$string['enddate'] = 'Fin';
$string['duration'] = 'Durée';
$string['effort'] = 'Effort';
$string['error'] = 'Une erreur est survenue';
$string['nocourse'] = 'Aucun cours n\'est disponible';
$string['cours'] = 'cours';
$string['courspluriel'] = 'cours';
$string['fr'] = 'Français';
$string['de'] = 'Allemand';
$string['en'] = 'Anglais';
$string['accesscours'] = 'Accéder au cours';
$string['hour'] = 'heure';
$string['hours'] = 'heures';
$string['week'] = 'semaine';
$string['weeks'] = 'semaines';
$string['role'] = 'Professeur';
//$string['contact'] = 'Contact';
$string['contacttitle'] = 'Contacter l\'administrateur' ;
$string['contactforname'] = 'Prénom : ';
$string['contactname'] = 'Nom : ';
$string['contactemail'] = 'Email : ';
$string['message'] = 'Message : ';
$string['send'] = 'Envoyer';
$string['refresh'] = 'Rafraichir';
$string['mandatoryfield'] = 'Tous les champs sont obligatoires';
$string['emailinvalid'] = "L'email n'est pas correct";
$string['mailinvalid'] = "Une erreur est survenue";
$string['mailsuccess'] = "Votre demande sera traitée dans les plus brefs délais";
$string['captchainvalid'] = "Captcha invalide";
$string['enrolend'] = "Cours terminé";
$string['registrationClosed'] = "Inscriptions fermées";
$string['verifymoodeccertificate'] = 'Verifier un certificat';

$string['contactfuturemooc'] = "Contactez-nous pour avoir plus d'informations";
$string['mailpreregistration'] = 'Préinscription pour ';
$string['mailprecontent'] = 'Madame, Monsieur, %0D%0AJe suis intéressé-e par le cours ';
