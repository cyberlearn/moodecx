<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file.
 *
 * @package   theme_moodec
 * @copyright 2017 Cyberlearn HES-SO
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// This line protects the file from being accessed by a URL directly.
defined('MOODLE_INTERNAL') || die();


$string['pluginname'] = 'MOOC HES-SO 3.5';
$string['choosereadme'] = 'Thema moodec ist ein Sohnthema von Boost für Cyberlearn HES-SO.';
$string['region-side-pre'] = 'Recht';
$string['region-content'] = 'Zentrum';

$string['assistance'] = 'Unterstützung';
$string['links'] = 'Links';
$string['joinin'] = 'Join in !';
$string['blog'] = 'Blog';
$string['team'] = 'Team';
$string['help'] = 'Hilfe';
$string['home'] = 'Zuhause';
$string['catalog'] = 'Katalog';
$string['contact'] = 'Kontakt';
$string['searchcourse'] = 'Kurse suchen';
$string['administration'] = 'Einstellungen';
$string['loginaai'] = 'Login AAI';
$string['logoutaai'] = 'Logout AAI';
$string['login'] = 'Haben Sie schon ein Benutzerkonto ?';
$string['myhome'] = 'Mein Dashboard';
$string['loggedinas'] = ' Angemeldet als ';
$string['loggedinfrom'] = 'In vom angemeldet ';
$string['loginSep'] = 'oder';
$string['loginPanelForgotten'] = 'Passwort vergessen';



//teacher and student dashboard slider
$string['thiscourse'] = 'Dieser Kurs';
$string['userlinks'] = 'Benutzer Links';
$string['userlinks_desc'] = 'Studierende Verwaltung';
$string['qbank'] = 'Fragensammlung';
$string['qbank_desc'] = 'Quizfragen esrtellen und organisieren';
$string['badges'] = 'Marken';
$string['badges_desc'] = 'Belohnen Sie ihre Studierenden';
$string['coursemanage'] = 'Kurseinstellungen';
$string['coursemanage_desc'] = 'Verwalten sie ihren vollständigen Kurs';
$string['coursemanagementbutton'] = 'Kursverwaltung';
$string['studentdashbutton'] = 'Dieser Kurs';
$string['courseinfo'] = 'Kurs Beschreibung';
$string['coursestaff'] = 'Dozenten des Kurses';
$string['activitylinkstitle'] = 'Aktivitäten';
$string['activitylinkstitle_desc'] = 'Alle Aktivitäten des Kurses ansehen';
$string['myprogresstext'] = 'Mein Fortschritt';
$string['mygradestext'] = 'Meine Noten';


//Edit Button Text
$string['editon'] = 'Bearbeiten einschalten';
$string['editoff'] = 'Bearbeiten ausschalten';



$string['mymoocs'] = 'Meine moocs';
$string['mycourses'] = 'Meine Kurse';
$string['myunits'] = 'Meine Einheiten';
$string['mymodules'] = 'Meine Module';
$string['myclasses'] = 'Meine Klassen';
$string['noenrolments'] = 'Zur Zeit seid Ihr in keinem Kurs eingeschrieben';
$string['siteadminquicklink'] = 'Site Verwaltung';

$string['backtotop'] = 'Zum Seitenanfang';

$string['guestUser'] = 'Gast';

/**
 * HOME PAGE
 */
$string['subscribehome'] = 'Einschreiben';
$string['nowhome'] = 'jetzt';
$string['bannersoustitre'] = 'Die Mooc Plattform der ';
$string['topcourse'] = 'Kurse zur Verfügung';
$string['topfuturecourse'] = "Bevorstehende Kurse";
$string['partenaires'] = 'Partners';
$string['enrolme'] = 'Registrieren';
$string['enrolnotyet'] = 'Ab  ';
$string['searchadvanced'] = 'Erweiterte Suche';
$string['keywords'] = 'Stichwörter';
$string['category'] = 'Kategorien';
$string['institution'] = 'Institutionen';
$string['lang'] = 'Sprachen';
$string['moreinfo'] = 'Zusätzliche Informationen';
$string['prerequisite'] = 'Voraussetzung';
$string['syllabus'] = 'Lehrplan';
$string['reading'] = 'Lesen';
$string['faq'] = 'FAQ';
$string['school'] = 'Institution';
$string['startdate'] = 'Beginn';
$string['enddate'] = 'Ende';
$string['duration'] = 'Dauer';
$string['effort'] = 'Bemühung';
$string['error'] = 'Ein Fehler trat auf';
$string['nocourse'] = 'Kein Kurs ist verfügbar';
$string['cours'] = 'Kurs';
$string['courspluriel'] = 'Kurse';
$string['fr'] = 'Französich';
$string['de'] = 'Deutsch';
$string['en'] = 'English';
$string['accesscours'] = 'Zugriff auf den Kurs';
$string['hour'] = 'Stunde';
$string['hours'] = 'Stunden';
$string['week'] = 'Woche';
$string['weeks'] = 'Wochen';
$string['role'] = 'Dozent';
$string['contact'] = 'Kontakt';
$string['contactforname'] = 'Vorname : ';
$string['contactname'] = 'Name : ';
$string['contactemail'] = 'Email : ';
$string['message'] = 'Mitteilung : ';
$string['send'] = 'Senden';
$string['refresh'] = 'Aktualisieren';
$string['mandatoryfield'] = 'Alle Felder obligatorisch';
$string['emailinvalid'] = "Email ist ungültig";
$string['mailinvalid'] = "Ein Fehler ist aufgetreten";
$string['mailsuccess'] = "Ihre Anfrage wurde verschickt";
$string['captchainvalid'] = "Captcha ist ungültig";
$string['enrolend'] = "Der Kurs  ist fertig";
$string['registrationClosed'] = "Anmeldungen geschlossen";
$string['verifymoodeccertificate'] = 'Überprüfen eines Zertifikats';

$string['contactfuturemooc'] = 'Kontaktieren Sie uns für weitere Informationen';
$string['mailpreregistration'] = 'Voranmeldung zu ';
$string['mailprecontent'] = 'Sehr geehrte Damen und Herren, ich interessiere mich für ';
