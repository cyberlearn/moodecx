<?php
$functions = array(

    'theme_cfai_get_home_popup' => array(
        'classname'   => 'theme_cfai\\webservice\\external',
        'methodname'  => 'get_home_popup',
        'description' => 'Return moodec description in popup',
        'type'        => 'read',
		'ajax' 		  => true,
		'loginrequired' => false
    ),
	'theme_cfai_get_home_search_advanced' => array(
        'classname'   => 'theme_cfai\\webservice\\external',
        'methodname'  => 'get_home_search_advanced',
        'description' => 'Return course for search advanced',
        'type'        => 'read',
		'ajax' 		  => true,
		'loginrequired' => false
    )
);

