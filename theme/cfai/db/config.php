<?php

include_once('../../../config.php');

//Connect DB
try {

	global $CFG, $DB;
	$dns = 'mysql:host=' . $CFG->dbhost . ';dbname=' . $CFG->dbname;
	$dbuser = $CFG->dbuser;
	$dbpass = $CFG->dbpass;

	$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");

	// Init connection
	$connection = new PDO($dns, $dbuser, $dbpass, $options);
	$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$connection->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
} catch (PDOException $e) {
	print "Error : " . $e -> getMessage();
	die();
}
?>

