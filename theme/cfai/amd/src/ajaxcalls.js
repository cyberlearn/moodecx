define(['jquery', 'core/notification','core/ajax', 'core/templates'],
       function($, notification, ajax, templates) {

    function Ajaxcall() {
        this.value = "ajax ok";
    };
	
	function close_home_popup(pageurl) {
        window.history.pushState( {} , '', pageurl );
		$('#home_popup').replaceWith('<div id="home_popup"></div>');
    };
	
	Ajaxcall.prototype.get_home_popup = function(itemid, idPage) {
		console.log('call: get_home_popup');
        //var test = 1;
		console.log('ajaxcall: ' + itemid + ' : ' + idPage);
		$('#home_popup').addClass('show');	
		
        var promises = ajax.call([{
            methodname: 'theme_cfai_get_home_popup',
            args: {itemid: itemid},
            done: console.log("ajax done"),
            fail: function(response) {
				$('#home_popup').replaceWith('<div id="home_popup"></div>');
				console.log(response);
			}
        }]);
        promises[0].then(function(data) {
            data.teachers = JSON.parse(data.teachers);
			
			var pageurl = '?redirect=0';

			if(idPage == 2)
			{
				pageurl = pageurl+'&op=cat';
			}
			
			templates.render('theme_cfai/home_popup', data).done(function(html, js) {

				$('#home_popup').replaceWith(html);
				$('#home_popup').addClass('show');

				$(".closePopUp").click(function () {
					close_home_popup(pageurl);

				});
				$('#home_popup').click(function () {
					close_home_popup(pageurl);
				});
				$('#home_popup .popup-content').click(function (event) {
					//close_home_popup(pageurl);
					//console.log('home_popup click');
					event.stopPropagation();
				});
				$('#home_popup').on('touchmove', function (event) {
					//event.stopPropagation();
					event.preventDefault();
				});
			
            }).fail(notification.exception);
			
			
			
			window.history.pushState( {} , '', pageurl+'&cid='+itemid );
			
			
			
        });
    };
	
	Ajaxcall.prototype.get_home_search_advanced = function(dataSA) {
		console.log("dataSA");
		console.log(dataSA);
		var _this = this
		var datastring = JSON.stringify(dataSA);
		var promises = ajax.call([{
            methodname: 'theme_cfai_get_home_search_advanced',
            args: {data: datastring},
            done: console.log("ajax get_home_search_advanced done"),
            fail: function(response) {
				
				console.log('fail: get_home_search_advanced');
				console.log(response);
			}
        }]);
		promises[0].then(function(data) {
			$('#catalogue #course_list').html(data.content);
			
			
			$("#course_list .blocCours").click(function () {
				var bcid = $(this).data("cid");
				_this.get_home_popup(bcid, 2);
			});
		
		});	
	};
    /*
    Ajaxcall.prototype.loadsettings111 = function(itemid, txtareaupdate) {

        //var test = 1;
		console.log('loadsettings: ' + itemid + ' : ' + txtareaupdate); 
        var promises = ajax.call([{
            methodname: 'theme_getcourse',
            args: {itemid: itemid},
            done: console.log("ajax done"),
            //fail: notification.exception
        }]);
		console.log(promises[0]);

		promises[0].done(function(response) {
		   console.log('mod_wiki/pluginname is' + response);
		}).fail(function(ex) {
		   // do something with the exception
		});
    };
	*/
       
    
    return Ajaxcall;
});
    