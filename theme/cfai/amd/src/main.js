define(['jquery', 'theme_cfai/ajaxcalls'],
    function($, Ajaxcalls) 
	{
	
	var ajaxx = require("theme_cfai/ajaxcalls");
	var ajax1 = new ajaxx();
    
	function ajaxCallAdvancedSearch(){
		var institutionFilterSearch = [];
		var langFilterSearch = [];
		
		var keyword = $("#inputTextSearchAdv").val();
		
		var cbChecked = 0;
		$.each($("input[name='cb_institution']:checked"), function(){
			institutionFilterSearch.push($(this).val());
			cbChecked++;
		});
		if(cbChecked > 0)
		{
			$("#institutionFilter").addClass('actived');
		}
		else
		{
			$("#institutionFilter").removeClass('actived');
		}
		
		
		cbChecked = 0;
		$.each($("input[name='cb_lang']:checked"), function(){
			langFilterSearch.push($(this).val());
			cbChecked++;
		});
		if(cbChecked > 0)
		{
			$("#langFilter").addClass('actived');
		}
		else
		{
			$("#langFilter").removeClass('actived');
		}

		var data = {
        keyword: keyword,
        institution : institutionFilterSearch,
        lang : langFilterSearch
		}
		
		ajax1.get_home_search_advanced(data);

	}
	
	function init_block_course_ajax(idPage)
	{
		$("#course_list .blocCours").click(function () {
			var bcid = $(this).data("cid");
			ajax1.get_home_popup(bcid, idPage);
        });
	}
	
	
	return {
        init: function(idPage, cid) {
			console.log('main init');
			init_block_course_ajax(idPage);
			
			
			if(idPage == 2)
			{
				ajaxCallAdvancedSearch();

				$('#inputTextSearchAdv').on('input',function(e){
					ajaxCallAdvancedSearch();
				});
				
				$(".filtreline input[type='checkbox']").on('click',function(e){
					ajaxCallAdvancedSearch();
				});
			}
			if(cid>0)
			{
				ajax1.get_home_popup(cid, idPage);
			}
			
			
        }
		
		
    };
});
