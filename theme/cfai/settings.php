<?php
/*
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    $settings = new theme_boost_admin_settingspage_tabs('themesettingclhesso', get_string('pluginname', 'theme_cfai'));
	$page = new admin_settingpage('theme_cfai_general', 'general');
	
	$name = 'theme_cfai/preset';
    $title = 'preset';
    $description = 'preset_desc';
    $default = 'default.scss';
	
    // Custom CSS file.
    $name = 'theme_cfai/customcss';
    $title = get_string('customcss', 'theme_cfai');
    $description = get_string('customcssdesc', 'theme_cfai');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Footnote setting.
    $name = 'theme_cfai/footnote';
    $title = get_string('footnote', 'theme_cfai');
    $description = get_string('footnotedesc', 'theme_cfai');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
	 $settings->add($page);
}
*/

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {                                                                                                             
 
    // Boost provides a nice setting page which splits settings onto separate tabs. We want to use it here.
    $settings = new theme_boost_admin_settingspage_tabs('themesettingcfai', get_string('pluginname', 'theme_cfai').' '.get_string('configsetting', 'theme_cfai'));
 
    // Each page is a tab - the first is the "General" tab.
    $page = new admin_settingpage('theme_cfai_general', get_string('generalsettings', 'theme_cfai'));
	
	// Raw SCSS to include after the content.
    $setting = new admin_setting_scsscode('theme_cfai/scss', get_string('rawscss', 'theme_boost'),
        get_string('rawscss_desc', 'theme_boost'), '', PARAM_RAW);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
      
	
	$page->add(new admin_setting_heading('theme_cfai_partenaires_heading',
        get_string('partenaires', 'theme_cfai'),
        format_text(get_string('partenairessettingdesc', 'theme_cfai'), FORMAT_MARKDOWN)));

	$name = 'theme_cfai/bloc_partenaires';
    $title = get_string('content');
    $description = '';
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);


	
	$page->add(new admin_setting_heading('theme_cfai_dashboard_news_heading',
        get_string('dashboardnews', 'theme_cfai'),
        format_text(get_string('dashboardnewssettingdesc', 'theme_cfai'), FORMAT_MARKDOWN)));	
	

	$name = 'theme_cfai/bloc_news_hes_name';
    $title = get_string('bloc_news_hes_name', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
	
	$name = 'theme_cfai/availability_bloc_news_hes';
    $title = get_string('availability', 'moodle');
    $description = '';
	$default = 0;
	$choices = array(
		'0' => get_string('hide', 'moodle'),
		'1' => get_string('showforall', 'theme_cfai'),
        '2' => get_string('showforstudent', 'theme_cfai'),
        '3' => get_string('showforteacher', 'theme_cfai') 
	);
	$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
	
	$name = 'theme_cfai/bloc_news_hes';
    $title = get_string('contenunews', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);
	
    // Must add the page after definiting all the settings!                                                                         
    $settings->add($page);

/*	
    // contact support.   
	
	$pageSupport = new admin_settingpage('theme_cfai_support', get_string('supportsettings', 'theme_cfai'));  

	$name = 'theme_cfai/support_intro';
    $title = get_string('support_intro', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $pageSupport->add($setting);
	
	
	$pageSupport->add(new admin_setting_heading('theme_cfai_support_pdf_heading',
        get_string('supportpdf', 'theme_cfai'),
        format_text(get_string('supportpdfsettingdesc', 'theme_cfai'), FORMAT_MARKDOWN)));	

		
    $name = 'theme_cfai/show_support_pdf';
    $title = get_string('show_support_pdf', 'theme_cfai');
    $description = '';
	$default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $pageSupport->add($setting);
	
                             
	$name = 'theme_cfai/support_pdf_desc';
    $title = get_string('support_desc', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $pageSupport->add($setting);
	
    
	$name = 'theme_cfai/support_pdf_issuu';
    $title = get_string('support_pdf_issuu', 'theme_cfai');
    $description = get_string('support_pdf_issuu_desc', 'theme_cfai');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_pdf_download';
    $title = get_string('support_pdf_download', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_pdf_download_label';
    $title = get_string('support_pdf_download_label', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_pdf_more_name';
    $title = get_string('support_pdf_more_name', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_pdf_more_desc';
    $title = get_string('support_pdf_more_desc', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $pageSupport->add($setting);
	
	$pageSupport->add(new admin_setting_heading('theme_cfai_video_heading',
        get_string('supportvideo', 'theme_cfai'),
        format_text(get_string('supportvideosettingdesc', 'theme_cfai'), FORMAT_MARKDOWN)));	
	
	$name = 'theme_cfai/show_support_video';
    $title = get_string('show_support_video', 'theme_cfai');
    $description = '';
	$default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $pageSupport->add($setting);
	
                             
	$name = 'theme_cfai/support_video_desc';
    $title = get_string('support_desc', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_video_lien1';
    $title = get_string('support_video_lien', 'theme_cfai'). '1';
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_video_lien2';
    $title = get_string('support_video_lien', 'theme_cfai'). '2';
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_video_lien3';
    $title = get_string('support_video_lien', 'theme_cfai'). '3';
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_video_lien4';
    $title = get_string('support_video_lien', 'theme_cfai'). '4';
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_video_lien5';
    $title = get_string('support_video_lien', 'theme_cfai'). '5';
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_video_lien6';
    $title = get_string('support_video_lien', 'theme_cfai'). '6';
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_video_lien_more';
    $title = get_string('support_video_lien_more', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	$name = 'theme_cfai/support_video_lien_more_label';
    $title = get_string('support_video_lien_more_label', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $pageSupport->add($setting);
	
	
	
	
	$pageSupport->add(new admin_setting_heading('theme_cfai_support_more_heading',
        get_string('support_more', 'theme_cfai'),
        format_text(get_string('support_more_desc', 'theme_cfai'), FORMAT_MARKDOWN)));	
		
		
	$name = 'theme_cfai/support_more';
    $title = get_string('support_more', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $pageSupport->add($setting);
	
	$pageSupport->add(new admin_setting_heading('theme_cfai_formation_heading',
        get_string('supportformation', 'theme_cfai'),''));	
	
	$name = 'theme_cfai/formation_desc';
    $title = get_string('support_desc', 'theme_cfai');
    $description = '';
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $pageSupport->add($setting);
	
	
	$settings->add($pageSupport);   

	
	
	
	
	
*/	
	
	
	
	// contact settings.                                 
    $page = new admin_settingpage('theme_cfai_contact', get_string('contactsettings', 'theme_cfai'));                           
	$name = 'theme_cfai/equipes';
    $title = get_string('team', 'theme_cfai');
    //$description = get_string('footnotedesc', 'theme_more');
    $description = '';
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');                                                                     
    $page->add($setting);
    $settings->add($page); 	
}
