define([
	'jquery', 
	'core/notification', 
	'core/ajax', 
	'core/templates', 
	'core/str'
	]
	function($, ajax, templates, notification, str)
	{
		
		function Ajaxcall() {
			this.value = "ajax ok";
		};
		Ajaxcall.prototype.loadsettings = function() {
			ajax.call([{
				methodname: 'mod_testtest_loadsettings',
				args: {itemID: 1},
				done: console.log("ajax done"),//how can I get the result?
				fail: notification.exception
			}]);
		};
		return Ajaxcall;
		
		/*
		return function(cid)
		{
			home: function()
			{
				$('aaa').on('click', function()
				{
					var promises = ajax.call([{
						methodname: '',
						args:{}
					}]);
					promise[0].done(function(data)
					{
						templates.render('cfai/index_page', data).done(function(html,js){
							$('aaa').replaceWith(html);
							templates.runTemplateJS(js);
						}).fail(notification.exception);
					}).fail(notification.exception);
				});
			}
		};
		*/
	}
);