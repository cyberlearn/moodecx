/**
 * Created by christop.hadorn on 06.10.2015.
 */

$(function () {
	
    // Get course list most popular
    getCourseListTop(8);
    getFutureCourseListTop(8);

    var idCourse = gup('courseid', location.href);
	var idUser = $("#idUser").val();

	if(idCourse){
		getCourse(idCourse,idUser);
	}

	$('.closePopUp').on('click', function () {
		window.location.hash="";
		hideCourse();
	})
	
	$('#pgwModal').on('click', function () {
		//alert('.modal-dialog clicked');
		window.location.hash="";
		hideCourse();
	})
	$('.modal-content').on('click', function (event) {
		//alert('.modal-dialog clicked');
		//window.location.hash="";
		//hideCourse();
		event.stopPropagation();
	})
	
});
