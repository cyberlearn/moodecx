<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A two column layout for the boost theme.
 *
 * @package   theme_cfai
 * @copyright 2017 Cyberlearn
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);
require_once($CFG->libdir . '/behat/lib.php');

$PAGE->requires->jquery();


if (isloggedin()) {
    $navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
} else {
    $navdraweropen = false;
}
$extraclasses = [];

$adminview=false;
if(is_siteadmin())
{
	$adminview=true;
	$extraclasses = [' adminview'];
}
else
{
	$extraclasses = [' notadminview'];
}

if ($navdraweropen) {
    $extraclasses[] .= 'drawer-open-left';
}

$frontpage = false;
$catalogpage = false;
$captcha = '';
$idOption = 1;


$courseListTop = '';
$courseListFuture = '';
$itemsResearch = '';

$loadingAnim=[];

$fileInclude = 'theme_cfai/frontpage';
$showhome = true;

$showCatalog = false;
$showContact = false;
$op = '';


$userForm=[];
$msg = "";
$msgError = "";

if (isset($_GET['op'])) 
{
	$op = $_GET['op'];
	if($op == 'cat')
	{
		$showCatalog = true;
		$showhome = false;
		$fileInclude = 'theme_cfai/frontpage_search';
		$extraclasses[] .= 'pagelayout-frontpage-catalog';

		$idOption = 2;
		$catalogpage = true;
		
		$loadingAnim['loadingsize'] = '100';
		$loadingAnim['loadingcolor'] = 'rgba(236, 0, 140, 0.5)';
	}
	else if($op == 'contact')
	{
		if (isloggedin()) 
		{
			$userForm = [
				'fn' => $USER->firstname,
				'ln' => $USER->lastname,
				'em' => $USER->email
			];
		} 
		
		
		$showContact = true;
		$showhome = false;
		$fileInclude = 'theme_cfai/frontpage_contact';
		$extraclasses[] .= 'pagelayout-frontpage-contact';
		$idOption = 3;
		
		if (!empty($_POST))
		{
			//homepagecontact
			$msg = "";
			if(isset($_POST['captchaToken']) && !empty($_POST['captchaToken'])) 
			{
				$secret = '6Lcz5Z0UAAAAAMUJPlfNHMIQbMbbY0nG2bj_lYY5';
				$token = $_POST['captchaToken'];
				
				$verifyResponse=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response={$token}");
				$responseData = json_decode($verifyResponse);
				
				if($responseData->success && $responseData->score>0.5)
				{
					
					$to = $CFG->supportemail;
					
					
					$name = html_to_text($_POST["name"]);
					$from = html_to_text($_POST["email"]);
					$forname = html_to_text($_POST["forname"]);
					$mailmessage = '';
					$mailmessagePost = $_POST["mailmessage"];
					$subject = "CFAI - Contact";


					$fromEmail = new stdClass();
					$fromEmail->email = $from;
					$fromEmail->firstname = $forname;
					$fromEmail->lastname = $name;
					$fromEmail->maildisplay = true;
					$fromEmail->mailformat = 1;
					$fromEmail->id = -99;
					$fromEmail->firstnamephonetic = $forname;
					$fromEmail->lastnamephonetic = $name;
					$fromEmail->middlename = $forname . " " . $name;
					$fromEmail->alternatename = $forname . " " . $name;

					$toEmail = new stdClass();
					$toEmail->email = $to;
					$toEmail->firstname = "Admin";
					$toEmail->lastname = "CFAI";
					$toEmail->maildisplay = true;
					$toEmail->mailformat = 1;
					$toEmail->id = -100;
					$toEmail->firstnamephonetic = "Admin";
					$toEmail->lastnamephonetic = "CFAI";
					$toEmail->middlename = "Admin CFAI";
					$toEmail->alternatename = "Admin CFAI";

					
					$msgPost  = "Nom :     " . $forname . " " . $name . " ".PHP_EOL;
					$msgPost .= "email :   " . $from . " ".PHP_EOL;
					$msgPost .= get_string('message', 'theme_cfai')." :  ".PHP_EOL;
					$msgPost .= $_POST["mailmessage"].PHP_EOL;
					
					
					$msgp1  = 'Bonjour, ' . PHP_EOL .''. PHP_EOL;
					$msgp1 .= 'Votre requête sera traitée dans les plus brefs délais.'. PHP_EOL;
					$msgp1 .= 'Voici un récapitulatif de votre demande :'.PHP_EOL;
					
					$msgp2 = 'Ceci est un message généré automatiquement, veuillez ne pas y répondre.'.PHP_EOL;
					

					$msghp2 = '<div style="font-size: 90%; color: #666;">';
					

					$msgPostp1 = "<div style='display: inline-block; width: 100px; vertical-align: top; color: #666; text-align: right; padding: 3px 6px;  ";
					
					$msgPostp2 = "<div style='display: inline-block; width: calc(100% - 130px); padding: 3px 6px; border-left: 1px solid #ddd;";
					
					$msgPostp3 = "'>";
					$msgPostp4 = "</div>";
					
					
					$msgPostHtml  = '<br><div style="border: 1px solid #aaaaaa; padding: 2px; margin: 10px 0; background: #f5f5f5;">';
					$msgPostHtml .= $msgPostp1."border-bottom: 1px solid #ddd; ".$msgPostp3.get_string('user')." :".$msgPostp4;
					$msgPostHtml .= $msgPostp2."border-bottom: 1px solid #ddd; ".$msgPostp3.$forname .' '. $name . $msgPostp4;
					
					$msgPostHtml .= $msgPostp1."border-bottom: 1px solid #ddd; ".$msgPostp3.get_string('email')." :".$msgPostp4;
					$msgPostHtml .= $msgPostp2."border-bottom: 1px solid #ddd; ".$msgPostp3.$from . $msgPostp4;
					
					$msgPostHtml .= $msgPostp1." ".$msgPostp3.get_string('message', 'theme_cfai')." :".$msgPostp4;
					$msgPostHtml .= $msgPostp2." ".$msgPostp3.trim(text_to_html($_POST["mailmessage"])) . $msgPostp4;
					$msgPostHtml .= "</div><br>";
					
					
					
					$msgText = $msgp1 . PHP_EOL . $msgPost . PHP_EOL . $msgp2;

					
					$msgHtml = text_to_html($msgp1).$msgPostHtml.$msghp2.$msgp2.'</div>';
					
					
					$email = email_to_user($toEmail, $fromEmail, $subject, $msgPost, $msgPostHtml);
					$emailcopie = email_to_user($fromEmail, $fromEmail, $subject, $msgText, $msgHtml);

					
					
					if ($email) {
						$msg .= '<div class="note">'.get_string('mailsuccess', 'theme_cfai').'</div>';
					} else {
						$msgError .= '<div class="note error">'.get_string('mailError', 'theme_cfai').'</div>';
					}
					
					
					
				}
				else
				{
					$msgError .= '<div class="note error">'.get_string('mailErrorGoogle', 'theme_cfai').'</div>';
				}
			}
		}
	}
	
}
else
{
	$extraclasses[] .= 'site_frontpage';
}

$cid=0;
if (isset($_GET['cid'])) 
{
	if($_GET['cid']>0)
	{
		$cid=$_GET['cid'];
	}
}


$bodyattributes = $OUTPUT->body_attributes($extraclasses);
$blockshtml = $OUTPUT->blocks('side-pre');
$contentblockshtml = $OUTPUT->blocks('content');
$hasblocks = strpos($blockshtml, 'data-block=') !== false;
$regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();

$menuheader = $OUTPUT->custom_menu_header($idOption);

$keyword = (!empty($_GET['keyword']) ? $_GET['keyword'] : '');
$templatecontext = [
    'adminview' => $adminview,
	'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
	'showbacktotop' => true,
    'sidepreblocks' => $blockshtml,
	'contentblocks' => $contentblockshtml,
    'hasblocks' => true,
    'bodyattributes' => $bodyattributes,
    'navdraweropen' => $navdraweropen,
    'regionmainsettingsmenu' => $regionmainsettingsmenu,
    'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
    'logo' => $OUTPUT->image_url('logo', 'theme_cfai'),
    'coursesearchaction' => $CFG->wwwroot . '/course/search.php',
	'isloggedin' => isloggedin(),
	'isguest' => isguestuser(),
	'keyword' => $keyword,
	'menuheader' => $menuheader,
	'iconMail' => $OUTPUT->image_url('icon_mail', 'theme_cfai'),
	'flatnavigation' => $PAGE->flatnav


];


if($showCatalog)
{
	$itemsResearch = $OUTPUT->get_home_search_filter();	
	
	$loadingAnim['loadingsize'] = '100';
	$loadingAnim['loadingcolor'] = 'rgba(236, 0, 140, 0.5)';
	
	$templatecontext['itemsResearch'] = $itemsResearch;
	$templatecontext['loadingAnim'] = $loadingAnim;
	
	$templatecontext['catalogpage'] = $showCatalog;
	
	$PAGE->requires->js_call_amd('theme_cfai/main', 'init', array($idOption, $cid));

	$url = new moodle_url('/?redirect=0', array('op' => 'cat'));
	$PAGE->set_url($url);
	
}
else if($showContact)
{
	$templatecontext['htmlequipesDesc'] = '';
	$templatecontext['userForm'] = $userForm;
	$templatecontext['msg'] = $msg;
	$templatecontext['msgError'] = $msgError;
	$templatecontext['showContact'] = $showContact;
	
	$url = new moodle_url('/?redirect=0', array('op' => 'contact'));
	$PAGE->set_url($url);
}
else
{
	
	//$urlPartenaires = ['http://www.hes-so.ch/', 'http://www.hes-so.ch/fr/recherche-hes-so-31.html', 'https://cyberlearn.hes-so.ch/', 'http://www.hesge.ch/heds/', 'http://www.hevs.ch/fr/'];
	//$imgPartenaires = [$OUTPUT->image_url('P_logoCL', 'theme_cfai'),$OUTPUT->image_url('P_heds', 'theme_cfai'),$OUTPUT->image_url('P_hevs', 'theme_cfai')];
	$imgPartenaires = [$OUTPUT->image_url('P_logoCL', 'theme_cfai')];
	$urlPartenaires = ['https://cyberlearn.hes-so.ch/'];
	$imgsizePartenaires = [150];
	
	$partenaires = '';
	
	for($i = 0; $i < count($imgPartenaires); $i++) {
		$partenaires .= '<a href="'. $urlPartenaires[$i] .'" target="_blank"><img src="'. $imgPartenaires[$i] .'" width="'. $imgsizePartenaires[$i] .'"></a>';
	}
	
	
	$htmlPartners = $PAGE->theme->settings->bloc_partenaires;
	//print_r($PAGE->theme->settings->bloc_partenaires);
	$courseListTop = $OUTPUT->get_courseListHome('top', 8);
	$courseListFuture = $OUTPUT->get_courseListHome('future', 8);
	
	
	$loadingAnim['loadingsize'] = '100';
	$loadingAnim['loadingcolor'] = 'rgba(0, 182, 190, 0.5)';
	
	
	$templatecontext['partenaires'] = $partenaires.$htmlPartners;
	$templatecontext['courseListTop'] = $courseListTop;
	$templatecontext['courseListFuture'] = $courseListFuture;
	$templatecontext['loadingAnim'] = $loadingAnim;
	$templatecontext['frontpage'] = true;
	

	$PAGE->requires->js_call_amd('theme_cfai/main', 'init', array($idOption, $cid));

}	


echo $OUTPUT->render_from_template($fileInclude, $templatecontext);

