<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace theme_cfai\output;

use coding_exception;
use html_writer;
use tabobject;
use tabtree;
use custom_menu_item;
use custom_menu;
use block_contents;
use navigation_node;
use action_link;
use stdClass;
use moodle_url;
use preferences_groups;
use action_menu;
use help_icon;
use single_button;
use single_select;
use paging_bar;
use url_select;
use context_course;
use context_module;
use pix_icon;
use theme_config;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot ."/course/renderer.php");
//require_once($CFG->libdir.'/coursecatlib.php');

/**
 * Renderers to align Moodle's HTML with that expected by Bootstrap
 *
 * @package    theme_cfai
 * @copyright  2012 Bas Brands, www.basbrands.nl
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


class core_renderer extends \theme_boost\output\core_renderer {

    public function course_title()
	{
		global $PAGE;
		$html = html_writer::start_div('region-main-header');
        $html .= $this->context_header();
        $html .= html_writer::end_div();
		return $html;
	}
	
	/**
     * Wrapper for header elements.
     *
     * @return string HTML to display the main header.
     */
    public function full_header() {

        global $PAGE, $COURSE;
        
        $html = html_writer::start_tag('header', array('id' => 'page-header', 'class' => ' '));
        $html .= html_writer::start_div('col-xs-12 p-a-1');
        $html .= html_writer::start_div('card');
        $html .= html_writer::start_div('card-block');
		

        $pageheadingbutton = $this->page_heading_button();
        if (empty($PAGE->layout_options['nonavbar'])) {
            $html .= html_writer::start_div('clearfix w-100 pull-xs-left', array('id' => 'page-navbar'));
            $html .= html_writer::tag('div', $this->navbar(), array('class' => 'breadcrumb-nav'));

            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button pull-xs-right');
            $html .= html_writer::end_div();
        } else if ($pageheadingbutton) {
            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button nonavbar pull-xs-right');
        }
       $html .= html_writer::tag('div', $this->course_header(), array('id' => 'course-header'));
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_tag('header');
        return $html;
    }
	
	
	
	public function frontpage_header() {
		global $PAGE, $COURSE;
		if(isloggedin() && is_siteadmin()) 
		{
			$html = html_writer::start_tag('header', array('id' => 'page-header', 'class' => ' '));
			$html .= html_writer::start_div('col-xs-12 p-a-1');
			$html .= html_writer::start_div('card');
			$html .= html_writer::start_div('card-block');
			
			if (isset($COURSE->id) && $COURSE->id == 1) {
				$html .= html_writer::div($this->context_header_settings_menu(), 'pull-xs-right context-header-settings-menu');
			}
			$pageheadingbutton = $this->page_heading_button();
			$html .= html_writer::div($pageheadingbutton, 'breadcrumb-button nonavbar pull-xs-right');
			
			$html .= html_writer::end_div();
			$html .= html_writer::end_div();
			$html .= html_writer::end_div();
			$html .= html_writer::end_tag('header');
			return $html;
		}
	}
	
	
	public function dashboard_header() {

        global $PAGE, $COURSE;
        
        $html = html_writer::start_tag('header', array('id' => 'page-header', 'class' => ' mymoodle'));
        $html .= html_writer::start_div('col-xs-12 p-a-1');
        $html .= html_writer::start_div('card');
        $html .= html_writer::start_div('card-block');
		
		$pageheadingbutton = $this->page_heading_button();
		
        $html .= html_writer::start_div('clearfix w-100 pull-xs-left', array('id' => 'page-navbar'));
        $html .= html_writer::tag('div', get_string('mymoodle', 'admin'), array('class' => 'titleDashboard'));
        
		$nbrBtn = substr_count($pageheadingbutton, 'singlebutton');
		if($nbrBtn == 1)
		{
			$pageheadingbutton = str_replace("singlebutton", "singlebutton editBtn", $pageheadingbutton);
		}
		else
		{
			$pageheadingbutton = preg_replace('/singlebutton/', 'singlebutton resetBtn', $pageheadingbutton, 1);
			if( ( $pos = strrpos( $pageheadingbutton , 'singlebutton' ) ) !== false ) {
				$search_length  = strlen( 'singlebutton' );
				$pageheadingbutton  = substr_replace( $pageheadingbutton , 'singlebutton cancelBtn' , $pos , $search_length );
			}
		}
		
		$html .= html_writer::div($pageheadingbutton, 'breadcrumb-button btn-mode-edition pull-xs-right');
        $html .= html_writer::end_div();
        
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();
        $html .= html_writer::end_tag('header');

		return $html;
    }
	
	

    public function image_url($imagename, $component = 'moodle') {
        // Strip -24, -64, -256  etc from the end of filetype icons so we
        // only need to provide one SVG, see MDL-47082.
        $imagename = \preg_replace('/-\d\d\d?$/', '', $imagename);
        return $this->page->theme->image_url($imagename, $component);
    }

    public function header_navbar() {
        global $PAGE;


		$html = '';
        $pageheadingbutton = $this->page_heading_button();
		if (empty($PAGE->layout_options['nonavbar'])) {
            $html .= html_writer::start_div('clearfix w-100 pull-xs-left', array('id' => 'page-navbar'));
            $html .= html_writer::tag('div', $this->navbar(), array('class' => 'breadcrumb-nav'));
            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button pull-xs-right');
            $html .= html_writer::end_div();
        } else if ($pageheadingbutton) {
            $html .= html_writer::div($pageheadingbutton, 'breadcrumb-button nonavbar pull-xs-right');
        }
		
        $html .= html_writer::tag('div', $this->course_header(), array('id' => 'course-header'));
        
        return $html;
    }
	
	/*
     * This renders the navbar.
     * Uses bootstrap compatible html.
     */
    public function navbar() {
        return $this->render_from_template('core/navbar', $this->page->navbar);
    }

	/*
     * 
     * add edit on 
     */
    public function edit_button(moodle_url $url) {
        global $SITE, $PAGE, $USER, $CFG, $COURSE;
        $url->param('sesskey', sesskey());
        if ($this->page->user_is_editing()) {
            $url->param('edit', 'off');
            $btn = 'btn-edit-off';
            $title = get_string('turneditingoff' , 'moodle');
            $icon = 'fa-edit-off';
        } else {
            $url->param('edit', 'on');
            $btn = 'btn-edit-on';
            $title = get_string('turneditingon' , 'moodle');
            $icon = 'fa-edit-on';
        }
        return html_writer::tag('a', html_writer::start_tag('i', array('class' => $icon . ' fa fa-fw')) .
            html_writer::end_tag('i') . $title, array('href' => $url, 'class' => 'btn  ' . $btn, 'title' => $title));
        return $output;
    }
	
	
	public function costom_loginBox() {
		$loginpage = $this->is_login_page();
		$loginurl = get_login_url();
		
		$loginContext = [];
		if (during_initial_install()) {return $loginContext;}
		
		$usertxtlogintmp = get_string('loggedinnot');
		if (isguestuser()) 
		{
			$usertxtlogintmp = get_string('loggedinasguest');
		}
		
		$loginContext['usertext'] = $usertxtlogintmp;
		$loginContext['hasddmenu'] = false;
		
		if (strpos($_SERVER['REQUEST_URI'], "login") == false)
		{
			$loginContext['hasddmenu'] = true;
			$ddmenu = []; //dropdown menu
			
			$ddmenu['actionurl'] = new moodle_url('/login/index.php');
			
			if($this->has_identity_providers())
			{
				$loginproviders = [];
				
				foreach ($this->get_identity_providers() as $provider) {
					$loginprovider = [];
					$loginprovider['purl'] = $provider['url'];
					$loginprovider['pname'] = $provider['name'];
					$loginprovider['purl'] = $provider['url'];

					if(isset($provider['iconurl']))
					{
						$loginprovider['picon'] = $provider['iconurl'];
					}
					
					$loginproviders[] = $loginprovider;
				}
				$ddmenu['loginproviders'] = $loginproviders;
			}
			
			$logintoken = \core\session\manager::get_login_token();
			$ddmenu['logintoken'] = $logintoken;
			$loginContext['ddmenu'] = $ddmenu;
		}
		
		
		
		$loginContext['forgetpassurl'] = new moodle_url('/login/forgot_password.php');
		$loginContext['startsignupurl'] = new moodle_url('/login/signup.php');
		
		//print_r('<br><br><br><br>');
		//print_r($loginContext);
		return $this->render_from_template('theme_cfai/custom_loginbox', $loginContext);
	}
	
	
	
	public function costom_loginBox111() {
		$loginpage = $this->is_login_page();
		$loginurl = get_login_url();
		
		$returnstr = "";
		$usermenulist = "";
		$usertxtlogin = '<span class="usertext">';
		$userpiclogin = '';
		$usertxtlogintmp = '';
		
		if (during_initial_install()) {return $returnstr;}
		
		
		if ($loginpage) {
			
		}
		$usertxtlogintmp = get_string('loggedinnot');
		if (isguestuser()) 
		{
			$usertxtlogintmp = get_string('loggedinasguest');
		}
		$userpiclogin = html_writer::tag('img', '',
			array(
				'src'=>$this->image_url('i/user'),
				'class'=>'userpicture',
				'alt'=>get_string('login'),
				'title'=>$usertxtlogintmp
			)
		);
		$returnstr = '<ul id="loginbox" class="nav"><li class="dropdown usermenuonlogged"><a href="#" class="dropdown-toggle" data-toggle="dropdown" title="usermenuonlogged">';
		$returnstr .= $usertxtlogin.$usertxtlogintmp.'</span>';
		$returnstr .= '<i id="iconloginuser" class="fas fa-user-circle" aria-hidden="true"></i><b class="caret"></b></a>';
		
		if (strpos($_SERVER['REQUEST_URI'], "login") == false)
		{
			$returnstr .= '<ul class="dropdown-menu dropdown-menu-right" id="loginpaneldropdown">';

			$returnstr .= '<li class="loginpanel">
							<form action="'.new moodle_url('/login/index.php').'" method="post" id="login">';
							
			if($this->has_identity_providers())
			{
				$returnstr .= '<div class="potentialidplist" class="m-t-1">';
				foreach ($this->get_identity_providers() as $provider) {
					$returnstr .= '<div class="potentialidp">';
					$returnstr .= '<a href="'.$provider['url'].'" ';
					$returnstr .= 'title="'.$provider['name'].'" class="btn btn-secondary btn-block '.$provider['name'].'">';
					if(isset($provider['iconurl']))
					{
						$returnstr .= '<img src="'.$provider['iconurl'].'" alt="" width="24" height="24"/>';
					}
					
					$returnstr .= $provider['name'];
					$returnstr .= '</a>';
					$returnstr .= '</div>';
				}


				$returnstr .= '</div>';
				$returnstr .= '<div class="loginPanelSep sep"><div class="septxt">'.get_string('loginSep','theme_cfai').'</div></div>';				
			}				
			$returnstr .= '<div class="loginform">
							<div class="input-group input-group-username">
								<span class="input-group-addon">
									<i class="fa fa-user-circle"></i>
								</span>
								<input type="text" placeholder="'.get_string('username').'" name="username" id="username" class="form-control" size="15" value="">
							</div>
							<div class="input-group input-group-password">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" placeholder="'.get_string('password').'" name="password" id="password" class="form-control" size="15" value="">
							</div>
							<button type="submit" id="loginsubmitbtn" class="btn btn-primary btn-block" form="login" value="'.get_string('login').'">'.get_string('login').'</button>
							</div>
							
							<div class="clearer"><!-- --></div>';
			$returnstr .= '</form></li>';

			$returnstr .= '<li class="signup">
								<div class="forgetpass">
									<a href="'.new moodle_url('/login/forgot_password.php').'" class="linkLostPass">'.get_string('loginPanelForgotten', 'theme_cfai').'</a>
									<a href="'.new moodle_url('/login/signup.php').'" class="linkInscription">'.get_string('startsignup').'</a>
								</div>
							</li>';
			$returnstr .= '</li></ul>';
		}
		$returnstr .= '</li></ul>';
		return $returnstr;
	}
	
	/**
     * Verify whether the site has identity providers
     *
     * @return mixed
     */
    public function has_identity_providers() {
        global $CFG;

        $authsequence = get_enabled_auth_plugins(true);

        require_once($CFG->libdir . '/authlib.php');

        $identityproviders = \auth_plugin_base::get_identity_providers($authsequence);

        return !empty($identityproviders);
    }
	
	/**
     * Return the site identity providers
     *
     * @return mixed
     */
    public function get_identity_providers() {
        global $CFG;

        $authsequence = get_enabled_auth_plugins(true);

        require_once($CFG->libdir . '/authlib.php');

        $identityproviders = \auth_plugin_base::get_identity_providers($authsequence);

        return $identityproviders;
    }
	
	
	
	
	
	public function custom_drawer_menu()
	{
		global $CFG;
		$html  = '';
		if (is_siteadmin()) {
			$branchtitle = get_string('administration', 'theme_cfai');
			
			$branchurl = new moodle_url('/admin/search.php');

            $html = '<nav class="list-group top admin">
				<a class="list-group-item list-group-item-action " href="'.$branchurl.'" data-key="siteadmin" data-isexpandable="0" data-indent="0" data-showdivider="1" data-type="71" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0">
					<div class="m-l-0">
						<div class="media">
							<span class="media-closed" title="'.$branchtitle.'">
								<i class="icon fa fa-wrench fa-fw " aria-hidden="true" aria-label=""></i>
							</span>
							<span class="media-left">
								<i class="icon fa fa-wrench fa-fw " aria-hidden="true" aria-label=""></i>
							</span>
							<span class="media-body ">'.$branchtitle.'</span>
						</div>
					</div>
				</a>
			</nav>';
		}
		
		return $html;
	}
	
	
	
	
	
    /*
     * This renders the bootstrap top menu.
     *
     * This renderer is needed to enable the Bootstrap style navigation.
     */
    protected function render_custom_menu(custom_menu $menu) {
        global $CFG;
        $context = $this->page->context;



        if (isloggedin() && !isguestuser()) {

			
            $branchlabel = get_string('mycfai', 'theme_cfai');
			$branchtitle = $branchlabel;
            $branchurl   = new moodle_url('/my/index.php');
            $branchsort  = 10000;
 
            $branch = $menu->add($branchlabel, $branchurl, $branchtitle, $branchsort);
            
            if ($courses = enrol_get_my_courses(NULL, 'fullname ASC')) {
                foreach ($courses as $course) {
                    if ($course->visible){
                        $branch->add(format_string($course->fullname), new moodle_url('/course/view.php?id='.$course->id), format_string($course->shortname));
                    }
                }
            } else {
                $noenrolments = get_string('noenrolments', 'theme_cfai');
                $branch->add('<em>'.$noenrolments.'</em>', new moodle_url('/'), $noenrolments);
            }

        }

        if (!$menu->has_children()) {
            return '';
        }
		
		$content = '';
        foreach ($menu->get_children() as $item) {
            $context = $item->export_for_template($this);
            $content .= $this->render_from_template('core/custom_menumycourse_item', $context);
        }

        return $content;
    }
	public function custom_menu_admin()
	{
		global $CFG;
		$html  = '';
		if (is_siteadmin()) {
			$branchtitle = get_string('administration', 'theme_cfai');
			$branchlabel = '<span class="txt">'.$branchtitle.'</span>';
			$branchurl = new moodle_url('/admin/search.php');
			
		
			$html = html_writer::start_tag('div', array('class' => 'nav-item admin'));
			$html .= html_writer::tag('a',  $branchlabel, array('href' => $branchurl, 'class' => 'nav-item nav-link', 'title' => $branchtitle));
			$html .= html_writer::end_tag('div');
			

                        
		}
		
		return $html;
	}

	
	
	public function custom_menu_header( $idOption=0) {
		//echo "<script>console.log( 'idOption = " . $idOption . "' );</script>";
		
		if(!isset($idOption))
			$idOption = 0;
		
		$menuContent = '';
		$cMenuHeader = '<div id="banner-menu">';

		$cMenuHeader .= html_writer::link(new moodle_url('/?redirect=0'), '<div id="logosmall"></div>', array('id' => "menu1", 'class' => 'mitem'));
		
		$cMenuHeader .= '<div id="mitems">';
		
		$menuLabel = get_string('catalog', 'theme_cfai');
		if($idOption == 2)
		{
			$menuContent  = '<div id="menu2" class="mitem selected">';
			$menuContent .= html_writer::tag('span', '', array('id'=>'menuicon_catalog', 'class'=>'menuicon', 'title'=>$menuLabel));
			$menuContent .= html_writer::tag('span',$menuLabel);
			$menuContent .= '</div>';
		}
		else
		{
			$menuContent = html_writer::link(new moodle_url('/?redirect=0&op=cat'), '<span id="menuicon_catalog" class="menuicon" title="'.$menuLabel.'"></span><span>'.$menuLabel.'</span>', array('id' => "menu2", 'class' => 'mitem'));
		}
		$cMenuHeader .= $menuContent;
		
		
		$menuLabel = get_string('contact', 'theme_cfai');
		if($idOption == 3)
		{
			$menuContent  = '<div id="menu3" class="mitem selected">';
			$menuContent .= html_writer::tag('span', '', array('id'=>'menuicon_catalog', 'class'=>'menuicon', 'title'=>$menuLabel));
			$menuContent .= html_writer::tag('span',$menuLabel);
			$menuContent .= '</div>';
		}
		else
		{
			$menuContent = html_writer::link(new moodle_url('/?redirect=0&op=contact'), '<span id="menuicon_catalog" class="menuicon" title="'.$menuLabel.'"></span><span>'.$menuLabel.'</span>', array('id' => "menu3", 'class' => 'mitem'));

		}
		$cMenuHeader .= $menuContent;
		
		$cMenuHeader .= '</div>';

		return $cMenuHeader.'</div>';


	}
	
	
	public function custom_menu_account()
	{
		$cMenuHeader='';
		if(isloggedin())
		{
			if (isguestuser()) 
			{
				$cMenuHeader .= '<div class="usermenu"><span class="login">';
				$cMenuHeader .= get_string('guestUser');
				$cMenuHeader .= html_writer::link(new moodle_url('/login/index.php'), get_string('login'));
				$cMenuHeader .= '</span></div>';
			}
			else
			{
				$cMenuHeader .= \core_renderer::user_menu();
			}
		}
		else
		{
			$cMenuHeader .= $this->costom_loginBox();
			
		}
		
		return $cMenuHeader;
	}
        
	public function custom_menu_language()
	{
		global $CFG;
		$langmenu = new custom_menu();
		
		$langs = get_string_manager()->get_list_of_translations();
        $haslangmenu = $this->lang_menu() != '';

        if (!$haslangmenu) {
            return '';
        }
		else
		{
		
			$currentlang = $this->get_currentlang($langs);

            $this->language = $langmenu->add($currentlang['code']);
            foreach ($langs as $langtype => $langname) {
                
				$start  = strpos($langname,"(" );
				$end = strpos($langname,")");
				$langcode = substr($langname, $start+1, $end-$start-1);
			
				if($langname ==  $currentlang['lang'])
				{
					$this->language->add($langcode);
				}
				else
				{
					$this->language->add($langcode, new moodle_url($this->page->url, array('lang' => $langtype)));	
				}
				
            }
		}
		$content = '';
        foreach ($langmenu->get_children() as $item) {
            $context = $item->export_for_template($this);
            $content .= $this->render_from_template('theme_cfai/custom_langmenu_item', $context);
        }

        return $content;
	}
	
	
    protected function render_thiscourse_menu(custom_menu $menu) {
        global $CFG;

        $content = '';
        foreach ($menu->get_children() as $item) {
            $context = $item->export_for_template($this);
            $content .= $this->render_from_template('theme_cfai/activitygroups', $context);
        }

        return $content;
    }

    public function thiscourse_menu() {
        global $PAGE, $COURSE, $OUTPUT, $CFG;
        $menu = new custom_menu();
        $context = $this->page->context;
            if (isset($COURSE->id) && $COURSE->id > 1) {
                $branchtitle = get_string('thiscourse', 'theme_cfai');
                $branchlabel = $branchtitle;
                $branchurl = new moodle_url('#');
                $branch = $menu->add($branchlabel, $branchurl, $branchtitle, 10002);

                $data = theme_cfai_get_course_activities();

                foreach ($data as $modname => $modfullname) {
                    if ($modname === 'resources') {
                        
                        $branch->add($modfullname, new moodle_url('/course/resources.php', array('id' => $PAGE->course->id)));
                    } else {

                        $branch->add($modfullname, new moodle_url('/mod/'.$modname.'/index.php',
                                array('id' => $PAGE->course->id)));
                    }
                }
                
        }

        return $this->render_thiscourse_menu($menu);
    }

    

   

   


    public function teacherdash() {
        global $PAGE, $COURSE, $CFG, $DB, $OUTPUT;

        require_once($CFG->dirroot.'/completion/classes/progress.php');
        $togglebutton = '';
        $hasteacherdash = '';
        if (isloggedin() && ISSET($COURSE->id) && $COURSE->id > 1) {
            $course = $this->page->course;
            $context = context_course::instance($course->id);
            $hasteacherdash = has_capability('moodle/course:viewhiddenactivities', $context);

            if (has_capability('moodle/course:viewhiddenactivities', $context)) {
                $togglebutton = get_string('coursemanagementbutton', 'theme_cfai');
            } 
        }
        $course = $this->page->course;
        $context = context_course::instance($course->id);
		$tmpEditCog = $this->context_header_settings_menu().'';
		$haseditcog = true;
		if(strlen($tmpEditCog)<=0)
		{
			$haseditcog = false;
		}
        
        $editcog = html_writer::div($this->context_header_settings_menu(), 'pull-xs-right context-header-settings-menu');
        $thiscourse = $this->thiscourse_menu();
        $showincourseonly = isset($COURSE->id) && $COURSE->id > 1 && isloggedin() && !isguestuser(); 
        $globalhaseasyenrollment = enrol_get_plugin('easy');
        $coursehaseasyenrollment = '';
        if($globalhaseasyenrollment) {
            $coursehaseasyenrollment = $DB->record_exists('enrol', array('courseid' => $COURSE->id, 'enrol' => 'easy'));
            $easyenrollinstance = $DB->get_record('enrol', array('courseid' => $COURSE->id, 'enrol' => 'easy'));
        }
        
        //link catagories
        $haspermission = has_capability('enrol/category:config', $context) && isset($COURSE->id) && $COURSE->id > 1;
        $userlinks = get_string('userlinks', 'theme_cfai');
        $userlinksdesc = get_string('userlinks_desc', 'theme_cfai');
        $qbank = get_string('qbank', 'theme_cfai');
        $qbankdesc = get_string('qbank_desc', 'theme_cfai');
        $badges = get_string('badges', 'theme_cfai');
        $badgesdesc = get_string('badges_desc', 'theme_cfai');
        $coursemanage = get_string('coursemanage', 'theme_cfai');
        $coursemanagedesc = get_string('coursemanage_desc', 'theme_cfai');

        //user links
        if($coursehaseasyenrollment && isset($COURSE->id) && $COURSE->id > 1){
            $easycodetitle = get_string('header_coursecodes', 'enrol_easy');
            $easycodelink = new moodle_url('/enrol/editinstance.php', array('courseid' => $PAGE->course->id, 'id' => $easyenrollinstance->id, 'type' =>'easy'));
        }
        $gradestitle = get_string('gradesoverview', 'gradereport_overview');
        $gradeslink = new moodle_url('/grade/report/grader/index.php', array('id' => $PAGE->course->id));
        $enroltitle = get_string('enrolledusers', 'enrol');
        $enrollink = new moodle_url('/user/index.php', array('id' => $PAGE->course->id));
        $grouptitle = get_string('groups', 'group');
        $grouplink = new moodle_url('/group/index.php', array('id' => $PAGE->course->id));
        $enrolmethodtitle = get_string('enrolmentinstances', 'enrol');
        $enrolmethodlink = new moodle_url('/enrol/instances.php', array('id' => $PAGE->course->id));
        
        //user reports
        $logstitle = get_string('logs', 'moodle');
        $logslink = new moodle_url('/report/log/index.php', array('id' => $PAGE->course->id));
        $livelogstitle = get_string('loglive:view', 'report_loglive');
        $livelogslink = new moodle_url('/report/loglive/index.php', array('id' => $PAGE->course->id));
        $participationtitle = get_string('participation:view', 'report_participation');
        $participationlink = new moodle_url('/report/participation/index.php', array('id' => $PAGE->course->id));
        //$activitytitle = get_string('outline:view', 'report_outline');
        //$activitylink = new moodle_url('/report/outline/index.php', array('id' => $PAGE->course->id));

        //questionbank
        $qbanktitle = get_string('questionbank', 'question');
        $qbanklink = new moodle_url('/question/edit.php', array('courseid' => $PAGE->course->id));
        $qcattitle = get_string('questioncategory', 'question');
        $qcatlink = new moodle_url('/question/category.php', array('courseid' => $PAGE->course->id));
        $qimporttitle = get_string('import', 'question');
        $qimportlink = new moodle_url('/question/import.php', array('courseid' => $PAGE->course->id));
        $qexporttitle = get_string('export', 'question');
        $qexportlink = new moodle_url('/question/export.php', array('courseid' => $PAGE->course->id));
        
        //manage course
        $courseadmintitle = get_string('courseadministration', 'moodle');
        $courseadminlink = new moodle_url('/course/admin.php', array('courseid' => $PAGE->course->id));
        $coursecompletiontitle = get_string('coursecompletion', 'moodle');
        $coursecompletionlink = new moodle_url('/course/completion.php', array('id' => $PAGE->course->id));
        $courseresettitle = get_string('reset', 'moodle');
        $courseresetlink = new moodle_url('/course/reset.php', array('id' => $PAGE->course->id));
        $coursebackuptitle = get_string('backup', 'moodle');
        $coursebackuplink = new moodle_url('/backup/backup.php', array('id' => $PAGE->course->id));
        //$courserestoretitle = get_string('restore', 'moodle');
        $courserestorelink = new moodle_url('/backup/restorefile.php', array('contextid' => $PAGE->context->id));
        $courseimporttitle = get_string('import', 'moodle');
        $courseimportlink = new moodle_url('/backup/import.php', array('id' => $PAGE->course->id));
        $courseedittitle = get_string('editcoursesettings', 'moodle');
        $courseeditlink = new moodle_url('/course/edit.php', array('id' => $PAGE->course->id));
        
        //badges
        $badgemanagetitle = get_string('managebadges', 'badges');
        $badgemanagelink = new moodle_url('/badges/index.php?type=2', array('id' => $PAGE->course->id));
        $badgeaddtitle = get_string('newbadge', 'badges');
        $badgeaddlink = new moodle_url('/badges/newbadge.php?type=2', array('id' => $PAGE->course->id));
        
        //misc
        $recyclebintitle = get_string('pluginname', 'tool_recyclebin');
        $recyclebinlink = new moodle_url('/admin/tool/recyclebin/index.php', array('contextid' => $PAGE->context->id));
        //$filtertitle = get_string('filtersettings', 'filters');
        //$filterlink = new moodle_url('/filter/manage.php', array('contextid' => $PAGE->context->id));
        //send to template
		$hasquestionpermission = has_capability('moodle/question:add', $context);
		$hasbadgepermission = has_capability('moodle/badges:awardbadge', $context);
		$hascoursepermission = has_capability('moodle/backup:backupcourse', $context);
		$hasuserpermission = has_capability('moodle/course:viewhiddenactivities', $context);
		$hasgradebookshow = $PAGE->course->showgrades == 1;
        $hascompletionshow = $PAGE->course->enablecompletion == 1;
		
		$btnMoretitle = get_string('morenavigationlinks');
		$btnMorelink = new moodle_url('/course/admin.php', array('courseid' => $PAGE->course->id));
		
        $dashlinks = [
        'showincourseonly' =>$showincourseonly,
        'haspermission' => $haspermission,
        'thiscourse' => $thiscourse,
        'haseditcog' => $haseditcog,
        'editcog' => $editcog,
        'togglebutton' => $togglebutton,
        'userlinkstitle' => $userlinks,
        'userlinksdesc' => $userlinksdesc,
        'qbanktitle' => $qbank,
        'qbankdesc' => $qbankdesc,
        'badgestitle' => $badges,
        'badgesdesc' => $badgesdesc,
        'coursemanagetitle' => $coursemanage,
        'coursemanagedesc' => $coursemanagedesc,

        'hasteacherdash' => $hasteacherdash,
		'teacherdash' =>array('hasquestionpermission' => $hasquestionpermission, 'hasbadgepermission' => $hasbadgepermission, 'hascoursepermission' => $hascoursepermission, 'hasuserpermission' => $hasuserpermission),

        'studentcourseadminlink' => $courseadminlink,

        'dashlinks' => array(

                array('hasuserlinks' => $enroltitle, 'title' => $enroltitle, 'url' => $enrollink),
                array('hasuserlinks' => $grouptitle, 'title' => $grouptitle, 'url' => $grouplink),
                array('hasuserlinks' => $enrolmethodtitle, 'title' => $enrolmethodtitle, 'url' => $enrolmethodlink),
                array('hasuserlinks' => $logstitle, 'title' => $logstitle, 'url' => $logslink),
                array('hasuserlinks' => $livelogstitle, 'title' => $livelogstitle, 'url' => $livelogslink),
                array('hasuserlinks' => $participationtitle, 'title' => $participationtitle, 'url' => $participationlink),
               // array('hasuserlinks' => $activitytitle, 'title' => $activitytitle, 'url' => $activitylink),
                array('hasqbanklinks' => $qbanktitle, 'title' => $qbanktitle, 'url' => $qbanklink),
                array('hasqbanklinks' => $qcattitle, 'title' => $qcattitle, 'url' => $qcatlink),
                array('hasqbanklinks' => $qimporttitle, 'title' => $qimporttitle, 'url' => $qimportlink),
                array('hasqbanklinks' => $qexporttitle, 'title' => $qexporttitle, 'url' => $qexportlink),
                array('hascoursemanagelinks' => $courseedittitle, 'title' => $courseedittitle, 'url' => $courseeditlink),
                array('hascoursemanagelinks' => $coursecompletiontitle, 'title' => $coursecompletiontitle, 'url' => $coursecompletionlink),
                array('hascoursemanagelinks' => $courseadmintitle, 'title' => $courseadmintitle, 'url' => $courseadminlink),
                array('hascoursemanagelinks' => $courseresettitle, 'title' => $courseresettitle, 'url' => $courseresetlink),
                array('hascoursemanagelinks' => $coursebackuptitle, 'title' => $coursebackuptitle, 'url' => $coursebackuplink),
                //array('hascoursemanagelinks' => $courserestoretitle, 'title' => $courserestoretitle, 'url' => $courserestorelink),
                array('hascoursemanagelinks' => $courseimporttitle, 'title' => $courseimporttitle, 'url' => $courseimportlink),
                array('hascoursemanagelinks' => $recyclebintitle, 'title' => $recyclebintitle, 'url' => $recyclebinlink),
                //array('hascoursemanagelinks' => $filtertitle, 'title' => $filtertitle, 'url' => $filterlink),
                array('hasbadgelinks' => $badgemanagetitle, 'title' => $badgemanagetitle, 'url' => $badgemanagelink),
                array('hasbadgelinks' => $badgeaddtitle, 'title' => $badgeaddtitle, 'url' => $badgeaddlink),
				array('hascoursemanagemorelinks' => $btnMoretitle, 'title' => $btnMoretitle, 'url' => $btnMorelink),
            ),
        ];

        //attach easy enrollment links if active
        if ($globalhaseasyenrollment && $coursehaseasyenrollment) {
            $dashlinks['dashlinks'][] = array('haseasyenrollment' => $coursehaseasyenrollment, 'title' => $easycodetitle, 'url' => $easycodelink);

        } 
            return $this->render_from_template('theme_cfai/teacherdash', $dashlinks );
        
    }
	
	
	/**
     * Renders the login form.
     *
     * @param \core_auth\output\login $form The renderable.
     * @return string
     */
    public function render_login(\core_auth\output\login $form) {
        global $SITE;

        $context = $form->export_for_template($this);

        // Override because rendering is not supported in template yet.
        $context->cookieshelpiconformatted = $this->help_icon('cookiesenabled');
        $context->errorformatted = $this->error_text($context->error);

       // $context->logourl = $this->get_logo();
        $context->sitename = format_string($SITE->fullname, true, array('context' => \context_course::instance(SITEID)));

        return $this->render_from_template('core/login', $context);
    }
	

	/**
     * Whether we should display the main theme logo in the navbar.
     *
     * @return bool
     */
    public function should_display_theme_logo() {
        $logo = $this->get_theme_logo_url();

        return !empty($logo);
    }

    /**
     * Get the main logo URL.
     *
     * @return string
     */
    public function get_theme_logo_url() {
        $theme = theme_config::load('moove');

        return $theme->setting_file_url('logo', 'logo');
    }
	
	public function get_currentlang($langs)
	{

		$strlang = get_string('language');
		$currentlang = current_language();
		if (isset($langs[$currentlang])) {
			$currentlang = $langs[$currentlang];
		} else {
			$currentlang = $strlang;
		}
		
		$start  = strpos($currentlang,"(" );
        $end = strpos($currentlang,")");
        $langname = substr($currentlang, 0, $start);
        $langcode = substr($currentlang, $start+1, $end-$start-1);
		
		$lang = [];
		$lang['lang'] = $currentlang;
		$lang['code'] = $langcode;
		$lang['name'] = $langname;
		
		//print_r('<br><br><br><br>');
		//print_r($langs);
		return $lang;
	}
	
	
	public function get_date_lang_local( $d, $format = "%d %b %Y")
	{
		global $CFG, $SESSION, $COURSE;

		
		
		$namelang = 'fr_FR.UTF8';

		
		$langs = get_string_manager()->get_list_of_translations();
		$currentlang = $this->get_currentlang($langs);	
		$langcode = $currentlang['code'];	
		
		if($langcode == 'en')
		{
			$namelang = 'en-US.UTF8';
		}
		else if($langcode == 'de')
		{
			$namelang = 'de-DE.UTF8';
		}
		else if($langcode == 'it')
		{
			$namelang = 'it-IT.UTF8';
		}
		
		
		
		setlocale(LC_TIME, $namelang);
		//$date = utf8_encode(strftime($format,$d));
		$date = strftime($format,$d);
		
		
			
		return $date;
		//$month = utf8_encode(strftime('%B',$record->startdate));

		
	}
	
	public function getCourseHome($idC)
	{
		global $CFG, $DB, $USER, $OUTPUT;
		
		
		$contextLevel = 50;
		$roleId = 4;
		$enrol = "%self%";
		
		/*
		$md_context_sql ="
				select md.id as md_id,  cm.id ascm_id, cm.course ,  cm.module,  mdl_context.*
				FROM(
					(((mdl_moodecdescription md LEFT JOIN mdl_course ON md.course = mdl_course.id )
					JOIN mdl_course_modules cm ON md.id = cm.instance AND  cm.course = $idC)
					join mdl_modules on mdl_modules.id = cm.module)
					join mdl_context on mdl_context.instanceid = cm.id
				)
				 WHERE mdl_modules.name='moodecdescription'
			";
		*/
		$courseSql = "
		SELECT c.id as contextid, mc.id, md.id as mdid, course as courseid , fullname, summary, startdate, e.id as enrolid, urlredirection , endcoursedate, enrolstartdate, enrolenddate, effort, md.price, md.currency, md.courseformat, duration, md.institution, syllabus, reading, faq, prerequisite , video, md.picture as picturecourse 
		FROM mdl_moodecdescription md, mdl_course mc, mdl_enrol e,  mdl_context c
		WHERE mc.id=md.course 
			AND e.courseid = mc.id 
			AND c.instanceid = mc.id 
			AND md.course= $idC 
			AND contextlevel = $contextLevel 
			AND e.enrol LIKE '%self%'					
		";
			
		if($cr = $DB->get_record_sql($courseSql))
		{
			
			
			$context = context_course::instance($idC);
			
			
			$urlpartners = [];
			
			
			$cr->partners = '';
			
			$instance = get_coursemodule_from_instance('moodecdescription', $cr->mdid, $idC);

			$contextmodule = context_module::instance($instance->id);

			$fs = get_file_storage();
			$indexp = 0;
			if ($files = $fs->get_area_files($contextmodule->id, 'mod_moodecdescription', 'partnersattachment', 1, 'sortorder', false))
			{
				foreach ($files as $file)
				{
					$indexp++;
					$fileurl = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), 1, $file->get_filepath(), $file->get_filename());
					
					$cr->partners .= HTML_WRITER::start_tag('img', array('src' => $fileurl, 'class' => 'md_logopartner'));
				}
				
			}
			
			$cr->enrol = is_enrolled($context, $USER->id, '', true);

			$role = $DB->get_record('role', array('shortname' => 'profediteur'));
			if(empty($role))
			{
				$role = $DB->get_record('role', array('shortname' => 'editingteacher'));
			}

			$teachersDB = get_role_users($role->id, $context, false,
                'u.id, u.firstname, u.lastname, u.email, u.picture');
			
			$teachers = [];
			foreach ($teachersDB as $staff) {
				$teacher['link'] = $CFG->wwwroot.'/user/profile.php?id='.$staff->id;
				$teacher['name'] = $staff->firstname . ' ' . $staff->lastname;
				$teacher['email'] = $staff->email;
				$teacher['picture'] = $OUTPUT->user_picture($staff, array('size' => 120, 'link'=>false) );
				$teachers[] = $teacher;
            }
			//$cr->teachers = serialize($teachers);
			$cr->teachers = json_encode($teachers);
			
			
			if($cr->courseformat == '')
			{
				$cr->courseformat = 'online';
			}
			$cr->courseformat = get_string($cr->courseformat, 'moodecdescription');
			
			
			if($cr->price <= 0)
			{
				$cr->price = '';	
			}
			else
			{
				$cr->price .= '.00 '.$cr->currency;	
			}
				
			
			
			
			$cr->infocourse = true;
			if($cr->prerequisite == "" && $cr->syllabus == "" && $cr->reading == "" && $cr->faq == "" )
			{
				$cr->infocourse = false;
			}
			
			/*
			$("#effort .iconDesc").html(course.effort + " " + $("#hours").val() + "/" + $("#week").val());
			
			 $("#duration .iconDesc").html(course.duration + " " + $("#weeks").val());
			*/
			//$m_effort = lcfirst(get_string('hour'));
			$m_effort = substr(get_string('hour'), 0, 1);
			$m_duration = get_string('week', 'theme_cfai');
			/*
			if($cr->effort > 1)
			{
				//$m_effort = get_string('hours');
			}
			*/
			$cr->effort .= " $m_effort / $m_duration";
			if($cr->duration > 1)
			{
				$m_duration = get_string('weeks', 'theme_cfai');
			}
			
			$cr->duration .= " $m_duration";
			
			
			$cr->startdate = $this->get_date_lang_local($cr->startdate);
			$cr->endcoursedate = $this->get_date_lang_local($cr->endcoursedate);
			//$cr->startdate =  ' '.date("F j, Y", $cr->startdate);
			//$cr->endcoursedate =  ' '.date("F j, Y", $cr->endcoursedate);
			
			$dateStart =  date($cr->enrolstartdate);
			$dateEnd =  date($cr->enrolenddate);
			$now = getdate()[0];
			
			$cr->enrolstartdate = $this->get_date_lang_local($cr->enrolstartdate);
			$cr->enrolenddate = $this->get_date_lang_local($cr->enrolenddate);
			//$cr->enrolstartdate =  ' '.date("F j, Y", $cr->enrolstartdate);
			//$cr->enrolenddate =  ' '.date("F j, Y", $cr->enrolenddate);
			
			$cr->enroltext = '';
			$cr->enrollink = $CFG->wwwroot.'/course/view.php?id='.$idC;
			if($dateStart <= $now)
			{
				if($cr->enrol)
				{
					$cr->enroltext = get_string('accesscours', 'theme_cfai');
				}
				else if($dateEnd < $now)
				{
					$cr->enroltext = get_string('registrationClosed', 'theme_cfai');
					$cr->enrollink = '';
				}
				else
				{
					$cr->enroltext = get_string('enrolme', 'theme_cfai');
					
					if($cr->urlredirection != '')
					{
						$cr->enrollink = $cr->urlredirection;
					}
				}
			}
			else
			{
				$cr->enroltext = get_string('enrolnotyet', 'theme_cfai').' '.$cr->enrolstartdate;
				$cr->enrollink = '';
			}
			
			
			
			//print_r('<br><br><br><br><br><br><br><br>--------------------');
			//print_r($now);
			//if($cr->enrolstartdate)
			
			return (array)$cr;
			
			

		}
		return false;
	}
	
	public function get_home_search_filter() 
	{
		global $DB, $CFG, $SESSION;
		
	
		
		$institutionSql = "
		SELECT DISTINCT md.institution
		FROM mdl_moodecdescription md, mdl_course mc, mdl_course_categories cc
		WHERE mc.id=md.course
		AND mc.category = cc.id AND mc.visible = 1					
		";
		$dataResearch = [];	
		if($ir = $DB->get_records_sql($institutionSql))
		{
			$index = 1;
			foreach ($ir as $institution) 
			{
				$dataInst['name'] = $institution->institution;
				$dataInst['id'] = $index++;
				
				$dataResearch['institutions'][] = $dataInst;
			}
		}
		
		$langs = get_string_manager()->get_list_of_translations();
		$currentlang = $this->get_currentlang($langs);

		$index = 1;
		foreach ($langs as $key => $value) 
		{
			$start  = strpos($value,"(" );
			
			$datalang['id'] = $index++;
			$datalang['name'] = substr($value, 0, $start);
			$datalang['key'] = $key;
			$datalang['selected'] = '';
			if($key == $currentlang['code'] )
			{
				$datalang['selected'] = 'checked';
			}
			
			$dataResearch['langs'][] = $datalang;
		}
		//print_r($dataResearch);
		return $dataResearch;
	}
	
	
	
	public function get_keywords($keywords){
		$temp = preg_split('/(\s+)/', $keywords, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

		$words = array_reduce( $temp, function( &$result, $item) {
			if(strlen( trim( $item)) !== 0) {
				$result[] = "%" . $item . "%";
			}
			return $result;
		}, array());
		return $words;
	}

	public function get_courses_home_search($search_data = '') 
	{
		global $DB, $CFG;

		//print_r('<br><br><br><br><br><br><br>get_courses_home_search');
		$search_data = json_decode($search_data, true);

		$queryInstitution = "";
		$queryLang = "";
		$queryKeywords = "";
		
		$keywords = $search_data['keyword'];
		$institution = $search_data['institution'];
		$lang = $search_data['lang'];

		
		$lang = array_filter($lang, 'strlen');
		Sort($lang);  

		$countLang = count($lang);
		
		for ($i = 0; $i < $countLang; $i++) {
			if($i == 0)
			{
				$queryLang .= "AND (";
			}
			else
			{
				$queryLang .= " OR ";
			}
			
			$queryLang .= "(lang LIKE '".$lang[$i]."'";
			if($lang[$i] == 'fr')
			{
				$queryLang .= " OR lang = ''";
			}
			$queryLang .= ")";
			if ($i == $countLang - 1) 
			{
				$queryLang .= " ) 
				";
			}
		}
		
		
		//$institution= array('vevey', 'hes-so', '', 'aaa' );
		$institution = array_filter($institution, 'strlen');
		Sort($institution);  
		$lastInstitution = count($institution)-1;

		foreach($institution as $key => $inst) 
		{ 
			if($key === 0) 
			{ 
				$queryInstitution .= " AND (";
			} 
			else
			{
				$queryInstitution .= " OR ";
			}
			$queryInstitution .= "institution LIKE '".$inst."' ";
			if ($key == $lastInstitution) 
			{
				$queryInstitution .= " ) 
				";
			}
		} 
		//print_r('<br><br><br><br><br><br><br>Keywords');
		// Keywords
		$arrayKeywords = $this->get_keywords($keywords);

		$arrayKeywords = array_filter($arrayKeywords, 'strlen');
		Sort($arrayKeywords);  
		$lastKeywords = count($arrayKeywords)-1;
		
		foreach($arrayKeywords as $key => $kw) 
		{ 
			if($key === 0) 
			{ 
				$queryKeywords .= " AND (";
			} 
			else
			{
				$queryKeywords .= " OR ";
			}
			$queryKeywords .= "tags LIKE '".$kw."' ";
			$queryKeywords .= " OR fullname LIKE '".$kw."' ";
			$queryKeywords .= " OR summary LIKE '".$kw."' ";
			$queryKeywords .= " OR institution LIKE '".$kw."' ";
			if ($key == $lastKeywords) 
			{
				$queryKeywords .= " ) 
				";
			}
		}
		
		
		$query =  $queryLang . $queryInstitution . $queryKeywords;	

		$queryAdv = "
			SELECT mc.id, md.id as mdid, course as courseid, picture as pictureCourse, fullname, startdate, summary, institution
			FROM mdl_moodecdescription md, mdl_course mc, mdl_course_categories cc, mdl_enrol en
			WHERE mc.id=md.course
			AND mc.id = en.courseid  
			AND md.endcoursedate > ".time()." 
			AND en.enrol = 'self'
			AND en.status = 0
			AND mc.category = cc.id
			AND mc.visible = 1
			AND mc.id != 30
			AND mc.id != 46
			AND mc.id != 54
			$query
			ORDER BY startdate ASC";
		

		$courseListHtml = '';
		if($courseListSearchRecords = $DB->get_records_sql($queryAdv))
		{
			foreach ($courseListSearchRecords as $cr) {
				
				$month = $this->get_date_lang_local($cr->startdate, "%B");
				$dateY = $this->get_date_lang_local($cr->startdate, "%Y");
				$date = $this->get_date_lang_local($cr->startdate, "%d-%m-%Y");
				$url = $this->get_courselist_imgurl($cr->courseid, $cr->mdid);
				
				$cl['cid'] = $cr->courseid;
				$cl['url'] = $url;
				$cl['date'] = $date;
				$cl['dateY'] = $dateY;
				$cl['month'] = $month;
				$cl['fullname'] = $cr->fullname;
				$cl['institution'] = $cr->institution;
				
				$cls['course'][] =  $cl;
			}

		}
		else
		{
			$cls['nocourse'][] = 1;
		}
		$content = $this->render_from_template('theme_cfai/bloc_course_home', $cls);
		$content .='<i aria-hidden="true" style="width: 220px;max-width: 220px;height: 1px;margin: 0px 10px;"></i>';
		$content .='<i aria-hidden="true" style="width: 220px;max-width: 220px;height: 1px;margin: 0px 10px;"></i>';
		$content .='<i aria-hidden="true" style="width: 220px;max-width: 220px;height: 1px;margin: 0px 10px;"></i>';
		$content .='<i aria-hidden="true" style="width: 220px;max-width: 220px;height: 1px;margin: 0px 10px;"></i>';
		$content .='<i aria-hidden="true" style="width: 220px;max-width: 220px;height: 1px;margin: 0px 10px;"></i>';
		$content .='<i aria-hidden="true" style="width: 220px;max-width: 220px;height: 1px;margin: 0px 10px;"></i>';

        return $content;
		
		
	}
	
	
	

	
	public function get_courseListHome($cl_type='top', $nbr=8 ) {
        global $CFG, $DB, $USER, $SESSION;
		require_once($CFG->libdir.'/moodlelib.php');
		
		$courseListHtml = '';
		
		
		if($cl_type=='future')
		{
			$courseListSql = "
				SELECT DISTINCT md.id, course as cid, fullname, startdate, md.picture as pictureCourse, institution
					FROM mdl_moodecdescription md, mdl_course mc, mdl_enrol en
					WHERE mc.id = md.course 
					AND mc.id = en.courseid 
					AND en.enrol = 'self' 
					AND en.status = 1 
					AND endcoursedate > ".time()." 
					AND enddate > ".time()." 
					AND mc.visible = 1 AND mc.id != 30 AND mc.id != 46 AND mc.id != 54
					ORDER BY startdate ASC LIMIT 
				" . $nbr;
		}
		else
		{
			$courseListSql = "
				SELECT md.id, course as cid, fullname, summary, startdate, picture as pictureCourse, institution
					FROM mdl_moodecdescription md, mdl_course mc, mdl_enrol en
					WHERE mc.id = md.course 
					AND mc.id = en.courseid 
					AND en.enrol = 'self' 
					AND en.status = 0 
					AND endcoursedate > ".time()."
					AND mc.visible = 1 AND mc.id != 30 AND mc.id != 46 AND mc.id != 54
					ORDER BY startdate ASC LIMIT 
				" . $nbr;
		}


		if($courseListRecords = $DB->get_records_sql($courseListSql))
		{
			
			$courseListHtml = '';
			$index_r = 0;
			$classHide = ' ';
		
			foreach ($courseListRecords as $record) 
			{

				$url = $this->get_courselist_imgurl($record->cid, $record->id);
				
				if ($index_r >= 6) {
					$classHide = ' tohide';	
				}

				if($cl_type=='future')
				{
					$cl['cid'] = $record->cid;
					$cl['url'] = $url;
					$cl['email'] = $CFG->supportemail;
					$cl['mailSubject'] = get_string('mailpreregistration', 'theme_cfai').$record->fullname;
					$cl['mailbody'] = get_string('mailprecontent', 'theme_cfai').$record->fullname;
					$cl['fullname'] = $record->fullname;
					$cl['institution'] = $record->institution;
					$cl['class'] = $classHide;
					$cls['future'][] =  $cl;	
				}
				else
				{

					$month = $this->get_date_lang_local($record->startdate, "%B");
					$dateY = $this->get_date_lang_local($record->startdate, "%Y");
					$date = $this->get_date_lang_local($record->startdate, "%d-%m-%Y");

					$cl['cid'] = $record->cid;
					$cl['url'] = $url;
					$cl['date'] = $date;
					$cl['dateY'] = $dateY;
					$cl['month'] = $month;
					$cl['fullname'] = $record->fullname;
					$cl['institution'] = $record->institution;
					$cl['class'] = $classHide;
					
					$cls['course'][] =  $cl;								
				}
				$index_r++;
			}

		}
		else
		{
			//$courseListHtml = "<div class='blocCoursEmpty'><p class='error'>".get_string('nocourse', 'theme_cfai')."</p></div>";
			$cls['nocourse'][] = 1;
		}
		
		$content = $this->render_from_template('theme_cfai/bloc_course_home', $cls);
        return $content;
    }
	
	
	
	public function get_courselist_imgurl($cid, $mdid)
	{
		global $CFG, $DB;
		
		$url = $CFG->wwwroot . '/mod/moodecdescription/pix/default_course_image.png';
		
		$instance = get_coursemodule_from_instance('moodecdescription', $mdid, $cid, false, MUST_EXIST);
		$context = context_course::instance($cid);
				
		$sql = "contextid = ? AND filearea = ? AND component = ? AND itemid = ? AND mimetype != ?";
		$param = array(
		   $context->id,
		   'overviewfiles',
		   'course',
		   0,
		   ''
		);
		
		if ($file = $DB->get_record_select('files', $sql, $param))
		{
			$fs = get_file_storage();
			$f = $fs->get_file($file->contextid, $file->component, $file->filearea, 0, $file->filepath, $file->filename);
			
			if ($f) {
				$url_to_decode = moodle_url::make_pluginfile_url(
				 $f->get_contextid(),
				 $f->get_component(),
				 $f->get_filearea(),
				 '',
				 $f->get_filepath(),
				 $f->get_filename()
				);
				
				$url = $url_to_decode->get_scheme() . '://' .$url_to_decode->get_host() . $url_to_decode->get_path();
			}
		}
		return $url;
	}
	
	/*
	
	public function get_html_CourseListTop111($cid, $img, $date, $dateY, $month, $fullname, $institution='', $class='')
	{

		$txtHtml ="
			<a id='homeBlocCourse$cid' class='blocCours $class' data-cid='$cid'>
				<div class='imgCours' style='background: #f5f5f5 url($img) no-repeat center; background-size: 100% auto'>
					<div class='dateCoursHighLight'>
						<div>$dateY</div>
						<div>$month</div>
					</div>
				</div>
				<div class='fdTop'>
					<div class='borderWhite'>".get_string('accesscours', 'theme_cfai')."</div>
				</div>
				<div class='fdBottom'>
					<div>
						<span class='detailsMoocs'>$institution</span>
						<span class='detailsMoocs'>$date</span>
						<span class='titreMooc'>$fullname</span>
					</div>
				</div>
			</a>
			";
		return $txtHtml;
	}
	
	public function get_html_CourseListFuture($email, $img, $mailSubject, $mailbody, $fullname, $institution='', $class='')
	{
		$txtHtml ="
			<a class='blocCours  $class' href='mailto:$email?subject=$mailSubject&body=$mailbody' target='_top' title='mail'>
				<div class='imgCours' style='background: #f5f5f5 url($img) no-repeat center; background-size: 100% auto'></div>
				<div class='bgHover'>
					<i></i>
					".get_string('contactfuturemooc', 'theme_cfai')."
					<div class='cadreW'></div>
				</div>
				<div class='institutionMoocs'>
					<div>$institution</div>
				</div>
				<div class='titreMooc'>.$fullname</div>
			</a>
		";
		return $txtHtml;
	}
	

	public static function loadsettings($itemid) {
        global $DB;
        //$params = self::validate_parameters(self::getExample_parameters(), array());

		//return 'loadsettings';
		$params = array('id'=>$itemid);
		$sql = "SELECT fullname FROM mdl_course WHERE id = $itemid";
		print_r('<br><br><br><br><br><br><br><br>--------------------');
		print_r($sql);
		print_r('<br>--------------------<br>');
		echo "<script>alert('received!')</script>";
		if($db_result = $DB->get_records_sql($sql))
		{
			return $db_result;
			
		}
		else
		{
			return null;
		}

		
    }
    
    public static function updatesettings($itemid, $data2update) {
        return 'updatesettings';

    }
	
	 public function render_index_page($page) {
        $data = $page->export_for_template($this);
        return parent::render_from_template('theme_cfai/index_page', $data);
    }
	*/

}