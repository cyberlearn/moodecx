<?php

namespace theme_cfai\webservice;

require_once($CFG->libdir."/externallib.php");

require_once($CFG->dirroot."/webservice/externallib.php");

use external_api;
use external_function_parameters;
use external_value;
use external_format_value;
use external_single_structure;
use external_multiple_structure;
use invalid_parameter_exception;


/**
 * This is the external API for this plugin.
 *
 * @copyright  2015 Damyon Wiese
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class external extends external_api {
    
	public static function get_home_popup_parameters() {
		return new external_function_parameters(
            array(
                'itemid' => new external_value(PARAM_INT, 'The item id to operate on'),
            )    
        );
    }
	
	public static function get_home_popup_returns() {

		return new external_single_structure(
           
                array(
                    'contextid' => new external_value(PARAM_RAW, ''),
					'id' => new external_value(PARAM_RAW, ''),
					'mdid' => new external_value(PARAM_RAW, ''),
					'courseid' => new external_value(PARAM_RAW, ''),
					'fullname' => new external_value(PARAM_RAW, ''),
					'summary' => new external_value(PARAM_RAW, ''),
					'startdate' => new external_value(PARAM_RAW, ''),
					'enrolid' => new external_value(PARAM_RAW, ''),
					'urlredirection' => new external_value(PARAM_RAW, ''),
					'endcoursedate' => new external_value(PARAM_RAW, ''),
					'enrolstartdate' => new external_value(PARAM_RAW, ''),
					'enrolenddate' => new external_value(PARAM_RAW, ''),
					'effort' => new external_value(PARAM_RAW, ''),
					'price' => new external_value(PARAM_RAW, ''),
					'currency' => new external_value(PARAM_RAW, ''),
					'courseformat' => new external_value(PARAM_RAW, ''),
					'duration' => new external_value(PARAM_RAW, ''),
					'institution' => new external_value(PARAM_RAW, ''),
					'syllabus' => new external_value(PARAM_RAW, ''),
					'reading' => new external_value(PARAM_RAW, ''),
					'faq' => new external_value(PARAM_RAW, ''),
					'prerequisite' => new external_value(PARAM_RAW, ''),
					'video' => new external_value(PARAM_RAW, ''),
					'picturecourse' => new external_value(PARAM_RAW, ''),
					'enrol' => new external_value(PARAM_BOOL, ''),
					'teachers' => new external_value(PARAM_RAW, ''),
					'infocourse' => new external_value(PARAM_BOOL, ''),
					'enroltext' => new external_value(PARAM_RAW, ''),
					'enrollink' => new external_value(PARAM_RAW, ''),
					'partners' => new external_value(PARAM_RAW, '')
                )
        );
		
    }

    public static function get_home_popup($itemid) 
	{
        global $DB, $OUTPUT;
		
		$db_result = $OUTPUT->getCourseHome($itemid); 

		return (array)($db_result);

    }
	
	
	
	public static function get_home_search_advanced_parameters() {
		return new external_function_parameters(
            array(
                'data' => new external_value(PARAM_RAW, 'filtre for search'),
            )    
        );
    }
	
	public static function get_home_search_advanced_returns() {

		return new external_single_structure(
           
                array(
                    'content' => new external_value(PARAM_RAW, ''),
                )
        );
		
    }

    public static function get_home_search_advanced($data) 
	{
        global $DB, $OUTPUT;
		//$data = var_dump(json_decode($data, true));
		//$db_result['content'] = 1; 
		
		$courselist = $OUTPUT->get_courses_home_search($data);
		
		
		$db_result['content'] = $courselist;
		

		return $db_result;

    }
	
	
	
	/*
	
	
	public static function get_home_popup111($serviceshortnames = array()) {
        global $PAGE;
		
		$renderer = $PAGE->get_renderer('theme_cfai');
        $page = new \theme_cfai\output\index_popup();
        return $page->export_for_template($renderer);
    }
    
     * Wrap the core function get_home_popup.
     *
     * @return external_description
     
    public static function get_home_popup_returns() {
        $result = \core_webservice_external::get_home_popup_returns();
        $result->keys['currenttime'] = new external_value(PARAM_RAW, 'the current time');
        return $result;
    }
	*/
}
