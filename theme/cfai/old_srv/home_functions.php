<?php
/**
 * Created by PhpStorm.
 * User: christop.hadorn
 * Date: 06.10.2015
 * Time: 14:16
 */

require_once('course.php');

$token = htmlspecialchars((!empty($_REQUEST['token']) ? $_REQUEST['token'] : ''));
$action = htmlspecialchars((!empty($_REQUEST['action']) ? $_REQUEST['action'] : ''));
$nbr = htmlspecialchars((!empty($_REQUEST['nbr']) ? $_REQUEST['nbr'] : ''));
$courseid = htmlspecialchars((!empty($_REQUEST['courseid']) ? $_REQUEST['courseid'] : ''));
$enrolid = htmlspecialchars((!empty($_REQUEST['enrolid']) ? $_REQUEST['enrolid'] : ''));
$keyword = htmlspecialchars((!empty($_REQUEST['keyword']) ? $_REQUEST['keyword'] : ''));
$contextid = htmlspecialchars((!empty($_REQUEST['contextid']) ? $_REQUEST['contextid'] : ''));
$institution = serialize(!empty($_REQUEST['institution']) ? $_REQUEST['institution'] : '');
$lang = serialize(!empty($_REQUEST['lang']) ? $_REQUEST['lang'] : '');
$idUser = htmlspecialchars((!empty($_REQUEST['iduser']) ? $_REQUEST['iduser'] : ''));
$codeCertificate = htmlspecialchars((!empty($_REQUEST['codecertificate']) ? $_REQUEST['codecertificate'] : ''));
$prelogin = htmlspecialchars((!empty($_REQUEST['prelogin']) ? $_REQUEST['prelogin'] : ''));

if(!empty($token) && !empty($_SESSION['token'])){
	if ($token == $_SESSION['token']) {
		switch($action) {
			case 'getLang':
				echo getLang();
				break;
			case 'getCourseListTop':
				echo getCourseListTop($nbr);
				break;
			case 'getCourse':
				echo getCourse($courseid, $idUser);
				break;
            case 'getFutureCourseList':
                echo getFutureCourseList($nbr);
                break;
			case 'search':
				//echo ($keyword);
				echo searchCourse($keyword, unserialize($lang));
				break;
			case 'getParamResearchAdvanced':
				echo getParamResearchAdvanced();
				break;
			case 'searchCourseAdvanced':
				echo searchCourseAdvanced($keyword, unserialize($institution), unserialize($lang));
				break;
			case 'getUser':
				echo getUser($idUser);
				break;
			case 'verifyCertificate':
				echo verifyCertificate($codeCertificate);
				break;
			case 'enrolUser':
				echo enrolUser($courseid, $prelogin);
				break;
			case 'hideCourse':
				echo hideCourse();
				break;
		}
	}
}
	
