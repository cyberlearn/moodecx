<?php
include_once('../../../../config.php');

$file = basename(urldecode($_GET['file']));
//$fileDir1 = '/home/clients/f23fe24001c451cacffb7b8b98eb5eda/moodledata/mod/moodecdescription/pix/thumbnail/';
$fileDir = $CFG->dataroot . '/mod/moodecdescription/pix/thumbnail/';

if (file_exists($fileDir . $file)) {
    $contents = file_get_contents($fileDir . $file);
    } else {
    $contents = file_get_contents('../../pix/home/default_course_image.png');
}
header('Content-type: image/jpeg');
echo $contents;
?>