<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file.
 *
 * @package   theme_cfai
 * @copyright 2017 Cyberlearn HES-SO
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// This line protects the file from being accessed by a URL directly.
defined('MOODLE_INTERNAL') || die();


$string['pluginname'] = 'CFAI 1.0';
$string['choosereadme'] = 'Theme cfai is a child theme of Boost for CFAI.';
$string['region-side-pre'] = 'Right';
$string['region-content'] = 'Center';

$string['assistance'] = 'Assistance';
$string['links'] = 'Links';
$string['joinin'] = 'Join in !';
$string['blog'] = 'Blog';
$string['team'] = 'Team';
$string['help'] = 'Help';
$string['home'] = 'Home';
$string['catalog'] = 'Catalog';
$string['contact'] = 'Contact';
$string['searchcourse'] = 'Search courses';
$string['administration'] = 'Administration';
$string['loginaai'] = 'Login AAI';
$string['logoutaai'] = 'Logout AAI';
$string['login'] = 'Already have an account?';
$string['myhome'] = 'My dashboard';
$string['loggedinas'] = ' logged in as ';
$string['loggedinfrom'] = 'Logged in from ';
$string['loginSep'] = 'or';
$string['loginPanelForgotten'] = 'Password lost';



//teacher and student dashboard slider
$string['thiscourse'] = 'This Course';
$string['userlinks'] = 'User Links';
$string['userlinks_desc'] = 'Manage your students';
$string['qbank'] = 'Question Bank';
$string['qbank_desc'] = 'Create and organize quiz questions';
$string['badges'] = 'Badges';
$string['badges_desc'] = 'Award your students';
$string['coursemanage'] = 'Course Settings';
$string['coursemanage_desc'] = 'Manage your entire course';
$string['coursemanagementbutton'] = 'Course Management';
$string['studentdashbutton'] = 'This Course';
$string['courseinfo'] = 'Course Description';
$string['coursestaff'] = 'Course Teachers';
$string['activitylinkstitle'] = 'Activities';
$string['activitylinkstitle_desc'] = 'View All Activities in Course';
$string['myprogresstext'] = 'My Progress';
$string['mygradestext'] = 'My Grades';


//Edit Button Text
$string['editon'] = 'Turn Edit On';
$string['editoff'] = 'Turn Edit Off';


$string['mycfai'] = 'My CFAI';
$string['mymoocs'] = 'My moocs';
$string['mycourses'] = 'My Courses';
$string['myunits'] = 'My Units';
$string['mymodules'] = 'My Modules';
$string['myclasses'] = 'My Classes';
$string['noenrolments'] = 'You have no current enrolments';
$string['siteadminquicklink'] = 'Site Administration';

$string['backtotop'] = 'Back to Top';

$string['guestUser'] = 'Guest';

/**
 * HOME PAGE
 */
$string['learnmore'] = 'Learn more';
$string['subscribehome'] = 'Register';
$string['nowhome'] = 'now';
$string['bannersoustitre'] = 'The Mooc plateform of the ';
$string['topcourse'] = 'Available courses';
$string['topfuturecourse'] = "Upcoming courses";
$string['partenaires'] = 'Partners';
$string['enrolnotyet'] = 'Available from ';
$string['enrolme'] = 'Enrol me';
$string['searchadvanced'] = 'Research advanced';
$string['keywords'] = 'Keywords';
$string['institution'] = 'Institutions';
$string['lang'] = 'Languages';
$string['moreinfo'] = 'More informations';
$string['prerequisite'] = 'Pre-requisite';
$string['syllabus'] = 'Syllabus';
$string['reading'] = 'Reading';
$string['faq'] = 'FAQ';
$string['school'] = 'Institution';
$string['startdate'] = 'Start date';
$string['enddate'] = 'End date';
$string['duration'] = 'Duration';
$string['effort'] = 'Effort';
$string['error'] = 'An error has occurred';
$string['nocourse'] = 'No courses are availables';
$string['cours'] = 'course';
$string['courspluriel'] = 'courses';
$string['fr'] = 'French';
$string['de'] = 'German';
$string['en'] = 'English';
$string['accesscours'] = 'Go to course';
$string['hour'] = 'hour';
$string['hours'] = 'hours';
$string['week'] = 'week';
$string['weeks'] = 'weeks';
$string['role'] = 'Teacher';
$string['contacttitle'] = 'Contact administrator' ;
$string['contactforname'] = 'Firstname : ';
$string['contactname'] = 'Lastname : ';
$string['contactemail'] = 'Email : ';
$string['message'] = 'Content : ';
$string['send'] = 'Send';
$string['refresh'] = 'Refresh';
$string['mandatoryfield'] = 'All fields are required';
$string['emailinvalid'] = "Email is invalid";
$string['mailinvalid'] = "An error has occurred";
$string['mailsuccess'] = "Your request will be processed as soon as possible";
$string['captchainvalid'] = "Captcha is invalid";
$string['enrolend'] = "Course is over";
$string['registrationClosed'] = "Registrations closed";
$string['verifycfaicertificate'] = 'Verify certificate';

$string['contactfuturemooc'] = "Contact us for more information";
$string['mailpreregistration'] = 'Preregistration for ';
$string['mailprecontent'] = 'Dear Sir or Madam, %0D%0AI am interested in following ';

/**
 * setting theme
 */
$string['configsetting'] = 'settings';


$string['generalsettings'] = 'General settings';



$string['partenairessettingdesc'] = 'la liste des partenaires sur la page d\'accueil.';

$string['dashboardnews'] = 'News - dashboard';
$string['dashboardnewssettingdesc'] = 'Configure here news';
$string['bloc_news_hes_name'] = 'Title';
$string['showforall'] = 'All';
$string['showforstudent'] = 'Student';
$string['showforteacher'] = 'Teacher';
$string['contenunews'] = 'Content of the news';


$string['contactsettings'] = 'Page Contacts';
$string['refresh'] = 'Refresh';
$string['mandatoryfield'] = 'All fields are required';
$string['emailinvalid'] = "Email is invalid";
$string['mailinvalid'] = "An error has occurred";

$string['mailError'] = "Your message has not been sent, an error has occurred.";
$string['mailErrorGoogle'] = "Your message has not been sent, a fraud has been detected.";
$string['mailsuccess'] = "Your message has been sent, your request will be processed as soon as possible.";
$string['captchainvalid'] = "Invalid captcha";
